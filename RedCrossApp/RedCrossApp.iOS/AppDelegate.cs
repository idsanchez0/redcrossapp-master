using Syncfusion.XForms.Pickers.iOS;
using Syncfusion.XForms.iOS.Cards;
using Syncfusion.XForms.iOS.ComboBox;
using Syncfusion.XForms.iOS.Core;
using Syncfusion.XForms.iOS.Graphics;
using Syncfusion.SfRating.XForms.iOS;
using Syncfusion.SfRotator.XForms.iOS;
using Syncfusion.XForms.iOS.Border;
using Syncfusion.ListView.XForms.iOS;
using Syncfusion.XForms.iOS.Buttons;
using System;

using Foundation;
using UIKit;
using Microsoft.Identity.Client;
using Firebase.CloudMessaging;
using UserNotifications;
using Xamarin.Essentials;
using Xamarin.Forms.Internals;
using Firebase.InstanceID;

namespace RedCrossApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        private App crossApp;
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //Xamarin.FormsMaps.Init("AIzaSyB7-SDQj8HEgDc5-E3Hii7UQVB_ILoKaa4");
            new Syncfusion.SfAutoComplete.XForms.iOS.SfAutoCompleteRenderer();            
            global::Xamarin.Forms.Forms.Init();
            SfSwitchRenderer.Init();
            SfTimePickerRenderer.Init();
            SfDatePickerRenderer.Init();
            SfCardViewRenderer.Init();
            Core.Init();
            SfGradientViewRenderer.Init();
            SfComboBoxRenderer.Init();
            SfRatingRenderer.Init();
            SfRotatorRenderer.Init();
            SfBorderRenderer.Init();
            SfListViewRenderer.Init();
            SfButtonRenderer.Init();
            Syncfusion.XForms.iOS.TabView.SfTabViewRenderer.Init();            
            // Specify the Keychain access group
            App.iOSKeychainSecurityGroup = NSBundle.MainBundle.BundleIdentifier;

            // Register your app for remote notifications.
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.Current.Delegate = this;

                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
                {
                    Log.Warning(nameof(FinishedLaunching), $"Granted: {granted}");
                });
            }
            else
            {
                // iOS 9 or before
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            Firebase.Core.App.Configure();
            Messaging.SharedInstance.Delegate = this;

            LoadApplication(new App());

            UIApplication.SharedApplication.RegisterForRemoteNotifications();
            InstanceId.SharedInstance.GetInstanceId(InstanceIdResultHandler);

            // Watch for notifications while the app is active
            UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            return base.FinishedLaunching(app, options);
        }

        private void InstanceIdResultHandler(InstanceIdResult result, NSError error)
        {
            if (error != null)
            {
                return;
            }

            Console.WriteLine($"(1) Firebase token: {result.Token}");

            Preferences.Set("TokenFirebase", result.Token);
        }

        [Export("messaging:didReceiveRegistrationToken:")]
        public void DidReceiveRegistrationToken(Messaging messaging, string fcmToken)
        {
            Console.WriteLine($"(2) Firebase registration token: {fcmToken}");

            if (string.IsNullOrEmpty(fcmToken))
                fcmToken = "2FC36E29-B28F-4D27-B652-2BED227D555D";

            Preferences.Set("TokenFirebase", fcmToken);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo,
            Action<UIBackgroundFetchResult> completionHandler)
        {
            //App.Current.MainPage.DisplayAlert("(3) Got remote notification", userInfo.Description, "ok");

            // Handle Notification messages in the background and foreground.
            // Handle Data messages for iOS 9 and below.

            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification

            // With swizzling disabled you must let Messaging know about the message, for Analytics
            //Messaging.SharedInstance.AppDidReceiveMessage(userInfo);

            HandleMessage(userInfo);

            completionHandler(UIBackgroundFetchResult.NewData);
        }

        [Export("messaging:didReceiveMessage:")]
        public void DidReceiveMessage(Messaging messaging, RemoteMessage remoteMessage)
        {
            Console.WriteLine("Message Received");
            // Handle Data messages for iOS 10 and above.
            HandleMessage(remoteMessage.AppData);
        }

        private void HandleMessage(NSDictionary message)
        {
            var action = message.ObjectForKey(new NSString("gcm.notification.action")) as NSString;
            if (action != null && action.ToString() == "News")
            {
                var Value = message.ObjectForKey(new NSString("value")) as NSString;

                if(Value != null)
                {
                    Preferences.Set("CurrentNews", Value?.ToString() ?? "0");
                    App.Current.MainPage = new MenuPage();
                }
            }
        }

        // <OpenUrlSnippet>
        // Handling redirect URL
        // See: https://github.com/azuread/microsoft-authentication-library-for-dotnet/wiki/Xamarin-iOS-specifics
        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            AuthenticationContinuationHelper.SetAuthenticationContinuationEventArgs(url);
            return true;
        }
        // </OpenUrlSnippet>

    }

    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        #region Constructors
        public UserNotificationCenterDelegate()
        {
        }
        #endregion

        #region Override Methods
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            // Do something with the notification
            Console.WriteLine("Active Notification: {0}", notification);

            // Tell system to display the notification anyway or use
            // `None` to say we have handled the display locally.
            completionHandler(UNNotificationPresentationOptions.Alert);
        }
        #endregion
    }

}
