﻿using RedCrossApp.Models;
using RedCrossApp.Views;
using RedCrossApp.Views.MyFilePage;
using RedCrossApp.Views.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp
{
    public partial class MenuPage : FlyoutPage
    {
        private string Name = "";

        private bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool) (App.Current.Properties["IsAdmin"]) : false;

        public List<MenuItemsModel> menuItems;
        public MenuPage(Page page = null, int? index = null)
        {

            InitializeComponent();

            Task task = Task.Run((Action)Utils.LoadParameters);



            if (index != null)
                Preferences.Set("MenuIndex", index.ToString());

            myPageMain(page);

            if (App.Current.Properties.ContainsKey("Name"))
                Name = App.Current.Properties["Name"].ToString();

            UserName.Text = Name;

            FlyoutLayoutBehavior = FlyoutLayoutBehavior.Popover;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        public void myPageMain(Page page = null)
        {
            if (page == null)
                Detail = new NavigationPage(new BottomNavigationPage());
            else
                Detail = new NavigationPage(page);

            menuItems = new List<MenuItemsModel> { };

            menuItems.Add(new MenuItemsModel { page = typeof(BottomNavigationPage), Title = "Inicio", Icon = "",productImage= "icono_inicio_menu" });//Icon = "\uf007"

            if (!isAdmin)
            {
                menuItems.Add(new MenuItemsModel { page = typeof(HealthProfilePage), Title = "Mi Ficha", Icon = "", productImage = "icono_mificha_menu" });//Icon = "\uf2c2"
                menuItems.Add(new MenuItemsModel { page = typeof(BottomNavigationPage), index = 3, Title = "Notificaciones", Icon = "", productImage = "icono_notificaciones_menu" });//Icon = "\uf27a"
            }

            if (isAdmin)
            {
                //menuItems.Add(new MenuItemsModel { page = typeof(ActivityRegisterPage), Title = "Publicación de Actividades", Icon = "\uf0a1" });
                //menuItems.Add(new MenuItemsModel { page = typeof(BottomNavigationPage), index = 3, Title = "Verificación de Participantes", Icon = "\uf4fc" });
            }
            else
            {
                //menuItems.Add(new MenuItemsModel { page = typeof(ActivitiesListPage), Title = "Actividades Propuestas", Icon = "\uf0a1" });
                //menuItems.Add(new MenuItemsModel { page = typeof(BottomNavigationPage), index = 3, Title = "Mis postulaciones", Icon = "\uf4c4" });
            }

            menuItems.Add(new MenuItemsModel { page = typeof(BottomNavigationPage), index = 2, Title = "Calendario", Icon = "", productImage = "icono_calendario_menu" });//Icon = "\uf073"

            menuItems.Add(new MenuItemsModel { page = typeof(LibraryPage), Title = "Biblioteca", Icon = "", productImage = "icono_biblioteca_menu" });//Icon = "\uf02d"

            menuItems.Add(new MenuItemsModel { page = typeof(SuggestionsPage), Title = "Buzon de Sugerencias", Icon = "", productImage = "icono_sugerencias_menu" });//Icon = "\uf1fa"

            menuItems.Add(new MenuItemsModel { page = typeof(SuggestionsPage), Title = "Cursos en Línea CRE", Icon = "", productImage = "icono_cursos_menu" });//Icon = "\uf19d"

            if (!isAdmin)
            {
                menuItems.Add(new MenuItemsModel { page = null, Title = "Encuesta", Icon = "", productImage = "icono_inicio_menu" });//Icon = "\uf46c"
            }

            menuItems.Add(new MenuItemsModel { page = null, Title = "Cerrar Sesión", Icon = "", productImage = "icono_cerrar_sesion_menu" });//Icon = "\uf2bd"

            listPageMain.ItemsSource = menuItems;

        }
        private async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var option = e.SelectedItem as MenuItemsModel;

            try
            {

                if (option.page != null)
                {
                    IsPresented = false;
                    if (option.index != null)
                        Preferences.Set("MenuIndex", option.index.ToString());
                    else
                        Preferences.Remove("MenuIndex");
                    Detail = new NavigationPage((Page)Activator.CreateInstance(option.page));
                }
                else if (option.Title == "Cerrar Sesión")
                {

                    var result = await DisplayAlert("Confirmar", "¿Estás seguro de cerrar sesión?", "Sí", "No");
                    if (result)
                    {
                        await Utils.Logout();
                    }
                }
                else if (option.Title == "Encuesta")
                {
                    App.Current.MainPage = new NavigationPage(new FormPage());
                }
                await Task.Delay(200);

                listPageMain.SelectedItem = null;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Task.Delay(5000).ContinueWith(t => Utils.checkSession());

            if (!isAdmin)
                Task.Delay(15000).ContinueWith(t => Utils.ShowForm());
        }
    }
}