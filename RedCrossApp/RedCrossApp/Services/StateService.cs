﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class StateService
    {
        private static string EndPoint = "states";

        public async static Task<List<State>> All(string filter = "")
        {

            try
            {
                string Uri = EndPoint;

                if (!string.IsNullOrEmpty(filter))
                    Uri += "?" + filter;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return new List<State>();
                }

                var ResponseModel = JsonConvert.DeserializeObject<StatesModel>(response.Message);

                return ResponseModel.States;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<State>();
            }

        }
    }
}
