﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ActivityService
    {
        private static string EndPoint = "activities";
        public async static Task<List<Activity>> All(string filter = "")
        {

            try
            {
                string Uri = EndPoint;

                if (!string.IsNullOrEmpty(filter))
                    Uri += "?" + filter;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return new List<Activity>();
                }

                var activitiesModel = JsonConvert.DeserializeObject<ActivitiesModel>(response.Message);

                return activitiesModel.Activities;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return new List<Activity>();
            }

        }

        public async static Task<Activity> Store(ActivityRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                if (!response.IsSuccess)
                    return null;

                var Activity = JsonConvert.DeserializeObject<ActivityModel>(response.Message);

                return Activity.activity;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async static Task<Activity> Update(int id, ActivityUpdateRequest request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Put(EndPoint + "/" + id.ToString(), JsonConvert.SerializeObject(request));

                if (!response.IsSuccess)
                    return null;

                var Activity = JsonConvert.DeserializeObject<ActivityModel>(response.Message);

                return Activity.activity;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async static Task<ActivityDetails> Detail(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(EndPoint + "/" + id.ToString() + "/details");

                if (!response.IsSuccess)
                    return new ActivityDetails() 
                    { 
                        Cities = new List<int>(), 
                        States = new List<int>(), 
                        Requirements = new List<ActivityDetailRequirement>()
                    };

                var Activity = JsonConvert.DeserializeObject<ActivityDetailModel>(response.Message);

                return Activity.ActivityDetails;
            }
            catch (Exception)
            {

                return new ActivityDetails()
                {
                    Cities = new List<int>(),
                    States = new List<int>(),
                    Requirements = new List<ActivityDetailRequirement>()
                };
            }
        }

        public async static Task<bool> CheckAndApprove(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(EndPoint + "/" + id.ToString() + "/check-and-complete");

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error in Activity CheckAndApprove: " + ex.Message);
                return false;
            }
        }

    }
}
