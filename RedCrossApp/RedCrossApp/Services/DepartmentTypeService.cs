﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class DepartmentTypeService
    {
        public static async Task<List<TipoDepartamento>> All()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("department-type");

                if (!response.IsSuccess)
                    return new List<TipoDepartamento>();

                var departmentType = JsonConvert.DeserializeObject<DepartmentTypeModel>(response.Message);

                return departmentType.tipoDepartamentos;
            }
            catch (Exception)
            {
                return new List<TipoDepartamento>();
            }
        }
    }
}
