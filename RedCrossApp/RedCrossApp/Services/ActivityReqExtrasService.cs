﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ActivityReqExtrasService
    {
        private static string EndPoint = "activity-req-extras";

        public async static Task<List<ActivityReqExtra>> All(string filter = "")
        {

            try
            {
                string Uri = EndPoint;

                if (!string.IsNullOrEmpty(filter))
                    Uri += "?" + filter;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return null;
                }

                var ResponseModel = JsonConvert.DeserializeObject<ActivityReqExtrasModel>(response.Message);

                return ResponseModel.ActivityReqExtras;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async static Task<ActivityReqExtra> Store(ActivityReqExtraRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                if (!response.IsSuccess)
                    return null;

                var ResponseModel = JsonConvert.DeserializeObject<ActivityReqExtraModel>(response.Message);

                return ResponseModel.ActivityReqExtra;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                if (!response.IsSuccess)
                    return false;

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
