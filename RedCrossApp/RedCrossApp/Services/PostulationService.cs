﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Models.Postulations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class PostulationService
    {
        public static async Task<Requirements> Requirements(int IdActividad)
        {
            try
            {
                var Url = "postulation/requirements/" + IdActividad.ToString();

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<RequirementsModel>(response.Message);

                return ServiceReponse.requirements;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<RequirementCompleteness> RequirementCompleteness(int IdActividad)
        {
            try
            {
                var Url = "postulation/requirement-completeness/" + IdActividad.ToString();

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<RequirementCompletenessModel>(response.Message);

                return ServiceReponse.requirements;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<bool> Postulate(PostulateRequest request)
        {
            try
            {
                var Url = "postulation/postulate";

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(Url, JsonConvert.SerializeObject(request));

                return response.IsSuccess;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<bool> RemovePostule(int IdActividad)
        {
            try
            {
                var Url = "postulation/postulate/" + IdActividad.ToString();

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(Url);

                return response.IsSuccess;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<List<PostulatedCompleteness>> PostulatedCompletenes(int IdActividad)
        {
            try
            {
                var Url = "postulation/postulated-completeness/" + IdActividad.ToString();

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<PostulatedCompletenessModel>(response.Message);

                return ServiceReponse.PostulatedCompleteness;

            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<bool> ApprovePostulations(ApprovePostulationsRequest request)
        {
            try
            {
                var Url = "postulation/approve-postulations";

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(Url, JsonConvert.SerializeObject(request));

                return response.IsSuccess;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
