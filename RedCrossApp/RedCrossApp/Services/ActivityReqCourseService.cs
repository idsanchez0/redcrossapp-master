﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ActivityReqCourseService
    {
        public static string EndPoint = "activity-req-courses";
        public static async Task<List<ActivityReqCourse>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<ActivityReqCoursesModel>(response.Message);

                return ServiceReponse.ActivityReqCourses;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async static Task<ActivityReqCourse> Store(ActivityReqCourseRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                if (!response.IsSuccess)
                    return null;

                var Activity = JsonConvert.DeserializeObject<ActivityReqCourseModel>(response.Message);

                return Activity.ActivityReqCourse;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                if (!response.IsSuccess)
                    return false;

            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }
    }
}
