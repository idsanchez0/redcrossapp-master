﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class CulturalIdentificationService
    {
        public static async Task<List<CulturalIdentification>> All(string filters = "")
        {
            try
            {

                if (App.Current.Properties.ContainsKey("CulturalIdentifications"))
                    return JsonConvert.DeserializeObject<List<CulturalIdentification>>(App.Current.Properties["CulturalIdentifications"].ToString());

                var Url = "cultural-identifications";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<CulturalIdentificationsModel>(response.Message);

                App.Current.Properties["CulturalIdentifications"] = JsonConvert.SerializeObject(ServiceReponse.CulturalIdentifications);

                await App.Current.SavePropertiesAsync();

                return ServiceReponse.CulturalIdentifications;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
