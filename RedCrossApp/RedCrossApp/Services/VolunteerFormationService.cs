﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class VolunteerFormationService
    {
        public static string EndPoint = "auth/volunteer-formations";
        public static async Task<List<VolunteerFormation>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return new List<VolunteerFormation>();

                var ServiceReponse = JsonConvert.DeserializeObject<VolunteerFormationModel>(response.Message);

                return ServiceReponse.VolunteerFormation;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer formations: {ex.Message}");
                return new List<VolunteerFormation>();
            }
        }

        public async static Task<bool> Store(VolunteerFormationRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer formations: {ex.Message}");
                return false;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer formations: {ex.Message}");
                return false;
            }
        }
    }
}
