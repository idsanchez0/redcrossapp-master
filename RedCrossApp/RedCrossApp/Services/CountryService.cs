﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class CountryService
    {
        public static async Task<List<Country>> All(string filters = "")
        {
            try
            {
                if (App.Current.Properties.ContainsKey("Countries"))
                    return JsonConvert.DeserializeObject<List<Country>>(App.Current.Properties["Countries"].ToString());

                var Url = "countries";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return new List<Country>();

                var ServiceReponse = JsonConvert.DeserializeObject<CountriesModel>(response.Message);

                App.Current.Properties["Countries"] = JsonConvert.SerializeObject(ServiceReponse.Countries);

                await App.Current.SavePropertiesAsync();

                return ServiceReponse.Countries;
            }
            catch (Exception)
            {
                return new List<Country>();
            }
        }
    }
}
