﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class VaccineService
    {
        private static string EndPoint = "vaccines";

        public async static Task<List<Vaccine>> All(string filter = "")
        {

            try
            {
                string Uri = EndPoint;

                if (!string.IsNullOrEmpty(filter))
                    Uri += "?" + filter;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return new List<Vaccine>();
                }

                var ResponseModel = JsonConvert.DeserializeObject<VaccinesModel>(response.Message);

                return ResponseModel.vaccines;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<Vaccine>();
            }

        }
    }
}
