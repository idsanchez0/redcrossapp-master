﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class VolunteerVaccineService
    {
        public static string EndPoint = "auth/volunteer-vaccines";
        public static async Task<List<VolunteerVaccine>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return new List<VolunteerVaccine>();

                var ServiceReponse = JsonConvert.DeserializeObject<VolunteerVaccinesModel>(response.Message);

                return ServiceReponse.VolunteerVaccines;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer vaccine: {ex.Message}");
                return new List<VolunteerVaccine>();
            }
        }

        public async static Task<bool> Store(VolunteerVaccineRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer vaccine: {ex.Message}");
                return false;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer vaccine: {ex.Message}");
                return false;
            }
        }
    }
}
