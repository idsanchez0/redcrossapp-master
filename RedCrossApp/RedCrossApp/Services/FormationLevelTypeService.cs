﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class FormationLevelTypeService
    {
        public static async Task<List<FormationLevelType>> All(string filters = "")
        {
            try
            {
                var Url = "formation-level-types";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<FormationLevelTypesModel>(response.Message);

                return ServiceReponse.FormationLevelTypes;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
