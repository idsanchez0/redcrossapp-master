﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class CourseTypeService
    {
        public static async Task<List<CourseType>> All(string filters = "")
        {
            try
            {
                var Url = "course-types";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<CourseTypesModel>(response.Message);

                return ServiceReponse.CourseTypes;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
