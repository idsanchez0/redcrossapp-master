﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class AcademicTitleTypeService
    {
        public static async Task<List<AcademicTitleType>> All(string filters = "")
        {
            try
            {
                var Url = "course-types";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<AcademicTitleTypesModel>(response.Message);

                return ServiceReponse.AcademicTitleTypes;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
