﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class SuggestionService
    {
        public async static Task<bool> SendSuggestion(SuggestionRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post("suggestion", JsonConvert.SerializeObject(Request));

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
