﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ActivityTypeService
    {
        public static async Task<List<Ambito>> All()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("activity-type-scope");

                if (!response.IsSuccess)
                    return null;

                var Scopes = JsonConvert.DeserializeObject<ActivityTypeScopeModel>(response.Message);

                return Scopes.ambitos;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
