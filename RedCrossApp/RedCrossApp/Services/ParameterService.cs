﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ParameterService
    {
        public static async Task<List<Parameter>> All()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("parameters");

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<ParameterModel>(response.Message);

                return ServiceReponse.parameters;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
