﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class DisabilityService
    {
        private static string EndPoint = "disabilities";

        public async static Task<List<Disability>> All(string filter = "")
        {

            try
            {
                string Uri = EndPoint;

                if (!string.IsNullOrEmpty(filter))
                    Uri += "?" + filter;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return new List<Disability>();
                }

                var ResponseModel = JsonConvert.DeserializeObject<DisabilitiesModel>(response.Message);

                return ResponseModel.Disabilities;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<Disability>();
            }

        }
    }
}
