﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class LaboralSituationService
    {
        public static async Task<List<LaboralSituation>> All(string filters = "")
        {
            try
            {
                var Url = "laboral-situations";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<LaboralSituationsModel>(response.Message);

                return ServiceReponse.LaboralSituation;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
