﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ProjectTypeService
    {
        public static string EndPoint = "project-type";
        public static async Task<List<TipoProyecto>> All()
        {

            try
            {
                string Uri = EndPoint;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Uri);

                if (!response.IsSuccess)
                {
                    return null;
                }

                var activitiesModel = JsonConvert.DeserializeObject<ProyectTypesModel>(response.Message);

                return activitiesModel.tipoProyectos;
            }
            catch (Exception)
            {
                return null;
            }

        }

    }
}
