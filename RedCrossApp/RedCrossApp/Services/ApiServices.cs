﻿using RedCrossApp.Models;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{

    class ApiServices
    {

        public static string Url = "http://voluntariado.cruzroja.org.ec/activados/";

        //public static string Url = "http://192.168.100.42:8080/";

        //public static string Url = "http://app2.cruzroja.org.ec:8085/backend/public/";

        //public static string Url = "http://192.168.0.150:8000/";

        private string BaseUrl { get; }
        private HttpClient Client { get; }
        public string Token { get; set; }

        public ApiServices()
        {
            BaseUrl = Url + "api/";

            Client = new HttpClient();

            if (App.Current.Properties.ContainsKey("Token"))
            {
                Token = App.Current.Properties["Token"].ToString();
            }

        }

        public async Task<Response> Get(string uri)
        {
            try
            {

                SetToken();

                Client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                Console.WriteLine($"URL: {uri}");

                HttpResponseMessage response = await Client.GetAsync(BaseUrl + uri);

                string Content = await response.Content.ReadAsStringAsync();

                Console.WriteLine(Content);

                return new Response
                {
                    IsSuccess = response.IsSuccessStatusCode,
                    Message = Content,
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }

        }

        public async Task<Response> Post(string uri, string body)
        {
            try
            {
                SetToken();

                Console.WriteLine("Request body: " + body);

                Console.WriteLine("Request url: " + BaseUrl + uri);

                var data = new StringContent(body, Encoding.UTF8, "application/json");

                Client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                HttpResponseMessage response = await Client.PostAsync(BaseUrl + uri, data);

                string Content = await response.Content.ReadAsStringAsync();

                Console.WriteLine("Response: " + Content);

                return new Response
                {
                    IsSuccess = response.IsSuccessStatusCode,
                    Message = Content,
                };
            } catch (Exception e)
            {

                Console.WriteLine("Error: " + e.Message);

                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                };

            }

        }

        public async Task<Response> PostMultiContent(string uri, MultipartFormDataContent multiContent)
        {
            SetToken();

            var response = await Client.PostAsync(BaseUrl + uri, multiContent);

            string Content = await response.Content.ReadAsStringAsync();

            return new Response
            {
                IsSuccess = response.IsSuccessStatusCode,
                Message = Content,
            };
        }

        public async Task<Response> Delete(string uri)
        {
            SetToken();

            Client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");

            HttpResponseMessage response = await Client.DeleteAsync(BaseUrl + uri);

            string Content = await response.Content.ReadAsStringAsync();

            return new Response
            {
                IsSuccess = response.IsSuccessStatusCode,
                Message = Content,
            };
        }

        public async Task<Response> Put(string uri, string body)
        {
            try
            {
                SetToken();

                Console.WriteLine("Request body: " + body);

                Console.WriteLine("Request url: " + BaseUrl + uri);

                var data = new StringContent(body, Encoding.UTF8, "application/json");

                Client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                HttpResponseMessage response = await Client.PutAsync(BaseUrl + uri, data);

                string Content = await response.Content.ReadAsStringAsync();

                Console.WriteLine("Response: " + Content);

                return new Response
                {
                    IsSuccess = response.IsSuccessStatusCode,
                    Message = Content,
                };
            }
            catch (Exception e)
            {

                Console.WriteLine("Error: " + e.Message);

                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                };

            }

        }

        public async Task<byte[]> GetFile(string uri)
        {
            try
            {

                SetToken();

                Console.WriteLine($"URL: {uri}");

                return await Client.GetByteArrayAsync(BaseUrl + uri);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private void SetToken()
        {
            if (!string.IsNullOrEmpty(Token))
            {
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + Token);
            }
        }

    }
}
