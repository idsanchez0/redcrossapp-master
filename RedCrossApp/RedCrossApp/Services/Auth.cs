﻿using RedCrossApp.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.IO;

namespace RedCrossApp.Services
{
    class Auth
    {

        public async Task<Response> AdminLogin(AdminLoginRequest request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post("auth/admin-login", JsonConvert.SerializeObject(request));

                if (!response.IsSuccess)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = response.Message,
                    };
                }

                var tokenResponse = JsonConvert.DeserializeObject<Token>(response.Message);

                return new Response
                {
                    IsSuccess = true,
                    Result = tokenResponse
                };
            } catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }

        }

        public async Task<Response> VolunteerLogin(VolunteerLoginRequest request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post("auth/volunteer-login", JsonConvert.SerializeObject(request));

                if (!response.IsSuccess)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = response.Message,
                    };
                }

                var tokenResponse = JsonConvert.DeserializeObject<Token>(response.Message);

                return new Response
                {
                    IsSuccess = true,
                    Result = tokenResponse
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message
                };
            }

        }

        public static async Task<MyFile> MyFile()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("auth/my-file");

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<MyfileModel>(response.Message);

                return ServiceReponse.myFile;
            }
            catch (Exception Ex)
            {
                Console.WriteLine("My file service error: " + Ex.Message);
                return null;
            }
        }

        public static async Task<byte[]> MyFilePDF()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                return await apiServices.GetFile("auth/my-file-pdf");

            }
            catch (Exception Ex)
            {
                Console.WriteLine("My file service error: " + Ex.Message);
                return null;
            }
        }

        public static async Task<VolunteerData> VolunteerData()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("auth/volunteer-data");

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<VolunteerDataModel>(response.Message);

                return ServiceReponse.Volunteer;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                return null;
            }
        }

        public static async Task<VolunteerData> UpdateVolunteerData(object request)
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post("auth/volunteer-data", JsonConvert.SerializeObject(request));

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<VolunteerDataModel>(response.Message);

                return ServiceReponse.Volunteer;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                return null;
            }
        }

        public static async Task<AdminData> AdminData()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("auth/admin-data");

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<AdminDataModel>(response.Message);

                return ServiceReponse.AdminData;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                return null;
            }
        }
        public static async Task<Response> AdminMe()
        {

            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("auth/admin-me");

                if (!response.IsSuccess)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = response.Message,
                    };
                }

                var isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool)App.Current.Properties["IsAdmin"] : false;

                if(isAdmin)
                {
                    var UserModel = JsonConvert.DeserializeObject<AdminUserModel>(response.Message);

                    return new Response
                    {
                        IsSuccess = true,
                        Result = UserModel.user
                    };
                }
                else
                {
                    var UserModel = JsonConvert.DeserializeObject<VolunteerModel>(response.Message);

                    return new Response
                    {
                        IsSuccess = true,
                        Result = UserModel.user
                    };
                }

            } 
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                };
            }
            
        }

        public static async Task<bool> Logout(string Id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete("auth/delete-token/" + Id);

                return response.IsSuccess;

            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return false;
            }
        }

        public static async Task<bool> ResetPassword(string mail)
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post("auth/recover-password", JsonConvert.SerializeObject(new VolunteerLoginRequest() { email = mail}));

                return response.IsSuccess;
            }
            catch (Exception Ex)
            {
                Console.Error.WriteLine(Ex.Message);
                return false;
            }

        }

    }
}
