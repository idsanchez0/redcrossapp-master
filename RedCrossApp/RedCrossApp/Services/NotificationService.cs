﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class NotificationService
    {
        private static string EndPoint = "notifications";
        public static async Task<List<Notification>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<NotificationsModel>(response.Message);

                return ServiceReponse.Notifications;
            }
            catch (Exception Ex)
            {
                Console.Error.WriteLine("Notification Error: " + Ex.Message);
                return null;
            }
        }

        public static async Task Recieved(NotificationRecievedRequest Request)
        {
            try
            {
                var Url = EndPoint + "/received";

                ApiServices apiServices = new ApiServices();

                await apiServices.Post(Url, JsonConvert.SerializeObject(Request));

            }
            catch (Exception Ex)
            {
                Console.Error.WriteLine("Notification Error: " + Ex.Message);
            }
        }

        public static async Task<List<MyNotification>> MyNotifications(string filters = "")
        {
            try
            {
                string Url = "my-notifications";

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<MyNotificationsModel>(response.Message);

                return ServiceReponse.MyNotifications;
            }
            catch (Exception Ex)
            {
                Console.Error.WriteLine("Notification Error: " + Ex.Message);
                return new List<MyNotification>();
            }
        }
    }
}
