﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class VolunteerDisabilityService
    {
        public static string EndPoint = "auth/volunteer-disabilities";
        public static async Task<List<VolunteerDisability>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return new List<VolunteerDisability>();

                var ServiceReponse = JsonConvert.DeserializeObject<VolunteerDisabilitiesModel>(response.Message);

                return ServiceReponse.VolunteerDisability;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer disability: {ex.Message}");
                return new List<VolunteerDisability>();
            }
        }

        public async static Task<bool> Store(VolunteerDisabilityRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer disability: {ex.Message}");
                return false;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                return response.IsSuccess;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error volunteer disability: {ex.Message}");
                return false;
            }
        }
    }
}
