﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class SegUserService
    {
        public static async Task<List<SegUserList>> All()
        {
            try
            {

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get("seg-users");

                if (!response.IsSuccess)
                    return null;

                var segUsers = JsonConvert.DeserializeObject<SegUserModel>(response.Message);

                return segUsers.segUsers;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
