﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedCrossApp.Services
{
    class ActivityReqFormationService
    {
        public static string EndPoint = "activity-req-formations";
        public static async Task<List<ActivityReqFormation>> All(string filters = "")
        {
            try
            {
                var Url = EndPoint;

                if (!string.IsNullOrEmpty(filters))
                    Url += "?" + filters;

                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Get(Url);

                if (!response.IsSuccess)
                    return null;

                var ServiceReponse = JsonConvert.DeserializeObject<ActivityReqFormationsModel>(response.Message);

                return ServiceReponse.ActivityReqFormations;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async static Task<ActivityReqFormation> Store(ActivityReqFormationRequest Request)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Post(EndPoint, JsonConvert.SerializeObject(Request));

                if (!response.IsSuccess)
                    return null;

                var Activity = JsonConvert.DeserializeObject<ActivityReqFormationModel>(response.Message);

                return Activity.ActivityReqFormation;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async static Task<bool> Delete(int id)
        {
            try
            {
                ApiServices apiServices = new ApiServices();

                Response response = await apiServices.Delete(EndPoint + "/" + id.ToString());

                if (!response.IsSuccess)
                    return false;

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
