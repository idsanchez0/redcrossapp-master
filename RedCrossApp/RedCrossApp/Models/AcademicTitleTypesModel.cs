﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class AcademicTitleType
    {
        public int id_tipo_titulo_academico { get; set; }
        public int id_tipo_nivel_formacion { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class AcademicTitleTypesModel
    {
        public List<AcademicTitleType> AcademicTitleTypes { get; set; }
    }

}
