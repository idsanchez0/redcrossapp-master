﻿namespace RedCrossApp.Models
{
    class Response
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }
    }

    public class Requirement
    {
        public string Key { get; set; }
        public float Value { get; set; }
    }
}
