﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RedCrossApp.Models
{
    class LoginModel
    {
    }

    class AdminLoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string device_name { get; set; }
        public string firebase_token { get; set; }
        public string platform { get; set; }
    }

    class VolunteerLoginRequest
    {
        public string email { get; set; }
        public string device_name { get; set; }
        public string firebase_token { get; set; }
        public string platform { get; set; }
    }

    public class Token
    {
        public string token { get; set; }
    }

    public class Office365User
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public ImageSource Photo { get; set; }
    }
}
