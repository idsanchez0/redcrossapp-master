﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class VolunteerDisability
    {
        public int id_voluntario { get; set; }
        public int id_tipo_discapacidad { get; set; }
        public int porcentaje { get; set; }
        public object descripcion { get; set; }
        public string nombre { get; set; }
    }

    public class VolunteerDisabilitiesModel
    {
        public List<VolunteerDisability> VolunteerDisability { get; set; }
    }

    public class VolunteerDisabilityRequest
    {
        public int id_tipo_discapacidad { get; set; }
        public int porcentaje { get; set; }
        public string descripcion { get; set; }
    }
}
