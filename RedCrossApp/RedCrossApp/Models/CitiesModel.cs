﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class CitiesModel
    {
        public List<City> Cities { get; set; }
    }

    public class City
    {
        public int id_ciudad { get; set; }
        public int id_estado { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public bool sede_nacional { get; set; }

    }
}
