﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class Activity
    {
        public int id_actividad { get; set; }
        public int id_tipo_actividad { get; set; }
        public int id_proyecto { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public int duracion { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string login_responsable { get; set; }
        public string responsable { get; set; }
        public object responsable_email { get; set; }
        public object fecha_registro { get; set; }
        public int seguro { get; set; }
        public int id_tipo_estado_seguro { get; set; }
        public int? id_estado_realizacion { get; set; }
        public int? id_ciudad_realizacion { get; set; }
        public object latitud { get; set; }
        public object longitud { get; set; }
        public bool multi_provincia { get; set; }
        public bool multi_canton { get; set; }
        public int id_tipo_departamento { get; set; }
        public object adjunto { get; set; }
        public bool adjunto_comprimido { get; set; }
        public string adjunto_size_mb { get; set; }
        public string color_until_start { get; set; }
        public object estado_postulacion { get; set; }
        public TipoActividad tipo_actividad { get; set; }
        public TipoEstadoSeguro estado_seguro { get; set; }
    }

    public class ActivitiesModel
    {
        public List<Activity> Activities { get; set; }
    }

    public class ActivityModel
    {
        public Activity activity { get; set; }
    }

    public class ActivityRequest
    {
        public int id_tipo_actividad { get; set; }
        public int id_proyecto { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int duracion { get; set; }
        public string responsable { get; set; }
        public string responsable_email { get; set; }
        public int id_tipo_departamento { get; set; }
        public bool multi_provincia { get; set; }
        public bool multi_canton { get; set; }
        public bool postulacion { get; set; }
        public List<int> estados { get; set; }
        public List<int> ciudades { get; set; }
    }

    public class ActivityUpdateRequest
    {
        public int id_tipo_actividad { get; set; }
        public int id_proyecto { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int duracion { get; set; }
        public string responsable { get; set; }
        public string responsable_email { get; set; }
        public int id_tipo_departamento { get; set; }
        public bool multi_provincia { get; set; }
        public bool multi_canton { get; set; }
        public bool postulacion { get; set; }
        public List<int> estados { get; set; }
        public List<int> ciudades { get; set; }
    }

    public class TipoEstadoSeguro
    {
        public int id_tipo_estado_seguro { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string ambito { get; set; }
        public string color { get; set; }
    }
}
