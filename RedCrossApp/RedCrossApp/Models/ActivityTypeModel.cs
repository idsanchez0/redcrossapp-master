﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class TipoActividad
    {
        public int id_tipo_actividad { get; set; }
        public string nombre { get; set; }
        public string ambito { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class Ambito
    {
        public string nombre { get; set; }
        public List<TipoActividad> tipoActividad { get; set; }
    }

    public class ActivityTypeScopeModel
    {
        public List<Ambito> ambitos { get; set; }
    }

}
