﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class PivotEnfermedad
    {
        public int? id_voluntario { get; set; }
        public int? id_tipo_enfermedad { get; set; }
        public object descripcion { get; set; }
    }

    public class PivotSituacionLaboral
    {
        public int? id_voluntario { get; set; }
        public int? id_tipo_situacion_laboral { get; set; }
        public object descripcion { get; set; }
    }

    public class Enfermedad
    {
        public int? id_tipo_enfermedad { get; set; }
        public object nombre { get; set; }
        public bool? estado { get; set; }
        public int? orden { get; set; }
        public PivotEnfermedad pivot { get; set; }
    }

    public class SituacionLaboral
    {
        public int? id_tipo_situacion_laboral { get; set; }
        public object nombre { get; set; }
        public bool? estado { get; set; }
        public int? orden { get; set; }
        public PivotSituacionLaboral pivot { get; set; }
    }


    public class VolunteerData
    {
        public int id_voluntario { get; set; }
        public bool eliminado { get; set; }
        public object codigo { get; set; }
        public object pin_seguridad { get; set; }
        public bool ficha_actualizada { get; set; }
        public int id_tipo_identificacion { get; set; }
        public string identificacion { get; set; }
        public int id_estado_voluntario { get; set; }
        public string fotografia { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public int? id_nacionalidad { get; set; }
        public object lugar_nacimiento { get; set; }
        public DateTime? fecha_nacimiento { get; set; }
        public object sexo { get; set; }
        public object genero { get; set; }
        public int? id_tipo_identificacion_cultural { get; set; }
        public int? id_tipo_estado_civil { get; set; }
        public string direccion_domicilio { get; set; }
        public object direccion_numeracion { get; set; }
        public object direccion_domicilio_secundaria { get; set; }
        public object direccion_sector { get; set; }
        public object direccion_referencia { get; set; }
        public string personal_telef_conv { get; set; }
        public string personal_telef_cel { get; set; }
        public object trabajo_dedicacion { get; set; }
        public string trabajo_lugar { get; set; }
        public string trabajo_telef_conv { get; set; }
        public string trabajo_ext { get; set; }
        public string email { get; set; }
        public string siniestro_contacto { get; set; }
        public string siniestro_parentesco { get; set; }
        public string siniestro_telef_conv { get; set; }
        public string siniestro_telef_cel { get; set; }
        public string grupo_sanguineo { get; set; }
        public object factor_rh { get; set; }
        public int id_pais { get; set; }
        public int id_estado { get; set; }
        public int id_ciudad { get; set; }
        public DateTime fecha_ingreso { get; set; }
        public object fecha_salida { get; set; }
        public object fecha_reingreso { get; set; }
        public DateTime? fecha_registro { get; set; }
        public object fecha_modificacion { get; set; }
        public object fecha_eliminacion { get; set; }
        public int duracion_horas { get; set; }
        public DateTime? fecha_ultima_actividad { get; set; }
        public int? id_tipo_voluntario { get; set; }
        public bool carnetizado { get; set; }
        public DateTime? fecha_carnet_entrega { get; set; }
        public DateTime? fecha_carnet_caducidad { get; set; }
        public int? id_programa { get; set; }
        public string rep_leg_contacto { get; set; }
        public string rep_leg_telef_conv { get; set; }
        public string rep_leg_telef_cel { get; set; }
        public object fotografia_org { get; set; }
        public object brigada { get; set; }
        public object twitter { get; set; }
        public object facebook { get; set; }
        public object solicitud_carnet { get; set; }
        public bool fotografia_editada { get; set; }
        public bool fotografia_comprimida { get; set; }
        public string fotografia_size_mb { get; set; }
        public string cuenta_365 { get; set; }
        public object cuenta_365_password { get; set; }
        public string notas { get; set; }
        public object talla_camiseta { get; set; }
        public object talla_pantalon { get; set; }
        public object talla_chompa { get; set; }
        public object talla_calzado { get; set; }
        public object talla_peto { get; set; }
        public object experiencia_voluntario_lugar { get; set; }
        public object experiencia_voluntario_desc { get; set; }
        public int cargas_fam_numero { get; set; }
        public object cargas_fam_edades { get; set; }
        public bool cargas_fam_con_discapacidad { get; set; }
        public object cargas_fam_tipo_discapacidad { get; set; }
        public object cargas_fam_porcentaje_discapacidad { get; set; }
        public object peso_kg { get; set; }
        public object talla_m { get; set; }
        public object hist_clin_fecha_examen { get; set; }
        public bool hist_clin_tratamiento { get; set; }
        public bool hist_clin_cambio_salud { get; set; }
        public object hist_clin_cambio_salud_desc { get; set; }
        public object hist_clin_alergias { get; set; }
        public object hist_clin_medicacion { get; set; }
        public bool hist_clin_carnet_discapacidad { get; set; }
        public object vac_numero_carnet { get; set; }
        public bool? seg_seguro_social { get; set; }
        public bool? seg_seguro_privado { get; set; }
        public object seg_seguro_privado_desc { get; set; }
        public List<Enfermedad> enfermedades { get; set; }
        public List<SituacionLaboral> situaciones_laborales { get; set; }
    }



    public class VolunteerDataModel
    {
        public VolunteerData Volunteer { get; set; }
    }

    public class VolunteerClinicRequest
    {
        public float talla_m { get; set; }
        public float peso_kg { get; set; }
        public string hist_clin_alergias { get; set; }
        public bool hist_clin_tratamiento { get; set; }
        public string hist_clin_medicacion { get; set; }
        public object otra_enfermedad { get; set; }
        public List<int> enfermedades { get; set; }
    }
    public class VolunteerExpirienceRequest
    {
        public string trabajo_dedicacion { get; set; }
        public string trabajo_lugar { get; set; }
        public string trabajo_telef_conv { get; set; }
        public object otra_situacion_laboral { get; set; }
        public List<int> situaciones_laborales { get; set; }
    }
    public class VolunteerContactRequest
    {
        public string email { get; set; }
        public string personal_telef_conv { get; set; }
        public string personal_telef_cel { get; set; }
        public string direccion_domicilio { get; set; }
        public string direccion_domicilio_secundaria { get; set; }
        public string direccion_numeracion { get; set; }
        public string direccion_sector { get; set; }
        public string direccion_referencia { get; set; }
        public string siniestro_contacto { get; set; }
        public string siniestro_parentesco { get; set; }
        public string siniestro_telef_cel { get; set; }
    }

    public class VolunteerPersonalRequest
    {
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string genero { get; set; }
        public string sexo { get; set; }
        public int? id_tipo_estado_civil { get; set; }
        public int? id_nacionalidad { get; set; }
        public string lugar_nacimiento { get; set; }
        public int? id_tipo_identificacion_cultural { get; set; }
        public string grupo_sanguineo { get; set; }
        public string factor_rh { get; set; }
        public bool seg_seguro_social { get; set; }
        public bool seg_seguro_privado { get; set; }
        public string seg_seguro_privado_desc { get; set; }
        public bool hist_clin_carnet_discapacidad { get; set; }
    }

}
