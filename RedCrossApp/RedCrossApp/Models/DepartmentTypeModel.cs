﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class TipoDepartamento
    {
        public int id_tipo_departamento { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class DepartmentTypeModel
    {
        public List<TipoDepartamento> tipoDepartamentos { get; set; }
    }

}
