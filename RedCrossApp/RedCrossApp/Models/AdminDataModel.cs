﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class AdminData
    {
        public string names { get; set; }
        public string Image { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public int PublishedActivities { get; set; }
        public string WorkedHours { get; set; }
        public int PendingActivities { get; set; }
        public DateTime LastActivityDate { get; set; }
    }

    public class AdminDataModel
    {
        public AdminData AdminData { get; set; }
    }

}
