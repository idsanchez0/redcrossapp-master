﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    
    public class NivelAcademico
    {
        public int id_tipo_nivel_formacion { get; set; }
        public string nombre { get; set; }
        public bool requiere_descripcion { get; set; }
        public int numero_registros { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
        public string categoria { get; set; }
    }

    public class TituloAcademico
    {
        public int id_tipo_titulo_academico { get; set; }
        public int id_tipo_nivel_formacion { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
        public NivelAcademico nivel_academico { get; set; }
    }

    public class VolunteerFormation
    {
        public int id_voluntario_formacion { get; set; }
        public int id_voluntario { get; set; }
        public int id_tipo_titulo_academico { get; set; }
        public object id_tipo_subarea_conocimiento { get; set; }
        public object descripcion { get; set; }
        public bool? formacion_en_curso { get; set; }
        public int? nivel_formacion_en_curso { get; set; }
        public object modalidad_formacion_en_curso { get; set; }
        public object nivel_escrito { get; set; }
        public object nivel_oral { get; set; }
        public TituloAcademico titulo_academico { get; set; }
    }

    public class VolunteerFormationModel
    {
        public List<VolunteerFormation> VolunteerFormation { get; set; }
    }

    public class VolunteerFormationRequest
    {
        public int? id_voluntario_formacion { get; set; }
        public int id_tipo_titulo_academico { get; set; }
        public object descripcion { get; set; }
        public bool? formacion_en_curso { get; set; }
        public int? nivel_formacion_en_curso { get; set; }
        public object modalidad_formacion_en_curso { get; set; }
        public object nivel_escrito { get; set; }
        public object nivel_oral { get; set; }
    }
}
