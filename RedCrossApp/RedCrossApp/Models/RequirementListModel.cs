﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class RequirementListModel
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }
    }
}
