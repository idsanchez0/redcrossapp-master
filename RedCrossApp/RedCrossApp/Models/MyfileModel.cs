﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class MyFile
    {
        public string names { get; set; }
        public string Image { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public int Id { get; set; }
        public int WorkedHours { get; set; }
        public string Dignity { get; set; }
        public string DateLastActivity { get; set; }
        public int ToCompleteActivities { get; set; }
    }

    public class MyfileModel
    {
        public MyFile myFile { get; set; }
    }

}
