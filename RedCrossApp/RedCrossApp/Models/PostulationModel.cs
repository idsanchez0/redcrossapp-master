﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    class PostulationModel
    {
        public bool CheckedPostulation { get; set; }
        public bool CanApprove { get; set; }
        public string Name { get; set; }
        public string Completition { get; set; }
        public DateTime LastActivityDate { get; set; }
        public int voluntario_id { get; set; }
    }
}
