﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    class ContactsModel
    {
        public List<Contact> contacts { get; set; }
    }

    public class Contact
    {
        public int id_contacto { get; set; }
        public int id_ciudad { get; set; }
        public string nombre { get; set; }
        public string cargo { get; set; }
        public string email { get; set; }
    }
}
