﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class UserToken
    {
        public int id { get; set; }
        public string tokenable_type { get; set; }
        public string tokenable_id { get; set; }
        public string name { get; set; }
        public List<string> abilities { get; set; }
        public DateTime? last_used_at { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
    public class SegEstado
    {
        public int id_estado { get; set; }
        public List<string> ids_ciudad { get; set; }
    }
    public class SegUser
    {
        public string login { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string active { get; set; }
        public string priv_admin { get; set; }
        public List<SegEstado> estados { get; set; }
        public List<UserToken> tokens { get; set; }
    }

    public class AdminUserModel
    {
        public SegUser user { get; set; }
    }
}
