﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class CulturalIdentification
    {
        public int id_tipo_identificacion_cultural { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class CulturalIdentificationsModel
    {
        public List<CulturalIdentification> CulturalIdentifications { get; set; }
    }

}
