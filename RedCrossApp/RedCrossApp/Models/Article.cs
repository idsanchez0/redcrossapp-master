﻿using RedCrossApp.ViewModels;
using Xamarin.Forms.Internals;

namespace RedCrossApp.Models
{
    /// <summary>
    /// Model for Article templates.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class Article : BaseViewModel
    {

        public string ImagePath { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public object Link { get; set; }

    }
}