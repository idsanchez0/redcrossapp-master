﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class Notification
    {
        public int id_notificacion { get; set; }
        public string imagen { get; set; }
        public string texto_corto { get; set; }
        public string texto_largo { get; set; }
        public int? id_estado { get; set; }
        public int? id_ciudad { get; set; }
        public object url_adjunto { get; set; }
        public bool estado { get; set; }
        public int? orden { get; set; }
        public DateTime? fecha_registro { get; set; }
        public object login_responsable { get; set; }
    }

    public class NotificationsModel
    {
        public List<Notification> Notifications { get; set; }
    }

    public class NotificationRecievedRequest
    {
        public int id_notificacion { get; set; }
    }

}
