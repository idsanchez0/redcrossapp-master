﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class ActivityReqFormation
    {
        public int id_actividad_req_formacion { get; set; }
        public int id_actividad { get; set; }
        public int id_tipo_titulo_academico { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public string descripcion { get; set; }
        public int peso { get; set; }
        public AcademicTitleType academic_title_type { get; set; }
    }

    public class ActivityReqFormationsModel
    {
        public List<ActivityReqFormation> ActivityReqFormations { get; set; }
    }

    public class ActivityReqFormationModel
    {
        public ActivityReqFormation ActivityReqFormation { get; set; }
    }

    public class ActivityReqFormationRequest
    {
        public int id_actividad { get; set; }
        public int id_tipo_titulo_academico { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public string descripcion { get; set; }
        public int peso { get; set; }
    }

}
