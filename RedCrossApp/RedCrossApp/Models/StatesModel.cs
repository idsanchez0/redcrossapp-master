﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class StatesModel
    {
        public List<State> States { get; set; }
    }

    public class State
    {
        public int id_estado { get; set; }
        public int id_pais { get; set; }
        public string nombre { get; set; }
        public string nombre_alterno { get; set; }
        public string email { get; set; }
        public string ciudad { get; set; }
        public string direccion { get; set; }
        public string telefono1 { get; set; }
        public string telefono2 { get; set; }
        public string fax { get; set; }
        public bool estado { get; set; }
        public bool sede_nacional { get; set; }
        public bool receptar_solicitudes { get; set; }
        public List<City> cities { get; set; }

    }
}
