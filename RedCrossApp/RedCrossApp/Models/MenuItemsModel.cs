﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RedCrossApp.Models
{
    public class MenuItemsModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public Type page { get; set; }
        public int? index { get; set; }
        public string productImage { get; set; }
    }
}
