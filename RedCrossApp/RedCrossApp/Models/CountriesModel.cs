﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Country
    {
        public int id_pais { get; set; }
        public string nombre { get; set; }
        public string codigo { get; set; }
        public string nacionalidad { get; set; }
        public bool sede_activa { get; set; }
    }

    public class CountriesModel
    {
        public List<Country> Countries { get; set; }
    }

}
