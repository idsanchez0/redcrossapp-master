﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class MyNotification
    {
        public int id_voluntario_notificacion { get; set; }
        public int id_voluntario { get; set; }
        public int id_notificacion { get; set; }
        public int estado { get; set; }
        public Notification notification { get; set; }
    }

    public class MyNotificationsModel
    {
        public List<MyNotification> MyNotifications { get; set; }
    }

    public class UserNotifications
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Author { get; set; }
        public bool HasLink { get; set; }
        public DateTime? DateTime { get; set; }
    }

}
