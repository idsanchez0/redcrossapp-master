﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class ActivityDetailRequirement
    {
        public string type { get; set; }
        public int value { get; set; }

        internal bool Any()
        {
            throw new NotImplementedException();
        }
    }

    public class ActivityDetails
    {
        public List<ActivityDetailRequirement> Requirements { get; set; }
        public List<int> Cities { get; set; }
        public List<int> States { get; set; }
    }

    public class ActivityDetailModel
    {
        public ActivityDetails ActivityDetails { get; set; }
    }

}
