﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class Parameter
    {
        public int id_parametro { get; set; }
        public string nombre { get; set; }
        public string titulo { get; set; }
        public string valor { get; set; }
        public string categoria { get; set; }
        public int estado { get; set; }
    }

    public class ParameterModel
    {
        public List<Parameter> parameters { get; set; }
    }
}
