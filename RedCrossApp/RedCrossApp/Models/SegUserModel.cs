﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class SegUserList
    {
        public string login { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string active { get; set; }
        public string priv_admin { get; set; }
    }

    public class SegUserModel
    {
        public List<SegUserList> segUsers { get; set; }
    }
}
