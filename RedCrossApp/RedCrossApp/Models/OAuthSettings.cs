﻿namespace RedCrossApp.Models
{
    public static class OAuthSettings
    {
        public const string ApplicationId = "e7571701-4cb3-48a5-96bd-572b309f1eba";
        //public const string ApplicationId = "473917a0-f36d-43ed-b1e5-7d0a8f004d68";
        public const string Scopes = "User.Read Calendars.Read";
        public const string RedirectUri = "msauth://com.RedCross.Activados";
    }
}