﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class FormationLevelType
    {
        public int id_tipo_nivel_formacion { get; set; }
        public string nombre { get; set; }
        public bool requiere_descripcion { get; set; }
        public int numero_registros { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
        public string categoria { get; set; }
        public List<AcademicTitleType> academic_title_types { get; set; }
    }

    public class FormationLevelTypesModel
    {
        public List<FormationLevelType> FormationLevelTypes { get; set; }
    }

}
