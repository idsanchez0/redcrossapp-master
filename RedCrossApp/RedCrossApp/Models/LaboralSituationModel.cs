﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class LaboralSituation
    {
        public int id_tipo_situacion_laboral { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class LaboralSituationsModel
    {
        public List<LaboralSituation> LaboralSituation { get; set; }
    }

}
