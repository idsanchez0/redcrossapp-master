﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    class EventModel
    {
        public Activity Activity { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Postulation { get; set; }
    }
}
