﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models.Postulations
{
    public class PostulateRequest
    {
        public int id_actividad { get; set; }
    }
    public class ApprovePostulationsRequest : PostulateRequest
    {
        public List<int> approved { get; set; }
    }
}
