﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models.Postulations
{
    public class RequirementCompleteness : Requirements
    {
        public int? Completeness { get; set; }
        public int? Postulation { get; set; }
        public bool CanPostulate { get; set; }
    }
    public class RequirementCompletenessModel
    {
        public RequirementCompleteness requirements { get; set; }
    }
}
