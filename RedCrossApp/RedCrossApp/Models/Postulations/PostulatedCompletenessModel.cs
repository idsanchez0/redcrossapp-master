﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models.Postulations
{
    public class PostulatedCompleteness
    {
        public int id_voluntario { get; set; }
        public DateTime fecha_ultima_actividad { get; set; }
        public string nombre { get; set; }
        public int id_tipo_estado_postulacion { get; set; }
        public int Completeness { get; set; }
    }

    public class PostulatedCompletenessModel
    {
        public List<PostulatedCompleteness> PostulatedCompleteness { get; set; }
    }

}
