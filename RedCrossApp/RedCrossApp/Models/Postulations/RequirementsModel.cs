﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models.Postulations
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class RequirementsCourse
    {
        public int id_tipo_actividad_requisito { get; set; }
        public int id_tipo_curso { get; set; }
        public string nombre { get; set; }
        public int peso { get; set; }
    }

    public class RequirementsExtra
    {
        public int id_tipo_actividad_requisito { get; set; }
        public string nombre { get; set; }
        public int peso { get; set; }
        public string tipo_rango { get; set; }
        public string valor1 { get; set; }
        public object valor2 { get; set; }
    }

    public class RequirementsFormation
    {
        public int id_tipo_actividad_requisito { get; set; }
        public int id_tipo_titulo_academico { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int peso { get; set; }
    }

    public class Requirements
    {
        public List<RequirementsCourse> Courses { get; set; }
        public List<RequirementsExtra> Extras { get; set; }
        public List<RequirementsFormation> Formations { get; set; }
        public string ProjectType { get; set; }
        public string Project { get; set; }
        public string Department { get; set; }
        public int totalWeight { get; set; }
    }

    public class RequirementsModel
    {
        public Requirements requirements { get; set; }
    }

}
