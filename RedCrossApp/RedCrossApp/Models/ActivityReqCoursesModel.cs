﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class ActivityReqCourse
    {
        public int id_actividad_req_curso { get; set; }
        public int id_actividad { get; set; }
        public int id_tipo_curso { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public int peso { get; set; }
        public CourseType course_type { get; set; }
    }

    public class ActivityReqCoursesModel
    {
        public List<ActivityReqCourse> ActivityReqCourses { get; set; }
    }

    public class ActivityReqCourseModel
    {
        public ActivityReqCourse ActivityReqCourse { get; set; }
    }

    public class ActivityReqCourseRequest
    {
        public int id_actividad { get; set; }
        public int id_tipo_curso { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public int peso { get; set; }
    }
}
