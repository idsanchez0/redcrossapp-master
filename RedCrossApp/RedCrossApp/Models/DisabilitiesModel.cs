﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class Disability
    {
        public int id_tipo_discapacidad { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class DisabilitiesModel
    {
        public List<Disability> Disabilities { get; set; }
    }
}
