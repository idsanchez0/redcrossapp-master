﻿using System.Collections.Generic;

namespace RedCrossApp.Models
{
    public class Proyecto
    {
        public int id_proyecto { get; set; }
        public int id_tipo_proyecto { get; set; }
        public string cod_proyecto { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public bool estado { get; set; }
        public string orden { get; set; }
    }

    public class TipoProyecto
    {
        public int id_tipo_proyecto { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
        public List<Proyecto> proyectos { get; set; }
    }

    public class ProyectTypesModel
    {
        public List<TipoProyecto> tipoProyectos { get; set; }
    }

}
