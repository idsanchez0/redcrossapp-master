﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class CourseType
    {
        public int id_tipo_curso { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class CourseTypesModel
    {
        public List<CourseType> CourseTypes { get; set; }
    }

}
