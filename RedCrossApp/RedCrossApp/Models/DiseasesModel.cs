﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class Disease
    {
        public int id_tipo_enfermedad { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class DiseasesModel
    {
        public List<Disease> Diseases { get; set; }
    }
}
