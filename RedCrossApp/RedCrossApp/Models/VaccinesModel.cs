﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class Vaccine
    {
        public int id_tipo_vacuna { get; set; }
        public string nombre { get; set; }
        public bool estado { get; set; }
        public int orden { get; set; }
    }

    public class VaccinesModel
    {
        public List<Vaccine> vaccines { get; set; }
    }
}
