﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{
    public class VolunteerVaccine
    {
        public int id_voluntario { get; set; }
        public int id_tipo_vacuna { get; set; }
        public object dosis1 { get; set; }
        public object dosis2 { get; set; }
        public object dosis3 { get; set; }
        public object descripcion { get; set; }
        public string nombre { get; set; }
    }

    public class VolunteerVaccinesModel
    {
        public List<VolunteerVaccine> VolunteerVaccines { get; set; }
    }

    public class VolunteerVaccineRequest
    {
        public int id_tipo_vacuna { get; set; }
        public string descripcion { get; set; }
    }
}
