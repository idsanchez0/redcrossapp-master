﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedCrossApp.Models
{

    public class ActivityReqExtra
    {
        public int id_actividad_req_extras { get; set; }
        public int id_actividad { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public string tipo_rango { get; set; }
        public string valor1 { get; set; }
        public string valor2 { get; set; }
        public int peso { get; set; }
    }

    public class ActivityReqExtrasModel
    {
        public List<ActivityReqExtra> ActivityReqExtras { get; set; }
    }

    public class ActivityReqExtraModel
    {
        public ActivityReqExtra ActivityReqExtra { get; set; }
    }

    public class ActivityReqExtraRequest
    {
        public int id_actividad { get; set; }
        public int id_tipo_actividad_requisito { get; set; }
        public string tipo_rango { get; set; }
        public string valor1 { get; set; }
        public string valor2 { get; set; }
        public int peso { get; set; }
    }

}
