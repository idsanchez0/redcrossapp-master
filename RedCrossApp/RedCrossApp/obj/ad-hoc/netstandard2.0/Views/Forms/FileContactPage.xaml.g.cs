//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("RedCrossApp.Views.Forms.FileContactPage.xaml", "Views/Forms/FileContactPage.xaml", typeof(global::RedCrossApp.Views.Forms.FileContactPage))]

namespace RedCrossApp.Views.Forms {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("Views\\Forms\\FileContactPage.xaml")]
    public partial class FileContactPage : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry FullNameEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry HomePhoneNumberEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry PersonalPhoneOneEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry MainStreetEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry SecondStreetEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry HomeNumberEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry PlaceEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry ReferenceEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry EmergencyContactEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry RelationEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::RedCrossApp.Controls.BorderlessEntry EmergencyContactPhoneEntry;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(FileContactPage));
            FullNameEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "FullNameEntry");
            HomePhoneNumberEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "HomePhoneNumberEntry");
            PersonalPhoneOneEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "PersonalPhoneOneEntry");
            MainStreetEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "MainStreetEntry");
            SecondStreetEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "SecondStreetEntry");
            HomeNumberEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "HomeNumberEntry");
            PlaceEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "PlaceEntry");
            ReferenceEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "ReferenceEntry");
            EmergencyContactEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "EmergencyContactEntry");
            RelationEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "RelationEntry");
            EmergencyContactPhoneEntry = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::RedCrossApp.Controls.BorderlessEntry>(this, "EmergencyContactPhoneEntry");
        }
    }
}
