﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Requirements;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Requirements
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DefaultListRequirementsPage : ContentPage
    {
        public string SecondTitle = "";
        public int RequirementKind = 0;
        public float Weight = 0;
        public DefualtListRequirementsViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="DefualtRequirementPage" /> class.
        /// </summary>
        public DefaultListRequirementsPage()
        {
            InitializeComponent();

            _viewModel = new DefualtListRequirementsViewModel();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.RequirementKind = RequirementKind;
            _viewModel.Weight = Weight;

            LabelTitle.Text = SecondTitle;
            _viewModel.LoadData();

        }

    }
}