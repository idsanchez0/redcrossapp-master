﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Requirements;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Requirements
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DefualtRequirementPage : ContentPage
    {
        public string SecondTitle = "";
        public int RequirementKind = 0;
        public float Weight = 0;
        public Activity Activity = null;
        public DefualtRequirementViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="DefualtRequirementPage" /> class.
        /// </summary>
        public DefualtRequirementPage()
        {
            InitializeComponent();

            _viewModel = new DefualtRequirementViewModel();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.RequirementKind = RequirementKind;
            _viewModel.Weight = Weight;
            _viewModel.Activity = Activity;

            LabelTitle.Text = SecondTitle;

            if (RequirementKind == 8)
            {
                FirstValEntry.IsEnabled = false;
                SecondValEntry.IsEnabled = false;
            }

        }

        void OpenStartDatePicker(object sender, EventArgs args)
        {
            if (RequirementKind == 8)
                StartDatePicker.IsOpen = !StartDatePicker.IsOpen;
        }

        void StartDateSelected(object sender, EventArgs args)
        {
            FirstValEntry.Text = StartDatePicker.Date.ToString("yyyy-MM-dd");
        }

        void OpenEndDatePicker(object sender, EventArgs args)
        {
            if (RequirementKind == 8)
                EndDatePicker.IsOpen = !StartDatePicker.IsOpen;
        }

        void EndDateSelected(object sender, EventArgs args)
        {
            SecondValEntry.Text = StartDatePicker.Date.ToString("yyyy-MM-dd");
        }

    }
}