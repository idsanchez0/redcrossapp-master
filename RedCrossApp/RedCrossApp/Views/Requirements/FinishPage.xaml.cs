﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Requirements;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Requirements
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FinishPage : ContentPage
    {
        public FinishViewModel _viewModel;
        public Activity Activity { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FinishPage" /> class.
        /// </summary>
        public FinishPage()
        {
            InitializeComponent(); 
            _viewModel = new FinishViewModel();
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.Activity = Activity;

        }
    }
}