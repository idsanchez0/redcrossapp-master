﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Servicios : ContentPage
    {
        public Servicios()
        {
            InitializeComponent();
        }
        //
        private async void Button1Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConoceCr());
        }
        private async void Button2Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BeneficiosVol());
        }
        private async void Button3Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Servicios());
        }
        private async void Button4Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new JuntasProvinciales());
        }
        private async void Button5Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginView());
        }
        protected void buttonfacebook(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.facebook.com/cruzrojaecuador/"));
        }
        protected void buttontwitter(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://twitter.com/cruzrojaecuador"));
        }
        protected void buttoninstagram(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttonyoutube(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.youtube.com/user/Cruzrojaec"));
        }
        protected void buttonlinkedin(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttontiktok(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.tiktok.com/@cruzrojaecuador"));
        }
    }
}