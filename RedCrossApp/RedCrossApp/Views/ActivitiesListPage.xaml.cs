﻿using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivitiesListPage : ContentPage
    {
        private ActivitiesListViewModel _viewModel;
        public ActivitiesListPage()
        {
            InitializeComponent();
            _viewModel = new ActivitiesListViewModel(Navigation);
            BindingContext = _viewModel;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }

        public void SearchTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;

            if(string.IsNullOrEmpty(searchBar.Text))
                _viewModel.LoadData();

        }
    }
}