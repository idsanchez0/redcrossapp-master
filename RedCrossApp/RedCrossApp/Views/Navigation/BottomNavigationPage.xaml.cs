﻿using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Navigation
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BottomNavigationPage : TabbedPage
    {
        public BottomNavigationPage()
        {

            InitializeComponent();

            int.TryParse(Preferences.Get("MenuIndex", "0"), out int page);

            Preferences.Remove("MenuIndex");

            bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool)(App.Current.Properties["IsAdmin"]) : false;

            Children.Add(new Home.HomePage()
            {
                Title = "Home",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf015", Size = 16 }
            });

            Children.Add(new CalendarPage()
            {
                Title = "Calendario",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf073", Size = 16 }
            });


            if (!isAdmin)
            {
                Children.Add(new TabbedNotificationsPage()
                {
                    Title = "Notificaciones",
                    IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf0f3", Size = 16 }
                });
            }

            if (page != 0)
            {
                var pages = Children.GetEnumerator();

                for (int i = 0;i < page; i++)
                {
                    pages.MoveNext();
                }

                CurrentPage = pages.Current;
            }
            
        }
    }
}