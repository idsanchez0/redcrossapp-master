﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Views.Requirements;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Navigation
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequirementsButtomNavigationPage : TabbedPage
    {
        public Activity Activity;
        public RequirementsButtomNavigationPage(Activity activity)
        {
            
            InitializeComponent();

            Activity = activity;

            try 
            {
                List<Requirement> Requirements = App.Current.Properties.ContainsKey("Requirements") ? 
                    JsonConvert.DeserializeObject<List<Requirement>>((string)App.Current.Properties["Requirements"]) : null;

                if(!Requirements.Any())
                    throw new Exception();

                foreach(Requirement requirement in Requirements)
                {
                    switch (requirement.Key)
                    {
                        case "Courses":
                            //Crud
                            Children.Add( new DefaultListRequirementsPage()
                            { 
                                Title = "Cursos", 
                                Weight = requirement.Value,
                                RequirementKind = 1, 
                                SecondTitle = "Cursos Necesarios (" + requirement.Value.ToString() + " %)", 
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "Academic":
                            //Crud
                            Children.Add(new DefaultListRequirementsPage()
                            { 
                                Title = "Formación Académica",
                                Weight = requirement.Value,
                                RequirementKind = 2,
                                SecondTitle = "Formación Académica (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "Eni":
                            //Crud
                            Children.Add(new DefaultListRequirementsPage()
                            { 
                                Title = "Formación Eni",
                                Weight = requirement.Value,
                                RequirementKind = 3,
                                SecondTitle = "Formación Eni (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "Languages":
                            //Crud pending
                            Children.Add(new DefaultListRequirementsPage()
                            { 
                                Title = "Idiomas",
                                Weight = requirement.Value,
                                RequirementKind = 4,
                                SecondTitle = "Idiomas (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "Ages":
                            Children.Add(new DefualtRequirementPage() 
                            { 
                                Title = "Edad",
                                Weight = requirement.Value,
                                RequirementKind = 6,
                                Activity = Activity,
                                SecondTitle = "Edad (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "ActivityYears":
                            Children.Add(new DefualtRequirementPage() 
                            { 
                                Title = "Años",
                                Weight = requirement.Value,
                                RequirementKind = 7,
                                Activity = Activity,
                                SecondTitle = "Años (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                        case "DateLastActivity":
                            Children.Add(new DefualtRequirementPage() 
                            { 
                                Title = "Fecha Ultima Actividad",
                                Weight = requirement.Value,
                                RequirementKind = 8,
                                Activity = Activity,
                                SecondTitle = "Fecha Ultima Actividad (" + requirement.Value.ToString() + " %)",
                                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 } 
                            });
                            break;
                    }
                }

                Children.Add(new FinishPage() 
                { 
                    Title = "Finalizar", 
                    IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf058", Size = 16 },
                    Activity = Activity
                });

            }
            catch (Exception)
            {
                App.Current.MainPage = new MenuPage();
            }

            
        }
    }
}