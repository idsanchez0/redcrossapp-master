﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Navigation
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedNotificationsPage : TabbedPage
    {
        public TabbedNotificationsPage()
        {
            InitializeComponent();

            Children.Add(new NotificationsPage()
            {
                Title = "No Leidas",
                StatusFilter = "1",
                //IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf015", Size = 16 }
            });

            Children.Add(new NotificationsPage()
            {
                Title = "Leidas",
                StatusFilter = "2",
                //IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf073", Size = 16 }
            });

            Children.Add(new NotificationsPage()
            {
                Title = "Todas",
                //IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf073", Size = 16 }
            });
        }
    }
}