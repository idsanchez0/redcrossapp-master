﻿using RedCrossApp.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostulationsListPage : ContentPage
    {
        private PostulationsListViewModel _viewModel;
        public PostulationsListPage()
        {
            InitializeComponent();
            _viewModel = new PostulationsListViewModel(Navigation);
            BindingContext = _viewModel;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }

        public void SearchTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;

            if(string.IsNullOrEmpty(searchBar.Text))
                _viewModel.LoadData();

        }
    }
}