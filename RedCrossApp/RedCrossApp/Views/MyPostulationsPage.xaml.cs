﻿using RedCrossApp.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyPostulationsPage : ContentPage
    {
        private MyPostulationsViewModel _viewModel;
        public MyPostulationsPage()
        {
            InitializeComponent();
            _viewModel = new MyPostulationsViewModel(Navigation);
            BindingContext = _viewModel;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }

        public void SearchTextChanged(object sender, EventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;

            if(string.IsNullOrEmpty(searchBar.Text))
                _viewModel.LoadData();

        }
    }
}