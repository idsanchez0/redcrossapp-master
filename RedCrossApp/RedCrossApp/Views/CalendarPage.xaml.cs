﻿using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarPage : ContentPage
    {
        private CalendarViewModel _viewModel;
        public CalendarPage()
        {
            InitializeComponent();

            _viewModel = new CalendarViewModel(Navigation);
            BindingContext = _viewModel;

            calendar.Culture = new CultureInfo("es-ES", false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }
    }
}