﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostulationsDetailPage : ContentPage
    {
        private Activity activity;
        private PostulationsDetailViewModel _viewModel;
        public PostulationsDetailPage(Activity _activity)
        {
            activity = _activity;
            InitializeComponent();
            _viewModel = new PostulationsDetailViewModel(activity, Navigation);
            BindingContext = _viewModel;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }

    }
}