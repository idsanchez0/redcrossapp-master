﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {
        public LoginView()
        {

            BindingContext = new LoginViewModel();
            InitializeComponent();
        }

        private async void OnSignIn(object sender, EventArgs e)
        {
            
            UserDialogs.Instance.ShowLoading("Ingresando...", MaskType.Black);

            try
            {
                await (Application.Current as App).SignIn();

                var User = await (Application.Current as App).GetUserInfo();
                //"texto cambiado";//Preferences.Get("TokenFirebase", "");
                //var User = new Office365User() { Email = "edison.guevara@voluntarioscre.org", Name = "Test Test" };

                if (User != null)
                {
                    var AuthService = new Auth();

                    var LoginResponse = await AuthService.VolunteerLogin(new VolunteerLoginRequest 
                    { 
                        email = User.Email,
                        device_name = DeviceInfo.Name,
                        firebase_token = Preferences.Get("TokenFirebase", ""),             
                        platform = Utils.Platform()
                    });
                    

                    if (!LoginResponse.IsSuccess)
                    {
                        throw new Exception("Su correo no esta registrado en el Sistema de la CRE");
                    }
                    else
                    {
                        var TokenResponse = (Token)LoginResponse.Result;

                        await Utils.VolunteerLoginCompleted(TokenResponse.token);

                        Application.Current.MainPage = new MenuPage();

                    }

                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Authentication Error", ex.Message, "OK");
            }

            UserDialogs.Instance.HideLoading();
        }

        private async void RecoverPassword(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecoverPasswordPage());
        }
        //
        private async void Button1Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConoceCr());
        }
        private async void Button2Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BeneficiosVol());
        }
        private async void Button3Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Servicios());
        }
        private async void Button4Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new JuntasProvinciales());
        }
        private async void Button5Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginView());
        }
        protected void buttonfacebook(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.facebook.com/cruzrojaecuador/"));
        }
        protected void buttontwitter(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://twitter.com/cruzrojaecuador"));
        }
        protected void buttoninstagram(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttonyoutube(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.youtube.com/user/Cruzrojaec"));
        }
        protected void buttonlinkedin(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttontiktok(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.tiktok.com/@cruzrojaecuador"));
        }
        //
        protected void ButtonFormVoluntario(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://voluntariado.cruzroja.org.ec/form_vol_voluntario_solicitud/"));
        }
    }
}