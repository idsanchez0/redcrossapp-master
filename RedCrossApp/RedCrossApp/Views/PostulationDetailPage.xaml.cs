﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostulationDetailPage : ContentPage
    {
        private Activity activity;
        private PostulationDetailViewModel _viewModel;
        public PostulationDetailPage(Activity _activity)
        {
            activity = _activity;
            InitializeComponent();
            _viewModel = new PostulationDetailViewModel(activity, Navigation);
            BindingContext = _viewModel;
        }
    }
}