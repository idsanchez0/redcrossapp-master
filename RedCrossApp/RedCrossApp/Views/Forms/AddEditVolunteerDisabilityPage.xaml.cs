﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditVolunteerDisabilityPage : ContentPage
    {
        private AddEditVolunteerDisabilityViewModel _viewModel;
        public AddEditVolunteerDisabilityPage(VolunteerDisability volunteerDisability = null)
        {
            InitializeComponent();
            _viewModel = new AddEditVolunteerDisabilityViewModel() 
            { 
                Navigation = Navigation,
                VolunteerDisability = volunteerDisability
            };

            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _viewModel.LoadData();

        }
    }
}