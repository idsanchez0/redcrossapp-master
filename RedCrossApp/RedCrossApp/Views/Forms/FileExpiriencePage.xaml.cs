﻿using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileExpiriencePage : ContentPage
    {
        private FileExpirienceViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="FileExpiriencePage" /> class.
        /// </summary>
        public FileExpiriencePage()
        {
            InitializeComponent();

            _viewModel = new FileExpirienceViewModel();
            BindingContext = _viewModel;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }
    }
}