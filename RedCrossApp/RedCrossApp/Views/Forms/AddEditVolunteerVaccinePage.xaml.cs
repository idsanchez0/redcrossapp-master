﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditVolunteerVaccinePage : ContentPage
    {
        private AddEditVolunteerVaccineViewModel _viewModel;
        public AddEditVolunteerVaccinePage(int? IdVaccine = null)
        {
            InitializeComponent();
            _viewModel = new AddEditVolunteerVaccineViewModel() 
            { 
                Navigation = Navigation, 
                IdVaccine = IdVaccine
            };

            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _viewModel.LoadData();

        }
    }
}