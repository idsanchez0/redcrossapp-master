﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditRequiredCoursesPage : ContentPage
    {
        public AddEditRequiredCourseViewModel _viewModel;
        public float WeightLeft;
        public AddEditRequiredCoursesPage(float weightLeft)
        {
            InitializeComponent();
            WeightLeft = weightLeft;
            _viewModel = new AddEditRequiredCourseViewModel();
            BindingContext = _viewModel;
            _viewModel.WeightLeft = WeightLeft;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();

            _viewModel.LoadData();

        }
    }
}