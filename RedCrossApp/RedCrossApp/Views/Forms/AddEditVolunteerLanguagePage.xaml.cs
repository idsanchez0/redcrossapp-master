﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditVolunteerLanguagePage : ContentPage
    {
        private AddEditVolunteerLanguageViewModel _viewModel;
        public AddEditVolunteerLanguagePage(VolunteerFormation volunteerFormation = null)
        {
            InitializeComponent();
            _viewModel = new AddEditVolunteerLanguageViewModel() 
            { 
                Navigation = Navigation, 
                VolunteerFormation = volunteerFormation 
            };

            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _viewModel.LoadData();

        }
    }
}