﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivityEditPage : ContentPage
    {
        private ActivityEditViewModel _viewModel;
        private Activity Activity;
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityRegisterPage" /> class.
        /// </summary>
        public ActivityEditPage(Activity _actividad)
        {

            InitializeComponent();
            Activity = _actividad;
            _viewModel = new ActivityEditViewModel(Activity);
            BindingContext = _viewModel;

            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-ES");
            StartDatePicker.MinimumDate = DateTime.Today.AddHours(-5);
            EndDatePicker.MinimumDate = DateTime.Today.AddHours(-5);

        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _viewModel.LoadData();

            StartDatePicker.Date = Activity.fecha_inicio;
            EndDatePicker.Date = Activity.fecha_fin;

            StartTimePicker.Time = new TimeSpan(8,0,0);
            EndTimePicker.Time = new TimeSpan(17,0,0);

        }

        #region Start DateTime
        void OpenStartDatePicker(object sender, EventArgs args)
        {
            StartDatePicker.IsOpen = !StartDatePicker.IsOpen;
        }

        void StartDateSelected(object sender, EventArgs args)
        {
            StartDatePickerEntry.Text = StartDatePicker.Date.ToString("yyyy-MM-dd");
        }

        void OpenStartTimePicker(object sender, EventArgs args)
        {
            StartTimePicker.IsOpen = !StartTimePicker.IsOpen;
        }

        void StartTimeSelected(object sender, EventArgs args)
        {
            StartTimePickerEntry.Text = StartTimePicker.Time.ToString(@"hh\:mm");
        }
        #endregion

        #region End DateTime
        void OpenEndDatePicker(object sender, EventArgs args)
        {
            EndDatePicker.IsOpen = !EndDatePicker.IsOpen;
        }

        void EndDateSelected(object sender, EventArgs args)
        {
            EndDatePickerEntry.Text = EndDatePicker.Date.ToString("yyyy-MM-dd");
        }

        void OpenEndTimePicker(object sender, EventArgs args)
        {
            EndTimePicker.IsOpen = !EndTimePicker.IsOpen;
        }

        void EndTimeSelected(object sender, EventArgs args)
        {
            EndTimePickerEntry.Text = EndTimePicker.Time.ToString(@"hh\:mm");
        }
        #endregion
    }
}