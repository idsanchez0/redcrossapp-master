﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditRequiredFormationPage : ContentPage
    {
        public AddEditRequiredFormationViewModel _viewModel;

        private int RequiredKind;
        private float WeightLeft;
        public AddEditRequiredFormationPage(int _requiredKind, float weightLeft)
        {
            InitializeComponent();

            _viewModel = new AddEditRequiredFormationViewModel();
            BindingContext = _viewModel;
            WeightLeft = weightLeft;
            RequiredKind = _requiredKind;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();

            _viewModel.RequiredKind = RequiredKind;
            _viewModel.WeightLeft = WeightLeft;
            _viewModel.LoadData();

        }
    }
}