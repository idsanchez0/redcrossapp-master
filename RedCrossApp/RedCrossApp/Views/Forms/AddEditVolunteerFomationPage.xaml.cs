﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEditVolunteerFomationPage : ContentPage
    {
        private AddEditVolunteerFomationViewModel _viewModel;
        public AddEditVolunteerFomationPage(VolunteerFormation volunteerFormation = null)
        {
            InitializeComponent();
            _viewModel = new AddEditVolunteerFomationViewModel() 
            { 
                Navigation = Navigation, 
                VolunteerFormation = volunteerFormation 
            };

            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();
            _viewModel.LoadData();

        }
    }
}