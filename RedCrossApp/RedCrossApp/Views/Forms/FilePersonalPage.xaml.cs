﻿using RedCrossApp.ViewModels.Forms;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Forms
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FilePersonalPage : ContentPage
    {
        private FilePersonalViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="FilePersonalPage" /> class.
        /// </summary>
        public FilePersonalPage()
        {
            InitializeComponent();

            _viewModel = new FilePersonalViewModel();
            BindingContext = _viewModel;

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();
        }

    }
}