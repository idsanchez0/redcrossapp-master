﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LibraryPage : ContentPage
    {
        public LibraryPage()
        {
            InitializeComponent();

            webView.Source = (string)(App.Current.Properties.ContainsKey("libraryLink") ? App.Current.Properties["libraryLink"] : "");

        }

        async void webViewNavigating(object sender, WebNavigatingEventArgs e)
        {
            //await DisplayAlert("URL", e.Url, "ok");
            if (e.Url.Contains("/get/"))
            {
                // Retrieving the URL  
                var file = new Uri(e.Url);

                // Open PDF URL with device browser to download  
                await Launcher.OpenAsync(file);

                // Cancel the navigation on click actions   
                // (retains in the same page.)  
                e.Cancel = true;
            }
        }

        async void OnBackButtonClicked(object sender, EventArgs e)
        {
            if (webView.CanGoBack)
            {
                webView.GoBack();
            }
            else
            {
                await Navigation.PopAsync();
            }
        }

    }
}