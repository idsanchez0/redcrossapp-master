﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : ContentPage
    {
        public class ImageInformation
        {
            public ImageSource _Image { get; set; }
        }
        private ObservableCollection<ImageInformation> imageCollection;

        public ObservableCollection<ImageInformation> ImageCollection
        {
            get { return imageCollection; }
            set
            {
                imageCollection = value;
                OnPropertyChanged();
            }
        }


        public Inicio()
        {
            InitializeComponent();
            BindingContext = this;
            ImageCollection = new ObservableCollection<ImageInformation>
            {
                new ImageInformation{_Image="foto_presidente.png"},
                new ImageInformation{_Image="imagen_referencia_noticias.png"},
                new ImageInformation{_Image="foto_presidente.png"}
            };
        }
        //
        private async void Button1Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConoceCr());
        }
        private async void Button2Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BeneficiosVol());
        }
        private async void Button3Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Servicios());
        }
        private async void Button4Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new JuntasProvinciales());
        }
        private async void Button5Click(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginView());
        }
        protected void buttonfacebook(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.facebook.com/cruzrojaecuador/"));
        }
        protected void buttontwitter(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://twitter.com/cruzrojaecuador"));
        }
        protected void buttoninstagram(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttonyoutube(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.youtube.com/user/Cruzrojaec"));
        }
        protected void buttonlinkedin(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttontiktok(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.tiktok.com/@cruzrojaecuador"));
        }
    }
}