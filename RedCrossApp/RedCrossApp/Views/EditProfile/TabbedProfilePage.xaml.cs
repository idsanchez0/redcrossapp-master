﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.EditProfile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedProfilePage : ContentPage
    {
        public TabbedProfilePage()
        {
            InitializeComponent();
        }
    }
}