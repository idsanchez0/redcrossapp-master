﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using RedCrossApp.Views.Forms;
using RedCrossApp.Views.MyFilePage;

namespace RedCrossApp.Views.EditProfile
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlusBottomNavigationPage : TabbedPage
    {
        public PlusBottomNavigationPage()
        {
            InitializeComponent();

            Children.Add(new VolunteerAcademicListPage()
            {
                Title = "Títulos Académicos",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf508", Size = 16 }
            });
            Children.Add(new VolunteerLanguageListPage()
            {
                Title = "Idiomas",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf1ab", Size = 16 }
            });
            Children.Add(new VolunteerVaccineListPage()
            {
                Title = "Inmunizaciones",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf48e", Size = 16 }
            });
            Children.Add(new VolunteerDisabilityListPage()
            {
                Title = "Discapacidades",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf193", Size = 16 }
            });

        }

    }
}