﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using RedCrossApp.Views.Forms;
using RedCrossApp.Views.MyFilePage;

namespace RedCrossApp.Views.EditProfile
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BottomNavigationPage : TabbedPage
    {
        public BottomNavigationPage()
        {
            InitializeComponent();

            Children.Add(new FileExpiriencePage()
            {
                Title = "Laboral",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf508", Size = 16 }
            });

            Children.Add(new FileContactPage()
            {
                Title = "Contacto",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf500", Size = 16 }
            });

            Children.Add(new FilePersonalPage()
            {
                Title = "Personal",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf4fc", Size = 16 }
            });

            Children.Add(new FileClinicPage()
            {
                Title = "Historia Clínica",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf0f0", Size = 16 }
            });

            Children.Add(new TabbedProfilePage()
            {
                Title = "Más",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf067", Size = 16 }
            });
            /*
            Children.Add(new VolunteerAcademicListPage()
            {
                Title = "Títulos Académicos",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf508", Size = 16 }
            });
            Children.Add(new VolunteerLanguageListPage()
            {
                Title = "Idiomas",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf1ab", Size = 16 }
            });
            Children.Add(new VolunteerVaccineListPage()
            {
                Title = "Inmunizaciones",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf48e", Size = 16 }
            });
            Children.Add(new VolunteerDisabilityListPage()
            {
                Title = "Discapacidades",
                IconImageSource = new FontImageSource() { FontFamily = "FontAwesome", Glyph = "\uf193", Size = 16 }
            });
            */
        }

        protected override void OnCurrentPageChanged()
        {

            base.OnCurrentPageChanged();

            if (CurrentPage is TabbedProfilePage)
            {
                Navigation.PushAsync(new PlusBottomNavigationPage());

                var pages = Children.GetEnumerator();

                pages.MoveNext();

                CurrentPage = pages.Current;
            }

            
        }
    }
}