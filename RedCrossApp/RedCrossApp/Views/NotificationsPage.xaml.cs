﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificationsPage : ContentPage
    {

        private NotificationsViewModel _viewModel;

        public string StatusFilter { get; set; }

        public NotificationsPage()
        {
            InitializeComponent();

            _viewModel = new NotificationsViewModel();
            BindingContext = _viewModel;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.StatusFilter = StatusFilter;
            _viewModel.LoadData();
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            UserNotifications notification = e.Item as UserNotifications;
            Preferences.Set("CurrentNews", notification.Id.ToString());
            App.Current.MainPage = new MenuPage();
        }
    }
}
