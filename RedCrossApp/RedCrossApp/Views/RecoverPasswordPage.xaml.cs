﻿using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecoverPasswordPage : ContentPage
    {
        private RecoverPasswordViewModel _viewModel;
        public RecoverPasswordPage()
        {
            _viewModel = new RecoverPasswordViewModel(Navigation);
            BindingContext = _viewModel;
            InitializeComponent();
        }
    }
}