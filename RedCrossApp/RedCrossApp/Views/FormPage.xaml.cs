﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormPage : ContentPage
    {
        public FormPage()
        {
            InitializeComponent();

            webView.Source = (string)(App.Current.Properties.ContainsKey("formLink") ? App.Current.Properties["formLink"] : "");

            App.Current.Properties["FormDate"] = DateTime.Now.AddDays(90).ToString("yyyy-MM-dd") + "T00:00:00+0:00";
            App.Current.SavePropertiesAsync();
        }

        async void webViewNavigating(object sender, WebNavigatingEventArgs e)
        {
            //await DisplayAlert("URL", e.Url, "ok");
            if (e.Url.Contains("/get/"))
            {
                // Retrieving the URL  
                var file = new Uri(e.Url);

                // Open PDF URL with device browser to download  
                await Launcher.OpenAsync(file);

                // Cancel the navigation on click actions   
                // (retains in the same page.)  
                e.Cancel = true;
            }
        }

        async void CloseForm(object sender, EventArgs e)
        {
            var response = await DisplayAlert("Alerta", "Si cierra esta pantalla no podra volver a la misma", "Ok","Cancelar");
            if(response)
                App.Current.MainPage = new MenuPage();
        }

    }
}