﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels.MyFilePage;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.MyFilePage
{
    /// <summary>
    /// Page to add business details such as name, email address, and phone number.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VolunteerLanguageListPage : ContentPage
    {

        private VolunteerLanguageListViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="VolunteerAcademicListPage" /> class.
        /// </summary>
        public VolunteerLanguageListPage()
        {
            InitializeComponent();

            _viewModel = new VolunteerLanguageListViewModel()
            {
                Navigation = Navigation
            };
            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.LoadData();

        }

    }
}