﻿using RedCrossApp.ViewModels.MyFilePage;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.MyFilePage
{
    /// <summary>
    /// Page to show the health profile.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HealthProfilePage : ContentPage
    {
        private HealthProfileViewModel _viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="HealthProfilePage" /> class.
        /// </summary>
        public HealthProfilePage()
        {
            InitializeComponent();

            _viewModel = new HealthProfileViewModel(Navigation);

            BindingContext = _viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //_viewModel.LoadData();
        }
    }
}