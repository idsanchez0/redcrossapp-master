﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views.Home
{
    /// <summary>
    /// Page to show the article tile
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleTilePage" /> class.
        /// </summary>
        public HomePage()
        {
            InitializeComponent();
        }
        protected void buttonfacebook(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new System.Uri("https://www.facebook.com/cruzrojaecuador/"));
        }
        protected void buttontwitter(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://twitter.com/cruzrojaecuador"));
        }
        protected void buttoninstagram(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttonyoutube(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.youtube.com/user/Cruzrojaec"));
        }
        protected void buttonlinkedin(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.instagram.com/cruz_roja_ecuatoriana/"));
        }
        protected void buttontiktok(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new LoginView());
            Device.OpenUri(new Uri("https://www.tiktok.com/@cruzrojaecuador"));
        }
    }
}