﻿using RedCrossApp.Models;
using RedCrossApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RedCrossApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivityDetailPage : ContentPage
    {
        private Activity activity;
        private ActivityDetailViewModel _viewModel;
        public ActivityDetailPage(Activity _activity)
        {
            activity = _activity;
            InitializeComponent();
            _viewModel = new ActivityDetailViewModel(activity, Navigation);
            BindingContext = _viewModel;
        }
    }
}