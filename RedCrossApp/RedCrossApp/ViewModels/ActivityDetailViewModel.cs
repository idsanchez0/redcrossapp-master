﻿using RedCrossApp.Models;
using RedCrossApp.Models.Postulations;
using RedCrossApp.Services;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class ActivityDetailViewModel : BaseViewModel
    {
        private Activity activity;
        private INavigation Navigation;

        private string name;
        private string activityId;
        private string detail;
        private string ambit;
        private string activityType;
        private string projectType;
        private string project;
        private DateTime start_date;
        private DateTime end_date;
        private string duration;
        private string department;
        private string responsable;
        private string emailResponsable;
        private string location;
        private string age;
        private string years;
        private string courses;
        private string formation;
        private string completeness;
        private bool canPostulate = false;
        private bool canEdit = false;
        private bool showCompleteness = false;

        public string nombre
        {
            get { return name; }
            set { SetValue(ref this.name, value); }
        }
        public string ActivityId
        {
            get { return activityId; }
            set { SetValue(ref this.activityId, value); }
        }
        public string detalle
        {
            get { return detail; }
            set { SetValue(ref this.detail, value); }
        }
        public string ambito
        {
            get { return ambit; }
            set { SetValue(ref this.ambit, value); }
        }
        public string ActivityType
        {
            get { return activityType; }
            set { SetValue(ref this.activityType, value); }
        }
        public string ProjectType
        {
            get { return projectType; }
            set { SetValue(ref this.projectType, value); }
        }
        public string Project
        {
            get { return project; }
            set { SetValue(ref this.project, value); }
        }
        public DateTime fecha_inicio
        {
            get { return start_date; }
            set { SetValue(ref this.start_date, value); }
        }
        public DateTime fecha_fin
        {
            get { return end_date; }
            set { SetValue(ref this.end_date, value); }
        }
        public string Duration
        {
            get { return duration; }
            set { SetValue(ref this.duration, value); }
        }
        public string Department
        {
            get { return department; }
            set { SetValue(ref this.department, value); }
        }
        public string Responsable
        {
            get { return responsable; }
            set { SetValue(ref this.responsable, value); }
        }
        public string EmailResponsable
        {
            get { return emailResponsable; }
            set { SetValue(ref this.emailResponsable, value); }
        }
        public string localizacion
        {
            get { return location; }
            set { SetValue(ref this.location, value); }
        }
        public string edad
        {
            get { return age; }
            set { SetValue(ref this.age, value); }
        }
        public string anios
        {
            get { return years; }
            set { SetValue(ref this.years, value); }
        }
        public string cursos
        {
            get { return courses; }
            set { SetValue(ref this.courses, value); }
        }
        public string formacion
        {
            get { return formation; }
            set { SetValue(ref this.formation, value); }
        }
        public string idoneidad
        {
            get { return completeness; }
            set { SetValue(ref this.completeness, value); }
        }
        public bool CanPostulate
        {
            get { return canPostulate; }
            set { SetValue(ref this.canPostulate, value); }
        }
        public bool CanEdit
        {
            get { return canEdit; }
            set { SetValue(ref this.canEdit, value); }
        }
        public bool ShowCompleteness
        {
            get { return showCompleteness; }
            set { SetValue(ref this.showCompleteness, value); }
        }

        public ICommand PostulateCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand LocationCommand { get; private set; }
        public ActivityDetailViewModel(Activity _activity, INavigation _navigation)
        {
            activity = _activity;
            Navigation = _navigation;

            PostulateCommand = new Command(async () => await PostulateMethod());
            EditCommand = new Command(async () => await EditMethod());
            LocationCommand = new Command(async () => await LocationMethod());

            LoadData();
        }

        private async void LoadData()
        {
            
            try
            {
                nombre = activity.nombre;
                ActivityId = activity.id_actividad.ToString();
                detalle = activity.descripcion;

                ambito = activity.tipo_actividad.ambito;
                ActivityType = activity.tipo_actividad.nombre;

                fecha_inicio = activity.fecha_inicio;
                fecha_fin = activity.fecha_fin;

                Duration = activity.duracion.ToString();
                Responsable = activity.responsable;
                EmailResponsable = activity.responsable_email?.ToString() ?? "";

                bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool)(App.Current.Properties["IsAdmin"]) : false;

                List<RequirementsExtra> Extras;
                List<RequirementsCourse> Courses;
                List<RequirementsFormation> Formations;
                if (isAdmin)
                {
                    CanEdit = true;

                    var _requirements = await PostulationService.Requirements(activity.id_actividad);
                    ProjectType = _requirements.ProjectType;
                    Project = _requirements.Project;
                    Department = _requirements.Department;

                    Extras = _requirements.Extras;
                    Courses = _requirements.Courses;
                    Formations = _requirements.Formations;
                }
                else
                {
                    ShowCompleteness = true;
                    var _requirements = await PostulationService.RequirementCompleteness(activity.id_actividad);
                    idoneidad = _requirements.Completeness.ToString() + " %";
                    ProjectType = _requirements.ProjectType;
                    Project = _requirements.Project;
                    Department = _requirements.Department;
                    CanPostulate = _requirements.CanPostulate;

                    Extras = _requirements.Extras;
                    Courses = _requirements.Courses;
                    Formations = _requirements.Formations;
                }

                edad = anios = cursos = formacion = "Sin Restricción";

                if (Extras.Any())
                {
                    foreach (RequirementsExtra _extra in Extras)
                    {
                        switch (_extra.id_tipo_actividad_requisito)
                        {
                            case 6: //ages
                                edad = FormatRangeType(_extra.tipo_rango, _extra.valor1, _extra.valor2);
                                break;
                            case 7: //years
                                anios = FormatRangeType(_extra.tipo_rango, _extra.valor1, _extra.valor2);
                                break;
                            case 8: // Last Activity Date
                                break;

                        }
                    }
                }

                if (Courses.Any())
                {
                    List<string> _courses = new List<string>();
                    foreach (RequirementsCourse _item in Courses)
                    {
                        _courses.Add(_item.nombre);
                    }

                    cursos = string.Join(", ", _courses);
                }

                if (Formations.Any())
                {
                    List<string> _formation = new List<string>();
                    foreach (RequirementsFormation _item in Formations)
                    {
                        _formation.Add(_item.nombre);
                    }

                    formacion = string.Join(", ", _formation);
                }
            }

            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

        }

        private string FormatRangeType(string RangeType, string Value1, Object Value2)
        {
            switch (RangeType)
            {
                case "gt":
                    return $"Mayor a {Value1}";
                case "lt":
                    return $"Menor a {Value1}";
                case "eq":
                    return $"Igual a {Value1}";
                case "bw":
                    return $"Entre a {Value1} y " + Value2.ToString();
                default:
                    return "";
            }
        }

        private async Task PostulateMethod()
        {
            var response = await App.Current.MainPage.DisplayAlert("Alerta", "Al postular a esta actividad se compromete a participar de la misma en las condiciones establecidas por CRE", "Aceptar","Cancelar");

            if(response)
            {
                var Postulation = await PostulationService.Postulate(new PostulateRequest { 
                    id_actividad = activity.id_actividad
                });

                if(Postulation)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Su postulación fue registrada exitosamente", "Aceptar");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo postular a la actividad.", "Aceptar");

            }
        }

        private async Task EditMethod()
        {
            await Navigation.PushAsync(new ActivityEditPage(activity));
        }
        private async Task LocationMethod()
        {
            await Launcher.OpenAsync(new Uri("https://www.google.com/maps/search/?api=1&query=" + activity.latitud + "," + activity.longitud));
        }
    }
}
