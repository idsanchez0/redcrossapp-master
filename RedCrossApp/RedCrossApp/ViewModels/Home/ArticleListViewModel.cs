﻿using RedCrossApp.Services;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using RedCrossApp.Models;
using Xamarin.Essentials;
using System;

namespace RedCrossApp.ViewModels.Home
{
    /// <summary>
    /// ViewModel for article list page.
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class ArticleListViewModel : BaseViewModel
    {
        #region Fields

        private ObservableCollection<Article> featuredStories;
        private int selectedIndex = 0;
        private string titleText;
        private string mainText = "";
        private bool isFirstArticle = true;
        private bool isNotFirstArticle = false;
        private bool hasLink = false;
        private string link;

        private string mainImage;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance for the <see cref="ArticleListViewModel" /> class.
        /// </summary>
        public ArticleListViewModel()
        {
            LoadData();

            this.MainImage = ApiServices.Url + "header/HomeImage.png";

            this.LinkCommand = new Command(linkMethod);
        }
        #endregion

        public async void LoadData()
        {
            try
            {

                bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool)(App.Current.Properties["IsAdmin"]) : false;

                var _Articles = new ObservableCollection<Article>();

                _Articles.Add(new Article
                {
                    ImagePath = ApiServices.Url + "header/imagen_1.jpg",
                    Name = "Mensaje del Presidente",
                    Description = "",
                    Link = null
                });

                int MainNew = 0;

                try
                {
                    object CurrentNews = Preferences.Get("CurrentNews", "0");
                    Console.WriteLine("Current news: " + CurrentNews.ToString());
                    int.TryParse(CurrentNews.ToString(), out MainNew);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Error Parsing int current news: " + e.Message);
                }
                

                Preferences.Remove("CurrentNews");

                var filter = "estado=1";

                if (isAdmin)
                {
                    string IdCities = App.Current.Properties.ContainsKey("IdCities") ?
                        App.Current.Properties["IdCities"].ToString() : "";

                    filter += $"&id_ciudad=in:{IdCities}";
                }
                else
                {
                    string IdCity = App.Current.Properties.ContainsKey("IdCity") ?
                        App.Current.Properties["IdCity"].ToString() : "0";

                    filter += $"&id_ciudad={IdCity}";
                }

                var News = await NotificationService.All(filter);

                int count = 1;
                
                int _selectedIndex = 0;

                int _selectedIdNotificacion = 0;

                foreach (Notification New in News)
                {

                    _Articles.Add(new Article
                    {
                        ImagePath = New.imagen,
                        Name = New.texto_corto,
                        Description = New.texto_largo,
                        Link = New.url_adjunto
                    });

                    if (New.id_notificacion == MainNew)
                    {
                        _selectedIndex = count;
                        _selectedIdNotificacion = New.id_notificacion;
                        Console.WriteLine($"La noticia {New.texto_corto} se presenta como primera");
                    }

                    count++;
                }

                this.FeaturedStories = _Articles;

                this.SelectedIndex = _selectedIndex;

                if (_selectedIdNotificacion != 0)
                    NotificationService.Recieved( new NotificationRecievedRequest { id_notificacion = _selectedIdNotificacion });
            }
            catch(Exception Ex)
            {
                Console.WriteLine($"Error loading the news: {Ex.Message}");
            }

        }

        #region Public Properties

        public string MainImage 
        {
        
            get { return mainImage; }
            set { SetValue(ref this.mainImage, value); }
        }
        public ObservableCollection<Article> FeaturedStories
        {
            get { return this.featuredStories; }
            set { SetValue(ref this.featuredStories, value); }
        }
        public int SelectedIndex
        {
            get { return this.selectedIndex; }
            set { SetValue(ref this.selectedIndex, value); ChangedIndex(value); }
        }
        public string TitleText
        {
            get { return this.titleText; }
            set { SetValue(ref this.titleText, value); }
        }
        public string MainText
        {
            get { return this.mainText; }
            set { SetValue(ref this.mainText, value); }
        }
        public bool IsFirstArticle
        {
            get { return this.isFirstArticle; }
            set { SetValue(ref this.isFirstArticle, value); }
        }
        public bool IsNotFirstArticle
        {
            get { return this.isNotFirstArticle; }
            set { SetValue(ref this.isNotFirstArticle, value); }
        }
        public bool HasLink
        {
            get { return this.hasLink; }
            set { SetValue(ref this.hasLink, value); }
        }
        #endregion

        #region Command
        public Command LinkCommand { get; set; }

        #endregion

        #region Methods

        private void ChangedIndex(int value)
        {
            try
            {
                IsFirstArticle = (value == 0);
                IsNotFirstArticle = !IsFirstArticle;

                var count = 0;
                foreach (Article article in FeaturedStories)
                {
                    if (count == value)
                    {
                        TitleText = article.Name;
                        MainText = article.Description;
                        link = article.Link?.ToString() ?? "";
                        HasLink = (article.Link != null);
                    }
                    count++;
                }

                Console.WriteLine($"Selected index: {value}");
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Error changing index: {ex.Message}");
            }
            
        }

        private void linkMethod()
        {
            try
            {
                Browser.OpenAsync(link, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ex)
            {
                // An unexpected error occured. No browser may be installed on the device.
            }
        }

        #endregion
    }
}
