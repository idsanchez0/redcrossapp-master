﻿using Acr.UserDialogs;
using RedCrossApp.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class RecoverPasswordViewModel : BaseViewModel
    {
        private string email;
        private INavigation Navigation;

        public string User
        {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }

        public ICommand ResetCommand
        {
            get
            {
                return new Command(async() => await ResetMethod());
            }
        }

        public RecoverPasswordViewModel(INavigation _navigation)
        {
            Navigation = _navigation;
        }

        private async Task ResetMethod()
        {

            if(string.IsNullOrEmpty(User))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar usu usuario", "Ok");
                return;
            }

            UserDialogs.Instance.ShowLoading("Enviando...", MaskType.Black);

            var ResponseResetPassword = await Auth.ResetPassword(User);

            if (!ResponseResetPassword)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Usuario no encontrado", "Ok");

                UserDialogs.Instance.HideLoading();
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Alerta", "Correo enviado con link para resetear la contraseña", "Ok");

                UserDialogs.Instance.HideLoading();

                await Navigation.PopAsync();
            }

        }
    }
}
