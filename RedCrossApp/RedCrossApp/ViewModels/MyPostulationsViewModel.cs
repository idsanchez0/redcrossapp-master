﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class MyPostulationsViewModel : BaseViewModel
    {
        private ObservableCollection<Activity> activities;
        private INavigation Navigation;
        private bool noActivities;

        public ObservableCollection<Activity> Activities
        {
            get { return activities; }
            set { SetValue(ref this.activities, value); }
        }
        public bool NoActivities
        {
            get { return noActivities; }
            set { SetValue(ref this.noActivities, value); }
        }

        public ICommand ActivityCommand { get; private set; }
        public ICommand SearchTextChangedCommand { get; private set; }
        
        public MyPostulationsViewModel(INavigation _navigation)
        {
            Navigation = _navigation;
            
            ActivityCommand = new Command<Activity>(async (ActivityData) => await ActivityMethod(ActivityData));

            SearchTextChangedCommand = new Command<string>((x) => SearchTextChangedMethod(x));
        }

        public async void LoadData(string search = "")
        {
            UserDialogs.Instance.ShowLoading("Cargando...", MaskType.Black);

            try
            {
                string _search = "today=true&postulacion=true&vol_voluntario=true";

                if (!string.IsNullOrEmpty(search))
                    _search = $"&nombre=orlk:{search}&responsable=orlk:{search}";

                Activities = new ObservableCollection<Activity>(await ActivityService.All(_search));

                NoActivities = !Activities.Any();

            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
                await App.Current.MainPage.DisplayAlert("Error", E.Message, "Aceptar");
            }

            UserDialogs.Instance.HideLoading();
        }

        private async Task ActivityMethod(Activity activityData)
        {
            if (activityData != null)
                await Navigation.PushAsync(new PostulationDetailPage(activityData));
        }

        private void SearchTextChangedMethod(object text)
        {
            var textValue = text?.ToString();

            LoadData(textValue);
        }

    }
}
