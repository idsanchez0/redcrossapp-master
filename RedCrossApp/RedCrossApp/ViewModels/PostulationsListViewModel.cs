﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class PostulationsListViewModel : BaseViewModel
    {
        private ObservableCollection<Activity> activities;
        private INavigation Navigation;
        private bool noActivities;
        private string searchFields;

        public ObservableCollection<Activity> Activities
        {
            get { return activities; }
            set { SetValue(ref this.activities, value); }
        }
        public bool NoActivities
        {
            get { return noActivities; }
            set { SetValue(ref this.noActivities, value); }
        }

        public Command ActivityCommand { get; private set; }
        public Command EditCommand { get; private set; }
        public Command SearchTextChangedCommand { get; private set; }
        public Command DownloadFileCommand { get; private set; }

        public PostulationsListViewModel(INavigation _navigation)
        {
            Navigation = _navigation;
            
            ActivityCommand = new Command<Activity>(async (ActivityData) => await ActivityMethod(ActivityData));

            EditCommand = new Command<Activity>(async (ActivityData) => await EditMethod(ActivityData));

            DownloadFileCommand = new Command(async () => await DownloadFileMethod());

            SearchTextChangedCommand = new Command<string>((x) => SearchTextChangedMethod(x));
        }

        public async void LoadData(string search = "")
        {
            UserDialogs.Instance.ShowLoading("Cargando...", MaskType.Black);

            try
            {
                searchFields = "today=true&postulacion=true";

                bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") && (bool)(App.Current.Properties["IsAdmin"]);

                if (!string.IsNullOrEmpty(search))
                    searchFields = $"&nombre=orlk:{search}&responsable=orlk:{search}";

                Activities = new ObservableCollection<Activity>(await ActivityService.All(searchFields));

                NoActivities = !Activities.Any();

            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
                await App.Current.MainPage.DisplayAlert("Error", E.Message, "Aceptar");
            }

            UserDialogs.Instance.HideLoading();
        }

        private async Task ActivityMethod(Activity activityData)
        {
            if (activityData != null)
                await Navigation.PushAsync(new PostulationsDetailPage(activityData));
        }

        private async Task EditMethod(Activity activityData)
        {
            if (activityData != null)
                await Navigation.PushAsync(new ActivityEditPage(activityData));
        }

        private void SearchTextChangedMethod(object text)
        {
            var textValue = text?.ToString();

            LoadData(textValue);
        }

        private async Task DownloadFileMethod()
        {
            try
            {
                var file = new Uri(ApiServices.Url + $"api/postulation/pdf?{searchFields}");

                // Open PDF URL with device browser to download  
                await Launcher.OpenAsync(file);
            }
            catch (Exception)
            {

            }
        }
    }
}
