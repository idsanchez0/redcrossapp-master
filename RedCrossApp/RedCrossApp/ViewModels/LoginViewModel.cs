﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class LoginViewModel : BaseViewModel
    {
        private string _password;
        private string username;
        private Auth AuthService;

        public string Username
        {
            get { return this.username; }
            set { SetValue(ref this.username, value); }
        }

        public string Password
        {
            get { return this._password; }
            set { SetValue(ref this._password, value); }
        }

        public ICommand LoginCommand { get; private set; }
        //modificacion de codigo
        public ICommand QuieroSerVoluntarioCommand { get; private set; }
        public LoginViewModel()
        {
            AuthService = new Auth();

            LoginCommand = new Command(async () => await LoginMethod());
            //QuieroSerVoluntarioCommand = new Command(async () => await QuieroSerVoluntarioMethod());
        }

        private async Task LoginMethod()
        {
            if (string.IsNullOrEmpty(Username))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar su usuario", "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una clave", "Aceptar");
                return;
            }

            UserDialogs.Instance.ShowLoading("Ingresando...", MaskType.Black);

            var request = new AdminLoginRequest
            {
                username = Username,
                password = Password,
                device_name = DeviceInfo.Name,
                firebase_token = Preferences.Get("TokenFirebase",""),
                platform = Utils.Platform()
            };

            var LoginResponse = await AuthService.AdminLogin(request);

            if (!LoginResponse.IsSuccess)
            {
                UserDialogs.Instance.HideLoading();

                await App.Current.MainPage.DisplayAlert("Error", "Usuario o Contraseña incorrectos", "Aceptar");
                Password = string.Empty;
                return;
            }

            var Token = (Token)LoginResponse.Result;

            await Utils.AdminLoginCompleted(Token.token);

            UserDialogs.Instance.HideLoading();
        }
        

    }

}
