﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Essentials;

namespace RedCrossApp.ViewModels
{
    class NotificationsViewModel : BaseViewModel
    {
        private ObservableCollection<UserNotifications> myNotifications;
        private bool noNotifications;

        public ObservableCollection<UserNotifications> MyNotifications
        {
            get { return myNotifications; }
            set { SetValue(ref this.myNotifications, value); }
        }
        public bool NoNotifications
        {
            get { return noNotifications; }
            set { SetValue(ref this.noNotifications, value); }
        }

        //public ICommand NotificationCommand { get; set; }

        public string StatusFilter { get; set; }

        public NotificationsViewModel()
        {
            //NotificationCommand = new Command<MyNotification>((Notification) => NotificationMethod(Notification));
        }

        private void NotificationMethod(MyNotification notification)
        {
            Preferences.Set("CurrentNews", notification.id_notificacion.ToString());
            App.Current.MainPage = new MenuPage();
        }

        public async void LoadData()
        {
            UserDialogs.Instance.ShowLoading("Cargando...", MaskType.Black);

            try
            {
                string filter = "";

                if (!string.IsNullOrEmpty(StatusFilter))
                    filter = "estado=" + StatusFilter;

                var _myNotifications = await NotificationService.MyNotifications(filter);

                NoNotifications = !_myNotifications.Any();

                if(!NoNotifications)
                {

                    var Notifications = new ObservableCollection<UserNotifications>();

                    foreach(MyNotification myNotification in _myNotifications)
                    {
                        Notifications.Add(new UserNotifications() {
                            Id = myNotification.notification.id_notificacion,
                            Title = myNotification.notification.texto_corto,
                            Body = myNotification.notification.texto_largo,
                            Author = myNotification.notification?.login_responsable?.ToString() ?? "",
                            HasLink = !string.IsNullOrEmpty(myNotification.notification?.url_adjunto?.ToString()),
                            DateTime = myNotification.notification?.fecha_registro ?? DateTime.Now
                        });
                    }

                    MyNotifications = Notifications;
                }

            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
                await App.Current.MainPage.DisplayAlert("Error", E.Message, "Aceptar");
            }

            UserDialogs.Instance.HideLoading();
        }

    }
}
