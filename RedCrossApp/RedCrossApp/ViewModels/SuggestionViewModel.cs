﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class SuggestionViewModel :BaseViewModel
    {
        private string message;

        public string Message
        {
            get { return this.message; }
            set { SetValue(ref this.message, value); }
        }

        public ICommand MessageCommand { get; private set; }
        public SuggestionViewModel()
        {
            try
            {
                MessageCommand = new Command(MessageMethod);
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine("Error in SuggestionViewModel: " + ex.Message);
            }
        }

        private async void MessageMethod()
        {
            if (string.IsNullOrEmpty(Message))
                await App.Current.MainPage.DisplayAlert("Error", "El Mensaje no puede ser vacio", "OK");
            else
            {
                await SuggestionService.SendSuggestion(new SuggestionRequest { message = Message });
                await App.Current.MainPage.DisplayAlert("Alerta", "Mensaje Enviado", "OK");
                App.Current.MainPage = new MenuPage();
            }
        }
    }
}
