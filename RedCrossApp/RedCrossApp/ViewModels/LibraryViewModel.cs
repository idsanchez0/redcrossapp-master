using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.Library
{

    [Preserve(AllMembers = true)]
    public class LibraryViewModel : BaseViewModel
    {
        #region Fields

        private ObservableCollection<LibraryItemsModel> libraryItems;

        private Command categorySelectedCommand;

        #endregion

        #region Public properties

        public ObservableCollection<LibraryItemsModel> LibraryItems
        {
            get { return this.libraryItems; }
            set
            {
                if (this.libraryItems == value)
                {
                    return;
                }

                this.libraryItems = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

        #region Command

        public Command ItemSelectedCommand
        {
            get { return categorySelectedCommand ?? (categorySelectedCommand = new Command(ItemSelected)); }
        }

        #endregion

        #region Methods

        public void LoadData()
        {
            LibraryItems = new ObservableCollection<LibraryItemsModel>
            {
                new LibraryItemsModel { Name = "Archivo.pdf", Icon = ApiServices.Url + "storage/img/cloudFile.png", Url = "cloudFile.png"},
                new LibraryItemsModel { Name = "Archivo 1.pdf", Icon = ApiServices.Url + "storage/img/cloudFile.png", Url = "cloudFile.png"},
                new LibraryItemsModel { Name = "Archivo 2.pdf", Icon = ApiServices.Url + "storage/img/cloudFile.png", Url = "cloudFile.png"},
                new LibraryItemsModel { Name = "Archivo 3.pdf", Icon = ApiServices.Url + "storage/img/cloudFile.png", Url = "cloudFile.png"},
                new LibraryItemsModel { Name = "Archivo 4.pdf", Icon = ApiServices.Url + "storage/img/cloudFile.png", Url = "cloudFile.png"},
            };
        }

        /// <summary>
        /// Invoked when the Category is selected.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void ItemSelected(object obj)
        {
            if (obj != null)
            {
                var Item = (LibraryItemsModel)obj;

                var uri = new Uri(ApiServices.Url + "storage/img/" + Item.Url);
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
            }
        }

        #endregion
    }
}