﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Plugin.Calendar.Models;

namespace RedCrossApp.ViewModels
{
    class CalendarViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private INavigation Navigation;
        public ICommand TodayCommand => new Command(() => { Month = DateTime.Today.Month; SelectedDate = DateTime.Today; });
        public ICommand EventSelectedCommand => new Command(async (item) => await ExecuteEventSelectedCommand(item));

        public CalendarViewModel(INavigation navigation) : base()
        {
            Navigation = navigation;
            // testing all kinds of adding events
            // when initializing collection

            Events = new EventCollection ();

            //LoadData();
        }

        public async void LoadData()
        {
            try
            {
                string _search = "limit=5000&today=true&vol_voluntario=true";

                var Activities = await ActivityService.All(_search);

                var _ActivitiesEvents = new List<EventModel>();

                int cont = 0;

                string Postulation = "";

                DateTime TmpDate = new DateTime();

                foreach (Activity _activity in Activities)
                {
                    switch (_activity?.estado_postulacion?.ToString() ?? "")
                    {
                        case "1":
                            Postulation = "Postulado";
                            break;
                        case "2":
                            Postulation = "Aceptado";
                            break;
                        case "3":
                            Postulation = "Rechazado";
                            break;
                        default:
                            Postulation = "";
                            break;
                    }

                    if (cont == 0)
                        TmpDate = _activity.fecha_inicio;

                    Console.WriteLine("TmpDate: " + TmpDate.ToString("yyyy-MM-dd"));

                    if (TmpDate.ToString("yyyy-MM-dd") == _activity.fecha_inicio.ToString("yyyy-MM-dd"))
                    {
                        Console.WriteLine("Add to current EventModel: " + TmpDate.ToString("yyyy-MM-dd"));
                        _ActivitiesEvents.Add(new EventModel
                        {
                            Activity = _activity,
                            Name = _activity.nombre,
                            Description = _activity.descripcion,
                            Postulation = Postulation
                        });
                    }
                    else
                    {
                        Console.WriteLine("Add to current Events, " + _ActivitiesEvents.Count().ToString() + " EventModel, Create new TmpDate: " + _activity.fecha_inicio.ToString("yyyy-MM-dd"));

                        Events.Add(TmpDate, _ActivitiesEvents); 
                        
                        TmpDate = _activity.fecha_inicio;

                        _ActivitiesEvents = new List<EventModel>() {
                            new EventModel
                            {
                                Activity = _activity, 
                                Name = _activity.nombre, 
                                Description = _activity.descripcion,
                                Postulation = Postulation
                            }
                        };
                    }

                    cont++;

                    Console.WriteLine("cont: " + cont.ToString() + "  Total: " + Activities.Count());

                    if (cont == Activities.Count())
                    {
                        Console.WriteLine("Final Event added, " + _ActivitiesEvents.Count().ToString() + " EventModel: " + _activity.fecha_inicio.ToString("yyyy-MM-dd"));
                        
                        Events.Add(_activity.fecha_inicio, _ActivitiesEvents);
                    }
                }
            } 
            catch(Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
            
        }

        private IEnumerable<EventModel> GenerateEvents(int count, string name)
        {
            return Enumerable.Range(1, count).Select(x => new EventModel
            {
                Name = $"{name} event{x}",
                Description = $"This is {name} event{x}'s description!"
            });
        }

        public EventCollection Events { get; set; }

        private int _month = DateTime.Today.Month;
        public int Month
        {
            get => _month;
            set => SetValue(ref _month, value);
        }

        public int _year = DateTime.Today.Year;
        public int Year
        {
            get => _year;
            set => SetValue(ref _year, value);
        }

        private DateTime _selectedDate = DateTime.Today;
        public DateTime SelectedDate
        {
            get => _selectedDate;
            set => SetValue(ref _selectedDate, value);
        }

        private DateTime _minimumDate = DateTime.Today.AddMonths(-2);
        public DateTime MinimumDate
        {
            get => _minimumDate;
            set => SetValue(ref _minimumDate, value);
        }

        private DateTime _maximumDate = DateTime.Today.AddMonths(5);
        public DateTime MaximumDate
        {
            get => _maximumDate;
            set => SetValue(ref _maximumDate, value);
        }

        private async Task ExecuteEventSelectedCommand(object item)
        {
            if (item is EventModel eventModel)
            {
                //await App.Current.MainPage.DisplayAlert(eventModel.Name, eventModel.Description, "Ok");
                await Navigation.PushAsync(new ActivityDetailPage(eventModel.Activity));
            }
        }
    }
}
