﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditVolunteerLanguageViewModel : BaseViewModel
    {

        #region private properties
        private ObservableCollection<AcademicTitleType> academicTitleTypes;
        private AcademicTitleType academicTitleType;
        private int academicTitleTypeIndex = -1;
        private int oralLevelIndex = -1;
        private int writtenLevelIndex = -1;

        #endregion

        public INavigation Navigation;
        public VolunteerFormation VolunteerFormation;

        public ObservableCollection<AcademicTitleType> AcademicTitleTypes
        {
            get { return academicTitleTypes; }
            set { SetValue(ref this.academicTitleTypes, value); }
        }
        public AcademicTitleType AcademicTitleType
        {
            get { return academicTitleType; }
            set { SetValue(ref this.academicTitleType, value); }
        }
        public int AcademicTitleTypeIndex
        {
            get { return academicTitleTypeIndex; }
            set { SetValue(ref this.academicTitleTypeIndex, value); }
        }
        public int OralLevelIndex
        {
            get { return oralLevelIndex; }
            set { SetValue(ref this.oralLevelIndex, value); }
        }
        public int WrittenLevelIndex
        {
            get { return writtenLevelIndex; }
            set { SetValue(ref this.writtenLevelIndex, value); }
        }
        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }

        public AddEditVolunteerLanguageViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            if(Navigation != null)
                Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                var Response = await VolunteerFormationService.Store(new VolunteerFormationRequest {
                    id_tipo_titulo_academico = AcademicTitleType.id_tipo_titulo_academico,
                    nivel_oral = GetLevel(OralLevelIndex),
                    nivel_escrito = GetLevel(WrittenLevelIndex),
                    id_voluntario_formacion = VolunteerFormation?.id_voluntario_formacion ?? null
                });

                if(Response)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Idioma registrado Correctamente", "ok");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el Idioma", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el Idioma", "ok");
            }

        }

        public async void LoadData()
        {
            try
            {

                var formationLevelTypes = await FormationLevelTypeService.All("id_tipo_nivel_formacion=in:7");

                if (formationLevelTypes != null && formationLevelTypes.Any())
                    AcademicTitleTypes = new ObservableCollection<AcademicTitleType>(formationLevelTypes[0].academic_title_types);

                if(VolunteerFormation != null)
                {
                    OralLevelIndex = SetLevel(VolunteerFormation?.nivel_oral?.ToString() ?? "");
                    WrittenLevelIndex = SetLevel(VolunteerFormation?.nivel_escrito?.ToString() ?? "");

                    int index = 0;

                    foreach (AcademicTitleType _title in AcademicTitleTypes)
                    {
                        if (_title.id_tipo_titulo_academico == (VolunteerFormation?.id_tipo_titulo_academico ?? -1))
                        {
                            AcademicTitleTypeIndex = index;
                            break;
                        }

                        index++;
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {

            if (AcademicTitleType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de idioma", "ok");
                return false;
            }

            if (OralLevelIndex == -1)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de nivel oral", "ok");
                return false;
            }

            if (WrittenLevelIndex == -1)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de nivel escrito", "ok");
                return false;
            }

            return true;
        }

        string GetLevel(int level)
        {
            switch(level)
            {
                case 0:
                    return "BASICO";
                case 1:
                    return "INTERMEDIO";
                case 2:
                    return "AVANZADO";
                case 3:
                    return "NATIVO";
                default:
                    return "";
            }
        }

        int SetLevel(string level)
        {
            switch (level)
            {
                case "BASICO":
                    return 0;
                case "INTERMEDIO":
                    return 1;
                case "AVANZADO":
                    return 2;
                case "NATIVO":
                    return 3;
                default:
                    return -1;
            }
        }
    }
}
