﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditVolunteerVaccineViewModel : BaseViewModel
    {

        #region private properties
        private ObservableCollection<Vaccine> vaccines;
        private Vaccine vaccine;
        private int vaccineIndex = -1;
        private string description;

        #endregion

        public INavigation Navigation;
        public int? IdVaccine;

        public ObservableCollection<Vaccine> Vaccines
        {
            get { return vaccines; }
            set { SetValue(ref this.vaccines, value); }
        }
        public Vaccine Vaccine
        {
            get { return vaccine; }
            set { SetValue(ref this.vaccine, value); }
        }
        public int VaccineIndex
        {
            get { return vaccineIndex; }
            set { SetValue(ref this.vaccineIndex, value); }
        }
        public string Description
        {
            get { return description; }
            set { SetValue(ref this.description, value); }
        }

        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }

        public AddEditVolunteerVaccineViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            if(Navigation != null)
                Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                var Response = await VolunteerVaccineService.Store(new VolunteerVaccineRequest {
                    id_tipo_vacuna = Vaccine.id_tipo_vacuna,
                    descripcion = Description ?? ""
                });

                if(Response)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Vacuna registrada Correctamente", "ok");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar la Vacuna", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar la Vacuna", "ok");
            }

        }

        public async void LoadData()
        {
            try
            {

                Vaccines = new ObservableCollection<Vaccine>(await VaccineService.All());

                if(IdVaccine != null)
                {

                    int index = 0;

                    foreach (Vaccine _vaccine in Vaccines)
                    {
                        if (_vaccine.id_tipo_vacuna == IdVaccine)
                        {
                            VaccineIndex = index;
                            break;
                        }

                        index++;
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {

            if (Vaccine == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una vacuna", "ok");
                return false;
            }

            return true;
        }

    }
}
