﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    class FileExpirienceViewModel : BaseViewModel
    {

        #region private properties
        private List<int> selectedLaboralRelationsIndices = new List<int>() { };
        private ObservableCollection<LaboralSituation> laboralRelations;

        private bool hasOtherRelation = false;
        private string otherRelation;
        private string occupation;
        private string occupationLocation;
        private string occupationPhone;
        #endregion

        #region public properties
        public List<int> SelectedLaboralRelationsIndices
        {
            get { return selectedLaboralRelationsIndices; }
            set { SetValue(ref this.selectedLaboralRelationsIndices, value); CheckRelations(); }
        }
        public ObservableCollection<LaboralSituation> LaboralRelations
        {
            get { return laboralRelations; }
            set { SetValue(ref this.laboralRelations, value); }
        }
        public bool HasOtherRelation
        {
            get { return hasOtherRelation; }
            set { SetValue(ref this.hasOtherRelation, value); }
        }
        public string OtherRelation
        {
            get { return otherRelation; }
            set { SetValue(ref this.otherRelation, value); }
        }
        public string Occupation
        {
            get { return occupation; }
            set { SetValue(ref this.occupation, value); }
        }
        public string OccupationLocation
        {
            get { return occupationLocation; }
            set { SetValue(ref this.occupationLocation, value); }
        }
        public string OccupationPhone
        {
            get { return occupationPhone; }
            set { SetValue(ref this.occupationPhone, value); }
        }
        public Command SubmitCommand { get; set; }
        #endregion

        public FileExpirienceViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }

        public async void LoadData()
        {
            try
            {
                var laboralSituations = await LaboralSituationService.All();

                if (laboralSituations != null)
                    LaboralRelations = new ObservableCollection<LaboralSituation>(laboralSituations);
                else
                    LaboralRelations = new ObservableCollection<LaboralSituation>();

                var Volunteer = await Auth.VolunteerData();

                Console.WriteLine("Volunteer loaded, checking laboral:" + JsonConvert.SerializeObject(Volunteer.situaciones_laborales));

                Occupation = Volunteer?.trabajo_dedicacion?.ToString() ?? "";
                OccupationLocation = Volunteer?.trabajo_lugar?.ToString() ?? "";
                OccupationPhone = Volunteer?.trabajo_telef_conv?.ToString() ?? "";

                Console.WriteLine("Loaded the plain entrys");

                var _selectedOccupationIndices = new List<int>();

                int count = 0;

                if(Volunteer?.situaciones_laborales != null && Volunteer.situaciones_laborales.Any() && LaboralRelations.Any())
                {
                    foreach (LaboralSituation laboralSituation in LaboralRelations)
                    {
                        foreach (SituacionLaboral situacionLaboral in Volunteer.situaciones_laborales)
                        {
                            if (situacionLaboral.id_tipo_situacion_laboral == laboralSituation.id_tipo_situacion_laboral)
                            {
                                _selectedOccupationIndices.Add(count);
                                if(situacionLaboral.id_tipo_situacion_laboral == 1)
                                {
                                    HasOtherRelation = true;
                                    OtherRelation = situacionLaboral.pivot.descripcion?.ToString() ?? "";
                                }
                            }
                        }

                        count++;
                    }

                }

                SelectedLaboralRelationsIndices = _selectedOccupationIndices;

                Console.WriteLine(JsonConvert.SerializeObject(SelectedLaboralRelationsIndices));

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        private async void SubmitClicked(object obj)
        {
            try
            {
                var ValidInput = await InputValidate();

                if (!ValidInput)
                    return;

                var _selectedOccupationIndices = new List<int>();

                int count = 0;

                if (SelectedLaboralRelationsIndices != null && SelectedLaboralRelationsIndices.Any() && LaboralRelations != null)
                {
                    foreach (LaboralSituation _laboralSituation in LaboralRelations)
                    {
                        if (SelectedLaboralRelationsIndices.Contains(count))
                            _selectedOccupationIndices.Add(_laboralSituation.id_tipo_situacion_laboral);
                        
                        count++;
                    }

                }

                var Volunteer = await Auth.UpdateVolunteerData(new VolunteerExpirienceRequest() {
                    trabajo_dedicacion = Occupation,
                    trabajo_lugar = OccupationLocation,
                    trabajo_telef_conv = OccupationPhone,
                    situaciones_laborales = _selectedOccupationIndices,
                    otra_situacion_laboral = HasOtherRelation ? OtherRelation : null

                });

                if (Volunteer != null)
                    await App.Current.MainPage.DisplayAlert("Alerta", "Datos Actualizados","ok");
                else
                    await App.Current.MainPage.DisplayAlert("Error", "Error actualizando su ficha", "ok");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task<bool> InputValidate()
        {

            if (SelectedLaboralRelationsIndices == null || !SelectedLaboralRelationsIndices.Any())
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe registrar al menos una situación laboral actual", "Ok");
                return false;
            }

            if (string.IsNullOrEmpty(Occupation))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe registrar a que se dedica laboralmente", "Ok");
                return false;
            }
            return true;
        }

        void CheckRelations()
        {

            int count = 0;

            if (SelectedLaboralRelationsIndices != null && SelectedLaboralRelationsIndices.Any() && LaboralRelations != null)
            {
                foreach (LaboralSituation _laboralSituation in LaboralRelations)
                {
                    if (SelectedLaboralRelationsIndices.Contains(count) && _laboralSituation.id_tipo_situacion_laboral == 1)
                    {
                        HasOtherRelation = true;
                        return;
                    }

                    count++;
                }

            }

            HasOtherRelation = false;
        }
    }
}
