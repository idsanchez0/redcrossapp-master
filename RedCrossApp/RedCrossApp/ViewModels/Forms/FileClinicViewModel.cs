﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    class FileClinicViewModel : BaseViewModel
    {

        #region Private properties
        private List<int> selectedDiseasesIndices = new List<int>() { };
        private ObservableCollection<Disease> diseases;

        private bool hasOtherDisease = false;
        private string otherDisease;
        private string height;
        private string weight;
        private string allergic;

        private bool hasTreatment;
        private string treatment;
        #endregion

        #region public properties
        public List<int> SelectedDiseasesIndices
        {
            get { return selectedDiseasesIndices; }
            set { SetValue(ref this.selectedDiseasesIndices, value); CheckDiseases(); }
        }
        public ObservableCollection<Disease> Diseases
        {
            get { return diseases; }
            set { SetValue(ref this.diseases, value); }
        }
        public bool HasOtherDisease
        {
            get { return hasOtherDisease; }
            set { SetValue(ref this.hasOtherDisease, value); }
        }
        public string OtherDisease
        {
            get { return otherDisease; }
            set { SetValue(ref this.otherDisease, value); }
        }
        public string Height
        {
            get { return height; }
            set { SetValue(ref this.height, value); }
        }
        public string Weight
        {
            get { return weight; }
            set { SetValue(ref this.weight, value); }
        }
        public string Allergic
        {
            get { return allergic; }
            set { SetValue(ref this.allergic, value); }
        }
        public bool HasTreatment
        {
            get { return hasTreatment; }
            set { SetValue(ref this.hasTreatment, value); }
        }
        public string Treatment
        {
            get { return treatment; }
            set { SetValue(ref this.treatment, value); }
        }

        public Command SubmitCommand { get; set; }
        #endregion
        public FileClinicViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }

        public async void LoadData()
        {
            try
            {
                var _diseases = await DiseasesService.All();

                if (_diseases != null)
                    Diseases = new ObservableCollection<Disease>(_diseases);
                else
                    Diseases = new ObservableCollection<Disease>();

                var Volunteer = await Auth.VolunteerData();

                Console.WriteLine("Volunteer loaded, checking disease:" + JsonConvert.SerializeObject(Volunteer.enfermedades));

                Height = Volunteer?.talla_m?.ToString() ?? "0";
                Weight = Volunteer?.peso_kg?.ToString() ?? "0";
                Allergic = Volunteer?.hist_clin_alergias?.ToString() ?? "";
                HasTreatment = Volunteer?.hist_clin_tratamiento ?? false;
                Treatment = Volunteer?.hist_clin_medicacion?.ToString() ?? "";

                Console.WriteLine("Loaded the plain entrys");

                var _selectedDiseasesIndices = new List<int>();

                int count = 0;

                if(Volunteer?.enfermedades != null && Volunteer.enfermedades.Any() && Diseases != null)
                {
                    foreach (Disease _disease in _diseases)
                    {
                        foreach (Enfermedad enfermedad in Volunteer.enfermedades)
                        {
                            if (enfermedad.id_tipo_enfermedad == _disease.id_tipo_enfermedad)
                            {
                                _selectedDiseasesIndices.Add(count);
                                if (enfermedad.id_tipo_enfermedad == 1)
                                {
                                    HasOtherDisease = true;
                                    OtherDisease = enfermedad.pivot.descripcion?.ToString() ?? "";
                                }
                            }
                        }

                        count++;
                    }

                }

                SelectedDiseasesIndices = _selectedDiseasesIndices;

                Console.WriteLine(JsonConvert.SerializeObject(SelectedDiseasesIndices));

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        private async void SubmitClicked(object obj)
        {
            try
            {
                if (!await InputValidate())
                    return;

                var _selectedDiseasesIndices = new List<int>();

                int count = 0;

                if (SelectedDiseasesIndices != null && SelectedDiseasesIndices.Any() && Diseases != null)
                {
                    foreach (Disease _disease in Diseases)
                    {
                        if (SelectedDiseasesIndices.Contains(count))
                            _selectedDiseasesIndices.Add(_disease.id_tipo_enfermedad);
                        
                        count++;
                    }

                }

                float.TryParse(Height, out float HeightFloat);
                float.TryParse(Weight, out float WeightFloat);

                var Volunteer = await Auth.UpdateVolunteerData(new VolunteerClinicRequest() {
                    talla_m = HeightFloat,
                    peso_kg = WeightFloat,
                    hist_clin_alergias = Allergic,
                    hist_clin_tratamiento = HasTreatment,
                    hist_clin_medicacion = Treatment,
                    enfermedades = _selectedDiseasesIndices,
                    otra_enfermedad = HasOtherDisease ? OtherDisease : null
                });

                if (Volunteer != null)
                    await App.Current.MainPage.DisplayAlert("Alerta", "Datos Actualizados","ok");
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar sus datos", "ok");
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        private async Task<bool> InputValidate()
        {
            if (HasTreatment && string.IsNullOrEmpty(Treatment))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Si se encuentra bajo tratamiento médico, por favor ingrese el mismo", "ok");
                return false;
            }

            if(HasOtherDisease && string.IsNullOrEmpty(OtherDisease))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Por favor, detalle que otra enfermedad tiene usted", "ok");
                return false;
            }
            return true;
        }

        private void CheckDiseases()
        {
            int count = 0;

            if (SelectedDiseasesIndices != null && SelectedDiseasesIndices.Any() && Diseases != null)
            {
                foreach (Disease _laboralSituation in Diseases)
                {
                    if (SelectedDiseasesIndices.Contains(count) && _laboralSituation.id_tipo_enfermedad == 1)
                    {
                        HasOtherDisease = true;
                        return;
                    }

                    count++;
                }

            }

            HasOtherDisease = false;
        }
    }
}
