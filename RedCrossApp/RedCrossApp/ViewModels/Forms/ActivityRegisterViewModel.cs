﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views;
using RedCrossApp.Views.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.Forms
{
    /// <summary>
    /// ViewModel for Business Registration Form page 
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class ActivityRegisterViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityRegisterViewModel" /> class
        /// </summary>
        public ActivityRegisterViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
            //LoadData();
        }
        #endregion

        #region Local Variables
        private string fullName;
        private string activityDescription;
        private ObservableCollection<Ambito> scopes;
        private Ambito scope;
        private ObservableCollection<TipoActividad> activityTypes;
        private TipoActividad activityType;
        private ObservableCollection<TipoProyecto> projectTypes;
        private TipoProyecto projectType;
        private ObservableCollection<Proyecto> projects;
        private Proyecto project;
        private bool dateTimeValidation = true;
        private string startDate;
        private string startTime;
        private string endDate;
        private string endTime;
        private ObservableCollection<TipoDepartamento> departments;
        private TipoDepartamento department;
        private ObservableCollection<State> states;
        private List<int> selectedStates = new List<int>() { };
        private ObservableCollection<City> cities;
        private List<int> selectedCities = new List<int>() { };
        private string responsableEmail;
        
        private int Duration = 0;

        private float sum;
        private List<Requirement> Requirements;

        #region requirements
        private bool hasCourses = false;
        private string coursesPercentage = "0";
        private bool hasAcademicFormation = false;
        private string academicPercentage = "0";
        private bool hasEni = false;
        private string eniPercentage = "0";
        private bool hasLanguages = false;
        private string languagesPercentage = "0";
        private bool hasAges = false;
        private string agesPercentage = "0";
        private bool hasActivityYears = false;
        private string activityYearsPercentage = "0";
        private bool hasDateLastActivity = false;
        private string dateLastActivityPercentage = "0";
        #endregion
        #endregion

        #region Properties

        public string FullName
        {
            get { return fullName; }
            set { SetValue(ref this.fullName, value); }
        }
        public string ActivityDescription
        {
            get { return activityDescription; }
            set { SetValue(ref this.activityDescription, value); }
        }
        public ObservableCollection<Ambito> Scopes
        {
            get { return scopes; }
            set { SetValue(ref this.scopes, value); }
        }
        public Ambito Scope
        {
            get { return scope; }
            set
            {
                SetValue(ref this.scope, value);
                ChangeScope(value);
            }
        }
        public ObservableCollection<TipoActividad> ActivityTypes
        {
            get { return activityTypes; }
            set { SetValue(ref this.activityTypes, value); }
        }
        public TipoActividad ActivityType
        {
            get { return activityType; }
            set { SetValue(ref this.activityType, value); }
        }
        public ObservableCollection<TipoProyecto> ProjectTypes
        {
            get { return projectTypes; }
            set { SetValue(ref this.projectTypes, value); }
        }
        public TipoProyecto ProjectType
        {
            get { return projectType; }
            set
            {
                SetValue(ref this.projectType, value);
                ChangeProject(value);
            }
        }
        public ObservableCollection<Proyecto> Projects
        {
            get { return projects; }
            set { SetValue(ref this.projects, value); }
        }
        public Proyecto Project
        {
            get { return project; }
            set { SetValue(ref this.project, value); }
        }
        public string StartDate
        {
            get { return startDate; }
            set { SetValue(ref this.startDate, value); }
        }
        public string StartTime
        {
            get { return startTime; }
            set { SetValue(ref this.startTime, value); }
        }
        public string EndDate
        {
            get { return endDate; }
            set { SetValue(ref this.endDate, value); }
        }
        public string EndTime
        {
            get { return endTime; }
            set { SetValue(ref this.endTime, value); }
        }
        public string DurationString { get; set; }
        public ObservableCollection<TipoDepartamento> Departments
        {
            get { return departments; }
            set { SetValue(ref this.departments, value); }
        }
        public TipoDepartamento Department
        {
            get { return department; }
            set { SetValue(ref this.department, value); }
        }
        public ObservableCollection<State> States
        {
            get { return states; }
            set { SetValue(ref this.states, value); }
        }
        public List<int> SelectedStates
        {
            get { return selectedStates; }
            set 
            { 
                SetValue(ref this.selectedStates, value);
                ChangeState(value);
            }
        }
        public ObservableCollection<City> Cities
        {
            get { return cities; }
            set { SetValue(ref this.cities, value); }
        }
        public List<int> SelectedCities
        {
            get { return selectedCities; }
            set { SetValue(ref this.selectedCities, value); }
        }
        public string Responsable { get; set; }
        public string ResponsableEmail
        {
            get { return responsableEmail; }
            set { SetValue(ref this.responsableEmail, value); }
        }

        #region requirements
        public bool HasCourses
        {
            get { return hasCourses; }
            set 
            { 
                SetValue(ref this.hasCourses, value);
                CalculateWeightDistribution();
            }
        }
        public string CoursesPercentage
        {
            get { return coursesPercentage; }
            set { SetValue(ref this.coursesPercentage, value); }
        }
        public bool HasAcademicFormation
        {
            get { return hasAcademicFormation; }
            set 
            { 
                SetValue(ref this.hasAcademicFormation, value);
                CalculateWeightDistribution();
            }
        }
        public string AcademicPercentage
        {
            get { return academicPercentage; }
            set { SetValue(ref this.academicPercentage, value); }
        }
        public bool HasEni
        {
            get { return hasEni; }
            set 
            { 
                SetValue(ref this.hasEni, value);
                CalculateWeightDistribution();
            }
        }
        public string EniPercentage
        {
            get { return eniPercentage; }
            set { SetValue(ref this.eniPercentage, value); }
        }
        public bool HasLanguages
        {
            get { return hasLanguages; }
            set 
            { 
                SetValue(ref this.hasLanguages, value);
                CalculateWeightDistribution();
            }
        }
        public string LanguagesPercentage
        {
            get { return languagesPercentage; }
            set { SetValue(ref this.languagesPercentage, value); }
        }
        public bool HasAges
        {
            get { return hasAges; }
            set 
            { 
                SetValue(ref this.hasAges, value);
                CalculateWeightDistribution();
            }
        }
        public string AgesPercentage
        {
            get { return agesPercentage; }
            set { SetValue(ref this.agesPercentage, value); }
        }
        public bool HasActivityYears
        {
            get { return hasActivityYears; }
            set 
            { 
                SetValue(ref this.hasActivityYears, value);
                CalculateWeightDistribution();
            }
        }
        public string ActivityYearsPercentage
        {
            get { return activityYearsPercentage; }
            set { SetValue(ref this.activityYearsPercentage, value); }
        }
        public bool HasDateLastActivity
        {
            get { return hasDateLastActivity; }
            set 
            { 
                SetValue(ref this.hasDateLastActivity, value);
                CalculateWeightDistribution();
            }
        }
        public string DateLastActivityPercentage
        {
            get { return dateLastActivityPercentage; }
            set { SetValue(ref this.dateLastActivityPercentage, value); }
        }
        #endregion 
        #endregion 

        #region Commands

        /// <summary>
        /// Gets or sets the command is executed when the Submit button is clicked.
        /// </summary>
        public Command SubmitCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Submit button clicked
        /// </summary>
        /// <param name="obj">The object</param>
        private async void SubmitClicked(Object obj)
        {
            try
            {

                var isValid = await validateInput();

                if (!isValid)
                    return;

                UserDialogs.Instance.ShowLoading("Insertando...", MaskType.Black);

                var _SelectedStates = new List<int>() { };
                var _SelectedCities = new List<int>() { };

                if (SelectedStates.Any())
                {
                    int Cont = 0;
                    foreach (State _state in States)
                    {
                        if (SelectedStates.Contains(Cont))
                            _SelectedStates.Add(_state.id_estado);

                        Cont++;
                    }
                }

                if (SelectedCities.Any())
                {
                    int Cont = 0;
                    foreach (City _city in Cities)
                    {
                        if (SelectedCities.Contains(Cont))
                            _SelectedCities.Add(_city.id_ciudad);

                        Cont++;
                    }
                }

                var Activity = await ActivityService.Store(new ActivityRequest
                {
                    id_tipo_actividad = ActivityType.id_tipo_actividad,
                    id_proyecto = Project.id_proyecto,
                    fecha_inicio = DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00"),
                    fecha_fin = DateTime.Parse(EndDate + "T" + EndTime + ":00+0:00"),
                    duracion = Duration,
                    nombre = FullName,
                    descripcion = ActivityDescription,
                    responsable = Responsable,
                    responsable_email = ResponsableEmail,
                    id_tipo_departamento = Department.id_tipo_departamento,
                    multi_provincia = _SelectedStates.Count() > 1,
                    multi_canton = _SelectedCities.Count() > 1,
                    estados = _SelectedStates,
                    ciudades = _SelectedCities,
                    postulacion = !Requirements.Any()
                });

                UserDialogs.Instance.HideLoading();

                if (Activity == null)
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar la actividad", "ok");
                else
                {
                    if (Requirements.Any())
                    {
                        await App.Current.MainPage.DisplayAlert("Alerta", "Se registro la actividad, debe registrar los requisitos", "ok");

                        App.Current.Properties["Requirements"] = JsonConvert.SerializeObject(Requirements);

                        App.Current.Properties["IdActivity"] = Activity.id_actividad.ToString();

                        await App.Current.SavePropertiesAsync();

                        await Task.Delay(100);

                        App.Current.MainPage = new NavigationPage(new RequirementsButtomNavigationPage(Activity));
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Alerta", "Se registro la actividad", "ok");

                        App.Current.MainPage = new MenuPage();
                    }

                }
            }
            catch(Exception e)
            {
                UserDialogs.Instance.HideLoading();
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "ok");
            }
        }

        public async void LoadData()
        {
            try
            {
                Scopes = new ObservableCollection<Ambito>(await ActivityTypeService.All());
                await Task.Delay(100);
                ProjectTypes = new ObservableCollection<TipoProyecto>(await ProjectTypeService.All());
                await Task.Delay(100);
                Departments = new ObservableCollection<TipoDepartamento>(await DepartmentTypeService.All());
                await Task.Delay(100);
                States = new ObservableCollection<State>(await StateService.All());

            }
            catch(Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error",e.Message,"Ok");
            }

        }

        private async Task<bool> validateInput()
        {
            if (string.IsNullOrEmpty(FullName))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un nombre para la actividad", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(ActivityDescription))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una descripción para la actividad", "ok");
                return false;
            }

            if (Scope == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un Ámbito", "ok");
                return false;
            }
            if (ActivityType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un tipo de actividad", "ok");
                return false;
            }
            if (ProjectType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger el tipo de proyecto", "ok");
                return false;
            }
            if (Project == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger el proyecto", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(StartDate) || string.IsNullOrEmpty(StartTime))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger fecha y hora de inicio de la actividad", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(EndDate) || string.IsNullOrEmpty(EndTime))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger fecha y hora de fin de la actividad", "ok");
                return false;
            }


            try
            {
                if (DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00") >= DateTime.Parse(EndDate + "T" + EndTime + ":00+0:00"))
                {
                    await App.Current.MainPage.DisplayAlert("Error", "La fecha final no puede ser anterior a la fecha inicial", "ok");
                    return false;
                }

                if(dateTimeValidation)
                {
                    if (DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00") <= DateTime.Now.AddHours(-5).AddDays(1))
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "La fecha inicial no puede ser inferior a 24 horas desde la fecha actual", "ok");
                        return false;
                    }
                }
                else
                {
                    if (DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00") <= DateTime.Now.AddHours(-5))
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "La fecha inicial no puede ser inferior a la fecha actual", "ok");
                        return false;
                    }
                }

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "ok");
                return false;
            }


            if (string.IsNullOrEmpty(DurationString))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una duración de la actividad", "ok");
                return false;
            }

            float.TryParse(DurationString, out float DurationFloat);

            Duration = (int)DurationFloat;

            float DayActivityDuration = (float)(App.Current.Properties.ContainsKey("DayActivityDuration") ? App.Current.Properties["DayActivityDuration"] : 0);
            float SecureActivityDurationDays = (float)(App.Current.Properties.ContainsKey("SecureActivityDurationDays") ? App.Current.Properties["SecureActivityDurationDays"] : 0);

            var TotalTime = (DayActivityDuration * SecureActivityDurationDays);

            try
            {
                int TotalDays = (int)Math.Ceiling((DateTime.Parse(EndDate + "T" + EndTime + ":00+0:00") - DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00")).TotalDays);
                int TotalHours = (int)Math.Ceiling((DateTime.Parse(EndDate + "T" + EndTime + ":00+0:00") - DateTime.Parse(StartDate + "T" + StartTime + ":00+0:00")).TotalHours);

                if ((TotalDays < SecureActivityDurationDays) && (Duration > TotalDays * DayActivityDuration))
                {
                    await App.Current.MainPage.DisplayAlert("Error", $"Las horas por día no pueden ser mayor a {DayActivityDuration} horas", "ok");
                    return false;
                }

                if ((TotalDays == 1) && (Duration > TotalHours))
                {
                    await App.Current.MainPage.DisplayAlert("Error", $"La duración no puede ser mayor a {TotalHours} horas por las fechas y horas escogidas", "ok");
                    return false;
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", e.Message, "ok");
                return false;
            }

            if (TotalTime > 0 && Duration > TotalTime)
            {
                
                await App.Current.MainPage.DisplayAlert("Error", "La duración de la actividad no puede ser mayor a " + TotalTime.ToString() + " horas", "ok");
                return false;
            }

            if (Department == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un departamento para la actividad", "ok");
                return false;
            }

            if (!SelectedCities.Any() && SelectedStates.Any())
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger Cantones de las provincias seleccionadas", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(Responsable))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un responsable", "ok");
                return false;
            }

            if (!string.IsNullOrEmpty(ResponsableEmail) && !Utils.IsValidEmail(ResponsableEmail))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un correo valido al responsable", "ok");
                return false;
            }

            if (!CheckTotal())
            {
                await App.Current.MainPage.DisplayAlert("Error", "La suma de los pesos no es 100%", "ok");
                return false;
            }

            return true;
        }

        private void CalculateWeightDistribution()
        {
            List<bool> Switches = new List<bool>
            {
                HasCourses,
                HasAcademicFormation,
                HasEni,
                HasLanguages,
                HasAges,
                HasActivityYears,
                HasDateLastActivity,
            };

            int cont = 0, loop = 0;

            bool[] indexes = new bool[Switches.Count];

            foreach (bool Switch in Switches)
            {
                indexes[loop] = Switch;

                if (Switch)
                    cont++;

                loop++;
            }

            int DefualtValue = (cont == 0) ? 0 : (100 / cont);

            for(int i = 0; i < Switches.Count; i++)
            {
                switch (i) {
                    case 0:
                        CoursesPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 1:
                        AcademicPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 2:
                        EniPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 3:
                        LanguagesPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 4:
                        AgesPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 5:
                        ActivityYearsPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                    case 6:
                        DateLastActivityPercentage = indexes[i] ? DefualtValue.ToString() : "0";
                        break;
                }
            }
        }

        private bool CheckTotal()
        {
            sum = 0;

            List<float> Values = new List<float>
            {
                float.Parse(CoursesPercentage),
                float.Parse(AcademicPercentage),
                float.Parse(EniPercentage),
                float.Parse(LanguagesPercentage),
                float.Parse(AgesPercentage),
                float.Parse(ActivityYearsPercentage),
                float.Parse(DateLastActivityPercentage),
            };

            Requirements = new List<Requirement> { };

            int cont = 0;
            foreach (float Value in Values)
            {
                if(Value > 0)
                {
                    switch (cont)
                    {
                        case 0:
                            Requirements.Add(new Requirement { Key = "Courses", Value = Value });
                            break;
                        case 1:
                            Requirements.Add(new Requirement { Key = "Academic", Value = Value });
                            break;
                        case 2:
                            Requirements.Add(new Requirement { Key = "Eni", Value = Value });
                            break;
                        case 3:
                            Requirements.Add(new Requirement { Key = "Languages", Value = Value });
                            break;
                        case 4:
                            Requirements.Add(new Requirement { Key = "Ages", Value = Value });
                            break;
                        case 5:
                            Requirements.Add(new Requirement { Key = "ActivityYears", Value = Value });
                            break;
                        case 6:
                            Requirements.Add(new Requirement { Key = "DateLastActivity", Value = Value });
                            break;
                    }
                    sum += Value;
                }
                cont++;
            }

            if (sum == 100 || sum == 0)
                return true;

            return false;
        }

        private void ChangeProject(TipoProyecto tipoProyecto)
        {

            Project = null;

            if (tipoProyecto != null)
                Projects = new ObservableCollection<Proyecto>(tipoProyecto.proyectos);
            else
                Projects = null;

        }

        private void ChangeScope(Ambito ambito)
        {
            ActivityType = null;
            if (ambito != null)
            {
                ActivityTypes = new ObservableCollection<TipoActividad>(ambito.tipoActividad);
                dateTimeValidation = ambito.nombre != "EVENTO ADVERSO";
            }
            else
                ActivityTypes = new ObservableCollection<TipoActividad>();
        }

        private void ChangeState(List<int> _states)
        {
            SelectedCities = new List<int>() { };
            if (_states.Any())
            {
                var _cities = new List<City>();
                int Cont = 0;
                foreach(State _state in States)
                {
                    if (_states.Contains(Cont))
                        _cities.AddRange(_state.cities);

                    Cont++;
                }
                Cities = new ObservableCollection<City>(_cities);
            }
            else
                Cities = new ObservableCollection<City>();
        }


        #endregion
    }

}