﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditRequiredFormationViewModel : BaseViewModel
    {

        private bool hasFormationLevelType = false;

        private ObservableCollection<FormationLevelType> formationLevelTypes;

        private ObservableCollection<AcademicTitleType> academicTitleTypes;

        private FormationLevelType formationLevelType;
        private AcademicTitleType academicTitleType;

        private string academicTitleTypeHeader;
        private string labelTitle;

        public bool HasFormationLevelType
        {
            get { return hasFormationLevelType; }
            set { SetValue(ref this.hasFormationLevelType, value); }
        }
        public ObservableCollection<FormationLevelType> FormationLevelTypes
        {
            get { return formationLevelTypes; }
            set { SetValue(ref this.formationLevelTypes, value); }
        }
        public ObservableCollection<AcademicTitleType> AcademicTitleTypes
        {
            get { return academicTitleTypes; }
            set { SetValue(ref this.academicTitleTypes, value); }
        }
        public FormationLevelType FormationLevelType
        {
            get { return formationLevelType; }
            set 
            { 
                SetValue(ref this.formationLevelType, value);
                ChangeFormationLevelType(value);
            }
        }
        public AcademicTitleType AcademicTitleType
        {
            get { return academicTitleType; }
            set { SetValue(ref this.academicTitleType, value); }
        }
        public string AcademicTitleTypeHeader
        {
            get { return academicTitleTypeHeader; }
            set { SetValue(ref this.academicTitleTypeHeader, value); }
        }
        public string LabelTitle
        {
            get { return labelTitle; }
            set { SetValue(ref this.labelTitle, value); }
        }
        public string Description { get; set; }
        public string CourseWeight { get; set; }
        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }
        public int RequiredKind { get; set; }
        public float WeightLeft { get; set; }



        public AddEditRequiredFormationViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            App.Current.MainPage.Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                var IdActivityString = App.Current.Properties.ContainsKey("IdActivity") ? (string)App.Current.Properties["IdActivity"] : "0";

                float.TryParse(IdActivityString, out float IdActivityFloat);

                float.TryParse(CourseWeight, out float Weight);

                var ActivityReqFormation = await ActivityReqFormationService.Store(new ActivityReqFormationRequest
                {
                    id_actividad = (int)IdActivityFloat,
                    id_tipo_titulo_academico = AcademicTitleType.id_tipo_titulo_academico,
                    descripcion = Description ?? "",
                    id_tipo_actividad_requisito = RequiredKind,
                    peso = (int)Weight
                });

                if(ActivityReqFormation != null)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Requisito registrado Correctamente", "ok");
                    await App.Current.MainPage.Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el requisito", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el requisito", "ok");
            }

        }

        public async void LoadData()
        {
            try
            {
                var IdActivity = App.Current.Properties.ContainsKey("IdActivity") ? App.Current.Properties["IdActivity"].ToString() : "0";

                string filter = $"id_actividad=ntin:{IdActivity}";

                switch (RequiredKind)
                {
                    case 2:
                        HasFormationLevelType = true;
                        LabelTitle = "Formación";
                        AcademicTitleTypeHeader = "Tipo de Formación";
                        filter += "&id_tipo_nivel_formacion=in:4,5,6";
                        break;
                    case 3:
                        LabelTitle = "Formación Eni";
                        AcademicTitleTypeHeader = "Tipo de Formación Eni";
                        filter += "&id_tipo_nivel_formacion=8";
                        break;
                    case 4:
                        LabelTitle = "Idioma";
                        AcademicTitleTypeHeader = "Idioma";
                        filter += "&id_tipo_nivel_formacion=7";
                        break;
                }

                FormationLevelTypes = new ObservableCollection<FormationLevelType>(await FormationLevelTypeService.All(filter));

                if (FormationLevelTypes != null && FormationLevelTypes.Any() && FormationLevelTypes.Count == 1)
                    FormationLevelType = FormationLevelTypes[0];

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {
            if(AcademicTitleType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de formación", "ok");
                return false;
            }

            if(RequiredKind == 2 && string.IsNullOrEmpty(Description))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una descripción a la opción de formación", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(CourseWeight))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar el peso de la opción de formación", "ok");
                return false;
            }

            float.TryParse(CourseWeight, out float Weight);

            if (Weight <= 0)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El peso no puede ser menor o igual 0", "ok");
                return false;
            }

            if (Weight > WeightLeft)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El peso no puede ser mayor a " + WeightLeft.ToString() + " %", "ok");
                return false;
            }

            return true;
        }


        private void ChangeFormationLevelType(FormationLevelType _formationLevelType)
        {
            if (_formationLevelType != null)
                AcademicTitleTypes = new ObservableCollection<AcademicTitleType>(_formationLevelType.academic_title_types);
            else
                AcademicTitleTypes = null;

            AcademicTitleType = null;
        }
    }
}
