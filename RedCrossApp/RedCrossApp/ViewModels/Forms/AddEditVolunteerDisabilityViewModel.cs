﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditVolunteerDisabilityViewModel : BaseViewModel
    {

        #region private properties
        private ObservableCollection<Disability> disabilities;
        private Disability disability;
        private int disabilityIndex = -1;
        private string percentage;
        private string description;
        private bool hasDescription = true;

        private int PercentageVal;

        #endregion

        public INavigation Navigation;
        public VolunteerDisability VolunteerDisability;

        public ObservableCollection<Disability> Disabilities
        {
            get { return disabilities; }
            set { SetValue(ref this.disabilities, value); }
        }
        public Disability Disability
        {
            get { return disability; }
            set { SetValue(ref this.disability, value); SetDisability(value); }
        }
        public int DisabilityIndex
        {
            get { return disabilityIndex; }
            set { SetValue(ref this.disabilityIndex, value); }
        }
        public string Percentage
        {
            get { return percentage; }
            set { SetValue(ref this.percentage, value); }
        }
        public string Description
        {
            get { return description; }
            set { SetValue(ref this.description, value); }
        }
        public bool HasDescription
        {
            get { return hasDescription; }
            set { SetValue(ref this.hasDescription, value); }
        }

        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }

        public AddEditVolunteerDisabilityViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            if(Navigation != null)
                Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                if (!HasDescription)
                    Description = "";

                var Response = await VolunteerDisabilityService.Store(new VolunteerDisabilityRequest {
                    id_tipo_discapacidad = Disability.id_tipo_discapacidad,
                    porcentaje = PercentageVal,
                    descripcion = Description ?? ""
                });

                if(Response)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Discapacidad registrada Correctamente", "ok");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar la Discapacidad", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar la Discapacidad", "ok");
            }

        }

        public async void LoadData()
        {
            try
            {

                Disabilities = new ObservableCollection<Disability>(await DisabilityService.All());

                if(VolunteerDisability != null)
                {

                    int index = 0;

                    Description = VolunteerDisability.descripcion?.ToString() ?? "";

                    Percentage = VolunteerDisability.porcentaje.ToString();

                    foreach (Disability _disability in Disabilities)
                    {
                        if (_disability.id_tipo_discapacidad == VolunteerDisability.id_tipo_discapacidad)
                        {
                            DisabilityIndex = index;
                            break;
                        }

                        index++;
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {

            if (Disability == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una discapacidad", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(Percentage))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un porcentaje", "ok");
                return false;
            }

            int.TryParse(Percentage, out PercentageVal);

            if (PercentageVal > 100)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El porcentaje no puede ser mayor al 100", "ok");
                return false;
            }

            if (PercentageVal < 1)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El porcentaje no puede ser menor a 1", "ok");
                return false;
            }

            return true;
        }

        private void SetDisability(Disability _disability)
        {
            HasDescription = !(_disability != null && _disability.id_tipo_discapacidad != 1);
        }

    }
}
