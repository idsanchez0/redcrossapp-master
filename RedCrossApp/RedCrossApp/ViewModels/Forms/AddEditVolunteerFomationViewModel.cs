﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditVolunteerFomationViewModel : BaseViewModel
    {

        #region private properties
        private ObservableCollection<FormationLevelType> formationLevelTypes;
        private ObservableCollection<AcademicTitleType> academicTitleTypes;
        private FormationLevelType formationLevelType;
        private AcademicTitleType academicTitleType;
        private int formationLevelTypeIndex = -1;
        private int academicTitleTypeIndex = -1;
        private bool hasDescription;
        private string description;
        private bool hasActiveFormation = false;
        private string currentFormationLevel;
        private int currentFormationModalityIndex = -1;

        #endregion

        public INavigation Navigation;
        public VolunteerFormation VolunteerFormation;

        public ObservableCollection<FormationLevelType> FormationLevelTypes
        {
            get { return formationLevelTypes; }
            set { SetValue(ref this.formationLevelTypes, value); }
        }
        public ObservableCollection<AcademicTitleType> AcademicTitleTypes
        {
            get { return academicTitleTypes; }
            set { SetValue(ref this.academicTitleTypes, value); }
        }
        public FormationLevelType FormationLevelType
        {
            get { return formationLevelType; }
            set 
            { 
                SetValue(ref this.formationLevelType, value);
                ChangeFormationLevelType(value);
            }
        }
        public AcademicTitleType AcademicTitleType
        {
            get { return academicTitleType; }
            set { SetValue(ref this.academicTitleType, value); }
        }
        public int FormationLevelTypeIndex
        {
            get { return formationLevelTypeIndex; }
            set { SetValue(ref this.formationLevelTypeIndex, value); }
        }
        public int AcademicTitleTypeIndex
        {
            get { return academicTitleTypeIndex; }
            set { SetValue(ref this.academicTitleTypeIndex, value); }
        }
        public bool HasDescription
        {
            get { return hasDescription; }
            set { SetValue(ref this.hasDescription, value); }
        }
        public string Description
        {
            get { return description; }
            set { SetValue(ref this.description, value); }
        }
        public bool HasActiveFormation
        {
            get { return hasActiveFormation; }
            set { SetValue(ref this.hasActiveFormation, value); }
        }
        public string CurrentFormationLevel
        {
            get { return currentFormationLevel; }
            set { SetValue(ref this.currentFormationLevel, value); }
        }
        public int CurrentFormationModalityIndex
        {
            get { return currentFormationModalityIndex; }
            set { SetValue(ref this.currentFormationModalityIndex, value); }
        }
        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }

        public AddEditVolunteerFomationViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            if(Navigation != null)
                Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                int? FormationLevel = null;
                string FormationModality = "";

                if(HasActiveFormation)
                {
                    if (!string.IsNullOrEmpty(CurrentFormationLevel))
                    {
                        int.TryParse(CurrentFormationLevel, out int tmp);
                        FormationLevel = tmp;
                    }

                    FormationModality = GetCurrentFormationModality();
                }

                if (!HasDescription)
                    Description = null;

                var Response = await VolunteerFormationService.Store(new VolunteerFormationRequest {
                    id_tipo_titulo_academico = AcademicTitleType.id_tipo_titulo_academico,
                    descripcion = Description,
                    formacion_en_curso = HasActiveFormation,
                    id_voluntario_formacion = VolunteerFormation?.id_voluntario_formacion ?? null,
                    modalidad_formacion_en_curso = FormationModality,
                    nivel_formacion_en_curso = FormationLevel
                });

                if(Response)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Nivel académico registrado Correctamente", "ok");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el Nivel académico", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el Nivel académico", "ok");
            }

        }

        public async void LoadData()
        {
            try
            {

                FormationLevelTypes = new ObservableCollection<FormationLevelType>(await FormationLevelTypeService.All("id_tipo_nivel_formacion=in:1,2,3,4,5,6"));

                if (FormationLevelTypes != null && FormationLevelTypes.Any() && FormationLevelTypes.Count == 1)
                    FormationLevelType = FormationLevelTypes[0];

                if(VolunteerFormation != null)
                {
                    Description = VolunteerFormation?.descripcion?.ToString() ?? "";
                    HasActiveFormation = VolunteerFormation?.formacion_en_curso ?? false;
                    CurrentFormationModalityIndex = SetCurrentFormationModality(VolunteerFormation?.modalidad_formacion_en_curso?.ToString() ?? "");
                    CurrentFormationLevel = VolunteerFormation?.nivel_formacion_en_curso?.ToString() ?? "";

                    int index = 0;

                    foreach(FormationLevelType _levelType in FormationLevelTypes)
                    {
                        foreach(AcademicTitleType _title in _levelType.academic_title_types)
                        {
                            if(_title.id_tipo_titulo_academico == VolunteerFormation.id_tipo_titulo_academico)
                            {
                                FormationLevelTypeIndex = index;
                                HasDescription = _levelType.requiere_descripcion;
                                break;
                            }

                        }

                        if (FormationLevelTypeIndex != -1)
                            break;

                        index++;
                    }

                    index = 0;
                    foreach (AcademicTitleType _title in AcademicTitleTypes)
                    {
                        if (_title.id_tipo_titulo_academico == (VolunteerFormation?.id_tipo_titulo_academico ?? -1))
                        {
                            AcademicTitleTypeIndex = index;
                            break;
                        }

                        index++;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {
            if(FormationLevelType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de Tipo de nivel de Formación", "ok");
                return false;
            }

            if (AcademicTitleType == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de Tipo de título Académico", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(Description) && HasDescription)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar el Registro de Título (SENESCYT/MEC)", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(CurrentFormationLevel) && HasActiveFormation)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar el Nivel que se encuentra Cursando", "ok");
                return false;
            }

            if (CurrentFormationModalityIndex == -1 && HasActiveFormation)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger una opción de la Modalidad de la formación en curso", "ok");
                return false;
            }

            return true;
        }


        private void ChangeFormationLevelType(FormationLevelType _formationLevelType)
        {
            if (_formationLevelType != null)
            {
                AcademicTitleTypes = new ObservableCollection<AcademicTitleType>(_formationLevelType.academic_title_types);
                HasDescription = _formationLevelType.requiere_descripcion;
            }
            else
            {
                AcademicTitleTypes = new ObservableCollection<AcademicTitleType>();
                HasDescription = false;
            }

            AcademicTitleType = null;
        }

        string GetCurrentFormationModality()
        {
            switch(CurrentFormationModalityIndex)
            {
                case 0:
                    return "AÑO";
                case 1:
                    return "SEMESTRE";
                case 2:
                    return "QUIMESTRE";
                case 3:
                    return "TRIMESTRE";
                case 4:
                    return "BIMESTRE";
                default:
                    return "";
            }
        }

        int SetCurrentFormationModality(string FormationModality)
        {
            switch (FormationModality)
            {
                case "AÑO":
                    return 0;
                case "SEMESTRE":
                    return 1;
                case "QUIMESTRE":
                    return 2;
                case "TRIMESTRE":
                    return 3;
                case "BIMESTRE":
                    return 4;
                default:
                    return -1;
            }
        }
    }
}
