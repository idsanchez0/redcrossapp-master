﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using RedCrossApp.Models;
using RedCrossApp.Services;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    public class AddEditRequiredCourseViewModel : BaseViewModel
    {

        private ObservableCollection<CourseType> courses;
        public ObservableCollection<CourseType> Courses
        {
            get { return courses; }
            set { SetValue(ref this.courses, value); }
        }
        public CourseType Course { get; set; }
        public string CourseWeight { get; set; }
        public Command SubmitCommand { get; set; }
        public Command CancelCommand { get; set; }

        public float WeightLeft { get; set; }

        public AddEditRequiredCourseViewModel()
        {

            this.SubmitCommand = new Command(this.SubmitClicked);

            this.CancelCommand = new Command(this.CancelClicked);

        }

        private void CancelClicked(object obj)
        {
            App.Current.MainPage.Navigation.PopAsync();
        }

        private async void SubmitClicked(object obj)
        {

            try
            {
                var isValid = await ValidateInput();

                if (!isValid)
                    return;

                var IdActivityString = App.Current.Properties.ContainsKey("IdActivity") ? (string)App.Current.Properties["IdActivity"] : "0";

                float.TryParse(IdActivityString, out float IdActivityFloat);

                float.TryParse(CourseWeight, out float Weight);

                var ActivityReqCourse = await ActivityReqCourseService.Store(new ActivityReqCourseRequest
                {
                    id_actividad = (int)IdActivityFloat,
                    id_tipo_curso = Course.id_tipo_curso,
                    id_tipo_actividad_requisito = 1,
                    peso = (int)Weight
                });

                if(ActivityReqCourse != null)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Requisito registrado Correctamente", "ok");
                    await App.Current.MainPage.Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar el requisito", "ok");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public async void LoadData()
        {
            try
            {
                var IdActivity = App.Current.Properties.ContainsKey("IdActivity") ? App.Current.Properties["IdActivity"].ToString() : "0";

                Courses = new ObservableCollection<CourseType>(await CourseTypeService.All($"id_actividad=ntin:{IdActivity}"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task<bool> ValidateInput()
        {
            if(Course == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un curso", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(CourseWeight))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar el peso del curso", "ok");
                return false;
            }

            float.TryParse(CourseWeight, out float Weight);

            if (Weight <= 0)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El peso no puede ser menor o igual 0", "ok");
                return false;
            }

            if (Weight > WeightLeft)
            {
                await App.Current.MainPage.DisplayAlert("Error", "El peso no puede ser mayor a " + WeightLeft.ToString() + " %", "ok");
                return false;
            }

            return true;
        }
    }
}
