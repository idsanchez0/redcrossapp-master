﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    class FileContactViewModel : BaseViewModel
    {
        #region private properties
        private string mail;
        private string homePhoneNumber;
        private string phoneNumber;
        private string mainStreet;
        private string secondStreet;
        private string homeNumber;
        private string place;
        private string reference;
        private string emergencyContact;
        private string relation;
        private string emergencyContactPhone;
        #endregion

        #region public properties
        public string Mail
        {
            get { return mail; }
            set { SetValue(ref this.mail, value); }
        }
        public string HomePhoneNumber
        {
            get { return homePhoneNumber; }
            set { SetValue(ref this.homePhoneNumber, value); }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { SetValue(ref this.phoneNumber, value); }
        }
        public string MainStreet
        {
            get { return mainStreet; }
            set { SetValue(ref this.mainStreet, value); }
        }
        public string SecondStreet
        {
            get { return secondStreet; }
            set { SetValue(ref this.secondStreet, value); }
        }
        public string HomeNumber
        {
            get { return homeNumber; }
            set { SetValue(ref this.homeNumber, value); }
        }
        public string Place
        {
            get { return place; }
            set { SetValue(ref this.place, value); }
        }
        public string Reference
        {
            get { return reference; }
            set { SetValue(ref this.reference, value); }
        }
        public string EmergencyContact
        {
            get { return emergencyContact; }
            set { SetValue(ref this.emergencyContact, value); }
        }
        public string Relation
        {
            get { return relation; }
            set { SetValue(ref this.relation, value); }
        }
        public string EmergencyContactPhone
        {
            get { return emergencyContactPhone; }
            set { SetValue(ref this.emergencyContactPhone, value); }
        }
        public Command SubmitCommand { get; set; }
        #endregion
        public FileContactViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }
        public async void LoadData()
        {
            try
            {

                var Volunteer = await Auth.VolunteerData();

                Console.WriteLine("Volunteer loaded");

                Mail = Volunteer?.email?.ToString() ?? "0";
                HomePhoneNumber = Volunteer?.personal_telef_conv?.ToString() ?? "0";
                PhoneNumber = Volunteer?.personal_telef_cel?.ToString() ?? "";
                MainStreet = Volunteer?.direccion_domicilio?.ToString() ?? "";
                SecondStreet = Volunteer?.direccion_domicilio_secundaria?.ToString() ?? "";
                HomeNumber = Volunteer?.direccion_numeracion?.ToString() ?? "";
                Place = Volunteer?.direccion_sector?.ToString() ?? "";
                Reference = Volunteer?.direccion_referencia?.ToString() ?? "";
                EmergencyContact = Volunteer?.siniestro_contacto?.ToString() ?? "";
                Relation = Volunteer?.siniestro_parentesco?.ToString() ?? "";
                EmergencyContactPhone = Volunteer?.siniestro_telef_cel?.ToString() ?? "";

                Console.WriteLine("Loaded the plain entrys");

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }
        private async void SubmitClicked(object obj)
        {
            try
            {

                if (!await InputValidate())
                    return;

                var Volunteer = await Auth.UpdateVolunteerData(new VolunteerContactRequest() {
                    email = Mail,
                    personal_telef_conv = HomePhoneNumber,
                    personal_telef_cel = PhoneNumber,
                    direccion_domicilio = MainStreet.ToUpper(),
                    direccion_domicilio_secundaria = SecondStreet.ToUpper(),
                    direccion_numeracion = HomeNumber.ToUpper(),
                    direccion_sector = Place.ToUpper(),
                    direccion_referencia = Reference.ToUpper(),
                    siniestro_contacto = EmergencyContact.ToUpper(),
                    siniestro_parentesco = Relation.ToUpper(),
                    siniestro_telef_cel = EmergencyContactPhone
                });

                if (Volunteer != null)
                    await App.Current.MainPage.DisplayAlert("Alerta", "Datos Actualizados","ok");
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudo registrar sus datos", "ok");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private async Task<bool> InputValidate()
        {

            if (string.IsNullOrEmpty(Mail))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un correo", "ok");
                return false;
            }

            if (!Utils.IsValidEmail(Mail))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un correo válido", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(MainStreet))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar la calle principal", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(SecondStreet))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar la calle secundaria", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(HomeNumber))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar el número de casa", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(Place))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un sector", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(Reference))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar una referencia", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(PhoneNumber))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un número personal", "ok");
                return false;
            }

            return true;
        }
    }
}
