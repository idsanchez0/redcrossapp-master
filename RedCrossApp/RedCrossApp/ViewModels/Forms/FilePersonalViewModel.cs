﻿using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.Forms
{
    class FilePersonalViewModel : BaseViewModel
    {

        #region private properties
        private string name;
        private string lastName;
        private string dob;
        private int gender = -1;
        private int sex = -1;
        private int civil = -1;
        private ObservableCollection<Country> countries;
        private int nationality = -1;
        private string birthPlace;
        private ObservableCollection<CulturalIdentification> culturalIdentities;
        private int culturalIdentity = -1;
        private int bloodType = -1;
        private int bloodFactor = -1;
        private bool hasIESS = false;
        private bool hasSecurity = false;
        private string privateSecurity;
        private bool hasDisabilityDocument;
        #endregion

        #region public properties
        public string Name
        {
            get { return name; }
            set { SetValue(ref this.name, value); }
        }
        public string LastName
        {
            get { return lastName; }
            set { SetValue(ref this.lastName, value); }
        }
        public string Dob
        {
            get { return dob; }
            set { SetValue(ref this.dob, value); }
        }
        public int Gender
        {
            get { return gender; }
            set { SetValue(ref this.gender, value); }
        }
        public int Sex
        {
            get { return sex; }
            set { SetValue(ref this.sex, value); }
        }
        public int Civil
        {
            get { return civil; }
            set { SetValue(ref this.civil, value); }
        }
        public ObservableCollection<Country> Countries
        {
            get { return countries; }
            set { SetValue(ref this.countries, value); }
        }
        public int Nationality
        {
            get { return nationality; }
            set { SetValue(ref this.nationality, value); }
        }
        public string BirthPlace
        {
            get { return birthPlace; }
            set { SetValue(ref this.birthPlace, value); }
        }
        public ObservableCollection<CulturalIdentification> CulturalIdentities
        {
            get { return culturalIdentities; }
            set { SetValue(ref this.culturalIdentities, value); }
        }
        public int CulturalIdentity
        {
            get { return culturalIdentity; }
            set { SetValue(ref this.culturalIdentity, value); }
        }
        public int BloodType
        {
            get { return bloodType; }
            set { SetValue(ref this.bloodType, value); }
        }
        public int BloodFactor
        {
            get { return bloodFactor; }
            set { SetValue(ref this.bloodFactor, value); }
        }
        public bool HasIESS
        {
            get { return hasIESS; }
            set { SetValue(ref this.hasIESS, value); }
        }
        public bool HasSecurity
        {
            get { return hasSecurity; }
            set { SetValue(ref this.hasSecurity, value); }
        }
        public string PrivateSecurity
        {
            get { return privateSecurity; }
            set { SetValue(ref this.privateSecurity, value); }
        }
        public bool HasDisabilityDocument
        {
            get { return hasDisabilityDocument; }
            set { SetValue(ref this.hasDisabilityDocument, value); }
        }
        public Command SubmitCommand { get; set; }
        #endregion

        public FilePersonalViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }

        public async void LoadData()
        {
            try
            {

                Countries = new ObservableCollection<Country>(await CountryService.All());

                CulturalIdentities = new ObservableCollection<CulturalIdentification>(await CulturalIdentificationService.All()); 

                var Volunteer = await Auth.VolunteerData();

                Console.WriteLine("Volunteer loaded");

                Name = Volunteer?.nombres?.ToString() ?? "";
                LastName = Volunteer?.apellidos?.ToString() ?? "";
                Dob = Volunteer?.fecha_nacimiento?.ToString("yyyy-MM-dd") ?? "";
                Gender = SetGender(Volunteer?.genero?.ToString() ?? "");
                Sex = SetSex(Volunteer?.sexo?.ToString() ?? "");
                Civil = (Volunteer?.id_tipo_estado_civil ?? 0) - 1;
                Nationality = SetNationality(Volunteer?.id_nacionalidad);
                BirthPlace = Volunteer?.lugar_nacimiento?.ToString() ?? "";
                CulturalIdentity = SetCulturalIdentity(Volunteer?.id_tipo_identificacion_cultural);
                BloodType = SetBloodType(Volunteer?.grupo_sanguineo?.ToString() ?? "");
                BloodFactor = SetBloodFactor(Volunteer?.factor_rh?.ToString() ?? "");
                HasIESS = Volunteer?.seg_seguro_social ?? false;
                HasSecurity = Volunteer?.seg_seguro_privado ?? false;
                PrivateSecurity = Volunteer?.seg_seguro_privado_desc?.ToString() ?? "";
                HasDisabilityDocument = Volunteer?.hist_clin_carnet_discapacidad ?? false;

                Console.WriteLine("Loaded the plain entrys");

            }
            catch (Exception e)
            {
                Console.WriteLine("Error in LoadData: " + e.Message);
            }
        }

        private async void SubmitClicked(object obj)
        {
            try
            {
                var ValidInput = await InputValidate();

                if (!ValidInput)
                    return;

                int? _civil = null;

                if (Civil > 0)
                    _civil = Civil + 1;
                    
                var Volunteer = await Auth.UpdateVolunteerData(new VolunteerPersonalRequest() {
                    nombres = Name.ToUpper(),
                    apellidos = LastName.ToUpper(),
                    genero = GetGender()?.ToString() ?? null,
                    sexo = GetSex()?.ToString() ?? null,
                    id_tipo_estado_civil = _civil,
                    id_nacionalidad = GetNationality(),
                    lugar_nacimiento = BirthPlace.ToUpper(),
                    id_tipo_identificacion_cultural = GetCulturalIdentity(),
                    grupo_sanguineo = GetBloodType()?.ToString() ?? null,
                    factor_rh = GetBloodFactor()?.ToString() ?? null,
                    seg_seguro_privado = HasIESS,
                    seg_seguro_social = HasSecurity,
                    seg_seguro_privado_desc = PrivateSecurity,
                    hist_clin_carnet_discapacidad = HasDisabilityDocument
                });

                if (Volunteer != null)
                    await App.Current.MainPage.DisplayAlert("Alerta", "Datos Actualizados","ok");
                else
                    await App.Current.MainPage.DisplayAlert("Error", "Error en el servicio, verifique su conexión a Internet", "ok");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task<bool> InputValidate()
        {
            if (string.IsNullOrEmpty(Name))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar sus nombres", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(LastName))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar sus apellidos", "ok");
                return false;
            }

            if (GetGender() == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe seleccionar su género", "ok");
                return false;
            }

            if (GetSex() == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe seleccionar su sexo", "ok");
                return false;
            }

            if (Civil == -1)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un estado civil", "ok");
                return false;
            }

            if (GetBloodType() == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un grupo sanguíneo", "ok");
                return false;
            }

            if (GetBloodFactor() == null)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe escoger un factor RH", "ok");
                return false;
            }

            if (string.IsNullOrEmpty(PrivateSecurity) && HasSecurity)
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe registrar la empresa de seguro privada", "ok");
                return false;
            }
            return true;
        }

        private int SetGender(string _Gender)
        {
            switch (_Gender)
            {
                case null:
                    return -1;
                case "MASCULINO":
                    return 0;
                case "FEMENINO":
                    return 1;
            }

            return -1;
        }
        private object GetGender()
        {
            switch (Gender)
            {
                case -1:
                    return null;
                case 0:
                    return "MASCULINO";
                case 1:
                    return "FEMENINO";
            }

            return null;
        }
        private int SetSex(string _Sex)
        {
            switch (_Sex)
            {
                case null:
                    return -1;
                case "HOMBRE":
                    return 0;
                case "MUJER":
                    return 1;
            }

            return -1;
        }
        private object GetSex()
        {
            switch (Sex)
            {
                case -1:
                    return null;
                case 0:
                    return "HOMBRE";
                case 1:
                    return "MUJER";
            }

            return null;
        }
        private int SetNationality(int? index)
        {
            int CountryCont = 0;

            if (index != null && Countries.Any())
            {
                foreach (Country _country in Countries)
                {
                    if (_country.id_pais == index)
                        return CountryCont;

                    CountryCont++;
                }

            }

            return -1;
        }
        private int? GetNationality()
        {
            int CountryCont = 0;

            if (Countries.Any())
            {
                foreach (Country _country in Countries)
                {
                    if (CountryCont == Nationality)
                        return _country.id_pais;

                    CountryCont++;
                }

            }

            return null;
        }
        private int SetCulturalIdentity(int? index)
        {
            int CulturalIdentityCont = 0;

            if (index != null && CulturalIdentities.Any())
            {
                foreach (CulturalIdentification _cultural in CulturalIdentities)
                {
                    if (_cultural.id_tipo_identificacion_cultural == index)
                        return CulturalIdentityCont;

                    CulturalIdentityCont++;
                }

            }

            return -1;
        }
        private int? GetCulturalIdentity()
        {
            int CulturalIdentityCont = 0;

            if (CulturalIdentities.Any())
            {
                foreach (CulturalIdentification _cultural in CulturalIdentities)
                {
                    if (CulturalIdentityCont == CulturalIdentity)
                        return _cultural.id_tipo_identificacion_cultural;

                    CulturalIdentityCont++;
                }

            }

            return null;
        }
        private int SetBloodType(string _BloobType)
        {
            switch (_BloobType)
            {
                case null:
                    return -1;
                case "O":
                    return 0;
                case "A":
                    return 1;
                case "B":
                    return 2;
                case "AB":
                    return 3;
            }
            return -1;
        }
        private object GetBloodType()
        {
            switch (BloodType)
            {
                case -1:
                    return null;
                case 0:
                    return "O";
                case 1:
                    return "A";
                case 2:
                    return "B";
                case 3:
                    return "AB";
            }
            return null;
        }
        private int SetBloodFactor(string _BloodFactor)
        {
            switch (_BloodFactor)
            {
                case null:
                    return -1;
                case "POSITIVO":
                    return 0;
                case "NEGATIVO":
                    return 1;
            }
            return -1;
        }
        private object GetBloodFactor()
        {
            switch (BloodFactor)
            {
                case -1:
                    return null;
                case 0:
                    return "POSITIVO";
                case 1:
                    return "NEGATIVO";
            }
            return null;
        }
    }
}
