﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.Requirements
{
    /// <summary>
    /// ViewModel for Business Registration Form page 
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class DefualtRequirementViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DefualtRequirementViewModel" /> class
        /// </summary>
        public DefualtRequirementViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }
        #endregion

        #region Local Variables

        private string validationType;
        private string firstVal;
        private string secondVal;
        private bool secondValVisible = false;
        private bool successMessageVisible = false;

        #endregion

        #region Properties
        public string ValidationType 
        {
            get { return validationType; }
            set 
            { 
                SetValue(ref this.validationType, value); 
                ChangeRequirementType(value); 
            }
        }
        public string FirstVal
        {
            get { return firstVal; }
            set { SetValue(ref this.firstVal, value); }
        }
        public string SecondVal
        {
            get { return secondVal; }
            set { SetValue(ref this.secondVal, value); }
        }
        public bool SecondValVisible
        {
            get { return secondValVisible; }
            set { SetValue(ref this.secondValVisible, value); }
        }
        public bool SuccessMessageVisible
        {
            get { return successMessageVisible; }
            set { SetValue(ref this.successMessageVisible, value); }
        }
        public int RequirementKind { get; set; }
        public float Weight { get; set; }
        public Activity Activity { get; set; }

        #endregion 

        #region Comments

        /// <summary>
        /// Gets or sets the command is executed when the Submit button is clicked.
        /// </summary>
        public Command SubmitCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Submit button clicked
        /// </summary>
        /// <param name="obj">The object</param>
        private async void SubmitClicked(Object obj)
        {
            if (string.IsNullOrEmpty(FirstVal))
            {
                await App.Current.MainPage.DisplayAlert("Error", "El valor 1 no puede estar vacio", "ok");
                return;
            }

            var IsValid = Validate();
            
            if(!IsValid)
            {
                await App.Current.MainPage.DisplayAlert("Error","El valor inicial no puede ser inferior al final","ok");
                return;
            }
            string TipoRango;

            switch (ValidationType)
            {
                case "Igual":
                    TipoRango = "eq";
                    break;
                case "Mayor que":
                    TipoRango = "gt";
                    break;
                case "Menor que":
                    TipoRango = "lt";
                    break;
                case "Entre":
                    TipoRango = "bw";
                    break;
                default:
                    await App.Current.MainPage.DisplayAlert("Error", "Debe Escoger un rango", "ok");
                    return;
            };

            var ActivityReqExtra = await ActivityReqExtrasService.Store(new ActivityReqExtraRequest() 
            {
                id_actividad = Activity.id_actividad,
                id_tipo_actividad_requisito = RequirementKind,
                tipo_rango = TipoRango,
                peso = (int)Weight,
                valor1 = FirstVal,
                valor2 = SecondVal,
            });

            await Task.Delay(100);

            if(ActivityReqExtra != null)
            {
                await App.Current.MainPage.DisplayAlert("Alerta", "Se grabó el requisito", "ok");
                SuccessMessageVisible = true;

                App.Current.Properties.Remove("HasPendingRequirement");
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo grabar el requisito", "ok");
                SuccessMessageVisible = false;
            }
        }

        private void ChangeRequirementType(string value)
        {
            if(value == "Entre")
            {
                SecondValVisible = true;
            }
            else
            {
                SecondValVisible = false;
                SecondVal = string.Empty;
            }
        }

        private bool Validate()
        {
            try
            {
                if (ValidationType == "Entre")
                {
                    if (RequirementKind == 8)
                    {
                    
                            var FirstDate = DateTime.Parse(FirstVal + "T00:00:00+0:00");
                            var SecondDate = DateTime.Parse(SecondVal + "T00:00:00+0:00");

                            if (FirstDate < SecondDate)
                                return false;
                    }
                    else
                    {
                        if (float.Parse(FirstVal) < float.Parse(SecondVal))
                            return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}