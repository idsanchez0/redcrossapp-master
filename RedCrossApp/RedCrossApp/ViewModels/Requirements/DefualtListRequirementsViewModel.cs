﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.Requirements
{
    /// <summary>
    /// ViewModel for Business Registration Form page 
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class DefualtListRequirementsViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DefualtRequirementViewModel" /> class
        /// </summary>
        public DefualtListRequirementsViewModel()
        {
            this.AddRequirementCommand = new Command(this.AddRequirementMethod);

            DeleteCommand = new Command<RequirementListModel>(async (Requirement) => await DeleteMethod(Requirement));
        }

        #endregion

        #region Local Variables

        private bool successMessageVisible = false;
        private ObservableCollection<RequirementListModel> requirements;

        #endregion

        #region Properties
        public bool SuccessMessageVisible
        {
            get { return successMessageVisible; }
            set { SetValue(ref this.successMessageVisible, value); }
        }

        public ObservableCollection<RequirementListModel> Requirements
        {
            get { return requirements; }
            set { SetValue(ref this.requirements, value); }
        }

        public int RequirementKind { get; set; }
        public float Weight { get; set; }

        private float WeightLeft = 0;
        public Activity Activity { get; set; }


        #endregion 

        #region Commands

        /// <summary>
        /// Gets or sets the command is executed when the Submit button is clicked.
        /// </summary>
        public Command AddRequirementCommand { get; set; }
        public ICommand DeleteCommand { get; private set; }

        #endregion

        #region Methods

        private async void AddRequirementMethod(object obj)
        {
            if (WeightLeft <= 0)
            {
                await App.Current.MainPage.DisplayAlert("Alerta","No se puede definir mas requisitos, el total de peso asignado se alcanzó","ok");
                return;
            }
            switch(RequirementKind)
            {
                case 1:
                    await App.Current.MainPage.Navigation.PushAsync(new Views.Forms.AddEditRequiredCoursesPage(WeightLeft));
                    break;
                case 2:
                case 3:
                case 4:
                    await App.Current.MainPage.Navigation.PushAsync(new Views.Forms.AddEditRequiredFormationPage(RequirementKind, WeightLeft));
                    break;
            }
        }

        public async void LoadData()
        {

            var IdActivity = App.Current.Properties.ContainsKey("IdActivity") ? App.Current.Properties["IdActivity"].ToString() : "0";

            WeightLeft = Weight;

            var _requirements = new ObservableCollection<RequirementListModel>();

            string filter = $"id_actividad={IdActivity}";

            List<ActivityReqFormation> ActivityReqFormations;

            switch(RequirementKind)
            {
                case 2:
                case 4:
                case 3:
                    filter += "&id_tipo_actividad_requisito=" + RequirementKind.ToString();
                    break;
            }

            //await App.Current.MainPage.DisplayAlert("Alerta", IdActivity,"ok");

            switch (RequirementKind)
            {
                case 1:
                    var Courses = await ActivityReqCourseService.All(filter);
                    
                    if(Courses != null && Courses.Any())
                    {
                        foreach (ActivityReqCourse _activityReqCourse in Courses)
                        {
                            _requirements.Add(new RequirementListModel
                            {
                                Index = _activityReqCourse.id_actividad_req_curso,
                                Name = _activityReqCourse.course_type.nombre,
                                Weight = _activityReqCourse.peso
                            });
                            WeightLeft -= _activityReqCourse.peso;
                        }
                    }
                    
                    break;

                case 2:
                    ActivityReqFormations = await ActivityReqFormationService.All(filter);

                    if (ActivityReqFormations != null && ActivityReqFormations.Any())
                    {

                        foreach (ActivityReqFormation _activityReqFormation in ActivityReqFormations)
                        {
                            _requirements.Add(new RequirementListModel
                            {
                                Index = _activityReqFormation.id_actividad_req_formacion,
                                Name = _activityReqFormation.academic_title_type.nombre + " - " + _activityReqFormation.descripcion,
                                Weight = _activityReqFormation.peso
                            });

                            WeightLeft -= _activityReqFormation.peso;
                        }

                    }
                    break;
                case 3:
                case 4:
                    ActivityReqFormations = await ActivityReqFormationService.All(filter);

                    if (ActivityReqFormations != null && ActivityReqFormations.Any())
                    {

                        foreach (ActivityReqFormation _activityReqFormation in ActivityReqFormations)
                        {
                            _requirements.Add(new RequirementListModel
                            {
                                Index = _activityReqFormation.id_actividad_req_formacion,
                                Name = _activityReqFormation.academic_title_type.nombre,
                                Weight = _activityReqFormation.peso
                            });

                            WeightLeft -= _activityReqFormation.peso;
                        }

                    }
                    break;
            }

            Requirements = _requirements;

            Console.WriteLine("Datos de la lista de requrimientos cargados");
        }

        private async Task DeleteMethod(RequirementListModel obj)
        {

            bool Response;

            switch (RequirementKind)
            {
                case 1:
                    Response = await ActivityReqCourseService.Delete(obj.Index);
                    break;
                case 2:
                case 3:
                case 4:
                    Response = await ActivityReqFormationService.Delete(obj.Index);
                    break;
                default:
                    Response = false;
                    break;
            }

            if (Response)
            {
                await App.Current.MainPage.DisplayAlert("Alerta","Se borro correctamente el requerimiento","Ok");
                LoadData();
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Error", "Problemas borrando el requerimiento", "Ok");
            }
        }

        #endregion
    }
}