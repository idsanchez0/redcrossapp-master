﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.Requirements
{
    /// <summary>
    /// ViewModel for Business Registration Form page 
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class FinishViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DefualtRequirementViewModel" /> class
        /// </summary>
        public FinishViewModel()
        {
            this.SubmitCommand = new Command(this.SubmitClicked);
        }
        #endregion

        #region Local Variables

        #endregion

        #region Properties
        public int RequirementKind { get; set; }
        public float Weight { get; set; }
        public Activity Activity { get; set; }

        #endregion 

        #region Comments

        /// <summary>
        /// Gets or sets the command is executed when the Submit button is clicked.
        /// </summary>
        public Command SubmitCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Invoked when the Submit button clicked
        /// </summary>
        /// <param name="obj">The object</param>
        private async void SubmitClicked(Object obj)
        {
            var Confirmation = await App.Current.MainPage.DisplayAlert("Alerta", "Una vez confirme los requisitos no podra editarlos, ¿Desea Cerrar el ingreso de requisitos?", "Ok", "Cancelar");

            if (Confirmation)
            {
                UserDialogs.Instance.ShowLoading("Verificando...", MaskType.Black);

                bool Approved = await ActivityService.CheckAndApprove(Activity.id_actividad);

                UserDialogs.Instance.HideLoading();

                if (Approved)
                    App.Current.MainPage = new MenuPage();
                else
                    await App.Current.MainPage.DisplayAlert("Alerta", "Aún no ha culminado el ingreso de requisitos de la actividad", "Ok");
            }
        }

        #endregion
    }
}