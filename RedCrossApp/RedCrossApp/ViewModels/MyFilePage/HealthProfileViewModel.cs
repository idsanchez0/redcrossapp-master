﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using RedCrossApp.Models;
using RedCrossApp.Services;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace RedCrossApp.ViewModels.MyFilePage
{
    /// <summary>
    /// ViewModel for health profile page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class HealthProfileViewModel : BaseViewModel
    {
        #region Fields

        /// <summary>
        /// To store the health profile data collection.
        /// </summary>
        private ObservableCollection<HealthProfile> cardItems;
        private string profileImage;
        private string profileName;
        private string city;
        private string state;
        private string id;

        private INavigation Navigation;

        public ICommand DownloadFileCommand { get; private set; }
        public ICommand EditFileCommand { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="HealthProfileViewModel" /> class.
        /// </summary>
        public HealthProfileViewModel(INavigation _navigation)
        {
            Navigation = _navigation;

            DownloadFileCommand = new Command(async () => await DownloadFileMethod());
            EditFileCommand = new Command(async () => await EditFileMethod());

            LoadData();
        }

        public async void LoadData()
        {
            try
            {

                MyFile _myFile = await Auth.MyFile();

                this.ProfileImage = string.IsNullOrEmpty(_myFile.Image) ? App.BaseImageUrl + "ProfileImage16.png" : _myFile.Image;
                this.ProfileName = _myFile.names;
                this.State = _myFile.State;
                this.City = _myFile.City;
                this.Id = _myFile.Id.ToString();

                CardItems = new ObservableCollection<HealthProfile>()
                {
                    new HealthProfile()
                    {
                        Category = "Dignidad",
                        CategoryValue = _myFile.Dignity,
                        ImagePath = "\uf508"
                    },
                    new HealthProfile()
                    {
                        Category = "Horas Aportadas",
                        CategoryValue = _myFile.WorkedHours.ToString(),
                        ImagePath = "\uf4ce"
                    },
                    new HealthProfile()
                    {
                        Category = "Fecha Última Actividad",
                        CategoryValue = _myFile.DateLastActivity.ToString(),
                        ImagePath = "\uf073"
                    },
                    new HealthProfile()
                    {
                        Category = "Actividades Pendientes de Confirmar",
                        CategoryValue = _myFile.ToCompleteActivities.ToString(),
                        ImagePath = "\uf0ae"
                    }
                };

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in MyFile LoadData: " + ex.Message);
            }
        }

        private async Task DownloadFileMethod()
        {
            try
            {
                //var file = new Uri(ApiServices.Url + "api/volunteers/pdf/" + Id.ToString());

                // Open PDF URL with device browser to download  
                //await Launcher.OpenAsync(file);

                UserDialogs.Instance.ShowLoading("Generando...");

                Uri url = new Uri(ApiServices.Url + "api/auth/my-file-pdf");

                
                var fileBites = await Auth.MyFilePDF();

                string localFilename = $"Ficha {ProfileName}.pdf";
                string filePath = Path.Combine(FileSystem.CacheDirectory, localFilename);
                File.WriteAllBytes(filePath, fileBites);

                await Share.RequestAsync(new ShareFileRequest
                {
                    Title = "Mi Ficha",
                    File = new ShareFile(filePath, "application/pdf"),
                    
                });

                UserDialogs.Instance.HideLoading();

            }
            catch(Exception)
            {

            }
        }

        private async Task EditFileMethod()
        {

            await Navigation.PushAsync(new Views.EditProfile.BottomNavigationPage());
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the health profile items collection.
        /// </summary>
        public ObservableCollection<HealthProfile> CardItems
        {
            get { return this.cardItems; }
            set { SetValue(ref this.cardItems, value); }
        }

        /// <summary>
        /// Gets or sets the profile image.
        /// </summary>
        public string ProfileImage
        {
            get { return this.profileImage; }
            set { SetValue(ref this.profileImage, value); }
        }

        /// <summary>
        /// Gets or sets the profile name.
        /// </summary>
        public string ProfileName
        {
            get { return this.profileName; }
            set { SetValue(ref this.profileName, value); }
        }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        public string State
        {
            get { return this.state; }
            set { SetValue(ref this.state, value); }
        }

        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        public string City
        {
            get { return this.city; }
            set { SetValue(ref this.city, value); }
        }
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public string Id
        {
            get { return this.id; }
            set { SetValue(ref this.id, value); }
        }

        #endregion
    }
}