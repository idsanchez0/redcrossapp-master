﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.MyFilePage
{
    class VolunteerLanguageListViewModel : BaseViewModel
    {
        private ObservableCollection<VolunteerFormation> languages;

        public INavigation Navigation;
        public ObservableCollection<VolunteerFormation> Languages
        {
            get { return languages; }
            set { SetValue(ref this.languages, value); }
        }
        public Command EditCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public Command AddCommand { get; set; }
        public VolunteerLanguageListViewModel()
        {
            EditCommand = new Command<VolunteerFormation>(async (VolunteerFormation) => await EditMethod(VolunteerFormation));
            DeleteCommand = new Command<VolunteerFormation>(async (VolunteerFormation) => await DeleteMethod(VolunteerFormation));
            AddCommand = new Command(AddMethod);
        }

        public async void LoadData()
        {
            Languages = new ObservableCollection<VolunteerFormation>(await VolunteerFormationService.All("language=true"));
        }
        private void AddMethod(object obj)
        {
            if (Navigation != null)
                Navigation.PushAsync(new AddEditVolunteerLanguagePage());
        }

        private async Task DeleteMethod(VolunteerFormation volunteerFormation)
        {
            var Confirm = await App.Current.MainPage.DisplayAlert("Alerta","¿Desea borrar el registro?","ok","cancelar");
            if(Confirm)
            {
                var success = await VolunteerFormationService.Delete(volunteerFormation.id_voluntario_formacion);
                if(success)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Registro borrado con éxito", "ok");
                    LoadData();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Error al borrar el registro", "ok");
                }
            }
        }

        private async Task EditMethod(VolunteerFormation volunteerFormation)
        {
            if (Navigation != null && volunteerFormation != null)
                await Navigation.PushAsync(new AddEditVolunteerLanguagePage(volunteerFormation));
        }

    }
}
