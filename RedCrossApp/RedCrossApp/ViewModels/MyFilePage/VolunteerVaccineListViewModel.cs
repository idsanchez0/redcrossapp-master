﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.MyFilePage
{
    class VolunteerVaccineListViewModel : BaseViewModel
    {
        private ObservableCollection<VolunteerVaccine> vaccines;

        public INavigation Navigation;
        public ObservableCollection<VolunteerVaccine> Vaccines
        {
            get { return vaccines; }
            set { SetValue(ref this.vaccines, value); }
        }
        public Command EditCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public Command AddCommand { get; set; }
        public VolunteerVaccineListViewModel()
        {
            EditCommand = new Command<VolunteerVaccine>(async (volunteerVaccine) => await EditMethod(volunteerVaccine));
            DeleteCommand = new Command<VolunteerVaccine>(async (volunteerVaccine) => await DeleteMethod(volunteerVaccine));
            AddCommand = new Command(AddMethod);
        }

        public async void LoadData()
        {
            Vaccines = new ObservableCollection<VolunteerVaccine>(await VolunteerVaccineService.All());
        }
        private void AddMethod(object obj)
        {
            if (Navigation != null)
                Navigation.PushAsync(new AddEditVolunteerVaccinePage());
        }

        private async Task DeleteMethod(VolunteerVaccine volunteerVaccine)
        {
            var Confirm = await App.Current.MainPage.DisplayAlert("Alerta","¿Desea borrar el registro?","ok","cancelar");
            if(Confirm)
            {
                var success = await VolunteerVaccineService.Delete(volunteerVaccine.id_tipo_vacuna);
                if(success)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Registro borrado con éxito", "ok");
                    LoadData();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Error al borrar el registro", "ok");
                }
            }
        }

        private async Task EditMethod(VolunteerVaccine volunteerVaccine)
        {
            if (Navigation != null && volunteerVaccine != null)
                await Navigation.PushAsync(new AddEditVolunteerVaccinePage(volunteerVaccine.id_tipo_vacuna));
        }

    }
}
