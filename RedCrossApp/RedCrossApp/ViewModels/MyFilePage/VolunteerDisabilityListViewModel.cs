﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.MyFilePage
{
    class VolunteerDisabilityListViewModel : BaseViewModel
    {
        private ObservableCollection<VolunteerDisability> disabilities;

        public INavigation Navigation;
        public ObservableCollection<VolunteerDisability> Disabilities
        {
            get { return disabilities; }
            set { SetValue(ref this.disabilities, value); }
        }
        public Command EditCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public Command AddCommand { get; set; }
        public VolunteerDisabilityListViewModel()
        {
            EditCommand = new Command<VolunteerDisability>(async (VolunteerDisability) => await EditMethod(VolunteerDisability));
            DeleteCommand = new Command<VolunteerDisability>(async (VolunteerDisability) => await DeleteMethod(VolunteerDisability));
            AddCommand = new Command(AddMethod);
        }

        public async void LoadData()
        {
            Disabilities = new ObservableCollection<VolunteerDisability>(await VolunteerDisabilityService.All());
        }
        private void AddMethod(object obj)
        {
            if (Navigation != null)
                Navigation.PushAsync(new AddEditVolunteerDisabilityPage());
        }

        private async Task DeleteMethod(VolunteerDisability volunteerDisability)
        {
            var Confirm = await App.Current.MainPage.DisplayAlert("Alerta","¿Desea borrar el registro?","ok","cancelar");
            if(Confirm)
            {
                var success = await VolunteerDisabilityService.Delete(volunteerDisability.id_tipo_discapacidad);
                if(success)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Registro borrado con éxito", "ok");
                    LoadData();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Error al borrar el registro", "ok");
                }
            }
        }

        private async Task EditMethod(VolunteerDisability volunteerDisability)
        {
            if (Navigation != null && volunteerDisability != null)
                await Navigation.PushAsync(new AddEditVolunteerDisabilityPage(volunteerDisability));
        }

    }
}
