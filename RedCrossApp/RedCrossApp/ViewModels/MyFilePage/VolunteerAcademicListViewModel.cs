﻿using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels.MyFilePage
{
    class VolunteerAcademicListViewModel : BaseViewModel
    {
        private ObservableCollection<VolunteerFormation> academics;

        public INavigation Navigation;
        public ObservableCollection<VolunteerFormation> Academics
        {
            get { return academics; }
            set { SetValue(ref this.academics, value); }
        }
        public Command EditCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public Command AddCommand { get; set; }
        public VolunteerAcademicListViewModel()
        {
            EditCommand = new Command<VolunteerFormation>(async (VolunteerFormation) => await EditMethod(VolunteerFormation));
            DeleteCommand = new Command<VolunteerFormation>(async (VolunteerFormation) => await DeleteMethod(VolunteerFormation));
            AddCommand = new Command(AddMethod);
        }

        public async void LoadData()
        {
            Academics = new ObservableCollection<VolunteerFormation>(await VolunteerFormationService.All());
        }
        private void AddMethod(object obj)
        {
            if (Navigation != null)
                Navigation.PushAsync(new AddEditVolunteerFomationPage());
        }

        private async Task DeleteMethod(VolunteerFormation volunteerFormation)
        {
            var Confirm = await App.Current.MainPage.DisplayAlert("Alerta","¿Desea borrar el registro?","ok","cancelar");
            if(Confirm)
            {
                var success = await VolunteerFormationService.Delete(volunteerFormation.id_voluntario_formacion);
                if(success)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Registro borrado con éxito", "ok");
                    LoadData();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Error al borrar el registro", "ok");
                }
            }
        }

        private async Task EditMethod(VolunteerFormation volunteerFormation)
        {
            if (Navigation != null && volunteerFormation != null)
                await Navigation.PushAsync(new AddEditVolunteerFomationPage(volunteerFormation));
        }

    }
}
