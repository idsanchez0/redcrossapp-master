﻿using RedCrossApp.Models;
using RedCrossApp.Models.Postulations;
using RedCrossApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp.ViewModels
{
    class PostulationsDetailViewModel : BaseViewModel
    {
        private INavigation Navigation;
        private Activity activity;
        private string activityName;
        private string activityId;

        private ObservableCollection<PostulationModel> postulations;
        private bool hasPostulations = false;
        private bool noPostulations = false;

        public string ActivityName
        {
            get { return activityName; }
            set { SetValue(ref this.activityName, value); }
        }
        public string ActivityId
        {
            get { return activityId; }
            set { SetValue(ref this.activityId, value); }
        }
        public ObservableCollection<PostulationModel> Postulations
        {
            get { return postulations; }
            set { SetValue(ref this.postulations, value); }
        }
        public bool CanApprove
        {
            get { return hasPostulations; }
            set { SetValue(ref this.hasPostulations, value); }
        }
        public bool NoPostulations
        {
            get { return noPostulations; }
            set { SetValue(ref this.noPostulations, !value); }
        }

        public ICommand AcceptPostulationCommand { get; private set; }
        public ICommand DownloadCommand { get; private set; }
        public PostulationsDetailViewModel(Activity _activity, INavigation _navigation)
        {
            Navigation = _navigation;
            activity = _activity;

            AcceptPostulationCommand = new Command(async () => await AcceptPostulationMethod());
            DownloadCommand = new Command(async () => await DownloadMethod());
        }

        public async void LoadData()
        {

            ActivityName = activity.nombre;
            ActivityId = activity.id_actividad.ToString();

            var _Postulations = new ObservableCollection<PostulationModel>();

            var _postulations = await PostulationService.PostulatedCompletenes(activity.id_actividad);

            if (_postulations.Any())
            {
                NoPostulations = false;
                foreach (PostulatedCompleteness _postulation in _postulations)
                {
                    CanApprove = _postulation.id_tipo_estado_postulacion != 2;
                    _Postulations.Add(new PostulationModel 
                    { 
                        CheckedPostulation = _postulation.id_tipo_estado_postulacion != 3,
                        CanApprove = _postulation.id_tipo_estado_postulacion == 1,
                        Completition = _postulation.Completeness.ToString(),
                        LastActivityDate = _postulation.fecha_ultima_actividad,
                        Name = _postulation.nombre, 
                        voluntario_id = _postulation.id_voluntario
                    });
                }
            }
            else
            {
                NoPostulations = true;
            }

            Postulations = _Postulations;

        }

        private async Task<bool> DownloadMethod()
        {
            return await Launcher.TryOpenAsync(ApiServices.Url + "api/postulation/postulated-completeness-xlsx/" + activity.id_actividad.ToString());
        }

        private async Task AcceptPostulationMethod()
        {
            var resp = await App.Current.MainPage.DisplayAlert("alerta", "¿Está seguro de los postulantes seleccionados?", "Ok","Cancelar");

            if (resp)
            {
                var Volunteer_Ids = new List<int>();

                foreach(PostulationModel _postulation in Postulations)
                {
                    if(_postulation.CheckedPostulation)
                    {
                        Volunteer_Ids.Add(_postulation.voluntario_id);
                    }
                }

                if(Volunteer_Ids.Count() == 0) 
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Debe aprobar al menos a un voluntario", "Ok");
                    return;
                }

                var _response = await PostulationService.ApprovePostulations(new ApprovePostulationsRequest 
                { 
                    id_actividad = activity.id_actividad,
                    approved = Volunteer_Ids
                });

                if (_response)
                {
                    await App.Current.MainPage.DisplayAlert("Alerta", "Se registraron los voluntarios", "Ok");
                    await Navigation.PopAsync();
                }
                else
                    await App.Current.MainPage.DisplayAlert("Error", "No se pudieron registrar los voluntarios", "Ok");
            }
        }
    }
}
