﻿using Acr.UserDialogs;
using RedCrossApp.Models;
using RedCrossApp.Services;
using RedCrossApp.Views;
using RedCrossApp.Views.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RedCrossApp
{
    class Utils
    {
        public static int DaysUntilForm = 15;
        public static bool IsValidEmail(string Email)
        {
            try
            {
                MailAddress m = new MailAddress(Email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static async Task AdminLoginCompleted(string Token, bool redirect = false)
        {
            try
            {
                App.Current.Properties["Token"] = Token;
                App.Current.Properties["IsAdmin"] = true;

                var meResponse = await Auth.AdminMe();

                if (!meResponse.IsSuccess)
                {
                    if (redirect)
                        App.Current.MainPage = new NavigationPage(new LoginView());
                    else
                        await App.Current.MainPage.DisplayAlert("Error", "Dispositivo desconectado", "Aceptar");
                }

                var Owner = (SegUser)meResponse.Result;

                App.Current.Properties["IsLoggedIn"] = Owner?.active == "Y";
                App.Current.Properties["Name"] = Owner.name;

                string idCities = "";
                string idStates = "";

                if(Owner.estados.Any())
                {

                    List<string> tmpIdCities = new List<string>();
                    List<string> tmpIdStates = new List<string>();

                    foreach (SegEstado segEstado in Owner.estados)
                    {

                        tmpIdCities.Add(string.Join(",",segEstado.ids_ciudad));
                        tmpIdStates.Add(segEstado.id_estado.ToString());
                    }

                    idCities = string.Join(",",tmpIdCities);
                    idStates = string.Join(",", tmpIdStates);
                }

                App.Current.Properties["IdCities"] = idCities;
                App.Current.Properties["idStates"] = idStates;

                await App.Current.SavePropertiesAsync();

                App.Current.MainPage = new MenuPage();
            }
            catch (Exception)
            {

            }

        }

        public static async Task VolunteerLoginCompleted(string Token, bool redirect = false)
        {
            try
            {
                App.Current.Properties["Token"] = Token;
                App.Current.Properties["IsAdmin"] = false;

                var meResponse = await Auth.AdminMe();

                if (!meResponse.IsSuccess)
                {
                    if (redirect)
                        App.Current.MainPage = new NavigationPage(new LoginView());
                    else
                        await App.Current.MainPage.DisplayAlert("Error", "Dispositivo desconectado", "Aceptar");
                }

                var Owner = (Volunteer)meResponse.Result;

                App.Current.Properties["IsLoggedIn"] = Owner?.id_estado_voluntario == 3;
                App.Current.Properties["Name"] = Owner.nombres + " " + Owner.apellidos;
                App.Current.Properties["IdCity"] = Owner.id_ciudad;
                App.Current.Properties["IdState"] = Owner.id_estado;
                App.Current.Properties["FormDate"] = DateTime.Now.AddDays(DaysUntilForm);

                await App.Current.SavePropertiesAsync();

                App.Current.MainPage = new MenuPage();
            }
            catch (Exception)
            {

            }

        }

        public static async void LoadParameters()
        {
            var Parameters = await ParameterService.All();

            float _float;

            if (Parameters != null && Parameters.Any())
            {
                foreach(Parameter _parameter in Parameters)
                {
                    switch(_parameter.nombre)
                    {
                        case "vol_link_encuesta":
                            App.Current.Properties["formLink"] = _parameter.valor;
                            break;
                        case "vol_link_biblioteca":
                            App.Current.Properties["libraryLink"] = _parameter.valor;
                            break;
                        case "vol_duracion_actividad_dia":
                            float.TryParse(_parameter.valor, out _float);
                            App.Current.Properties["DayActivityDuration"] = _float;
                            break;
                        case "vol_dias_duracion_activacion_seguro":
                            float.TryParse(_parameter.valor, out _float);
                            App.Current.Properties["SecureActivityDurationDays"] = _float;
                            break;
                    }
                }

                await App.Current.SavePropertiesAsync();
            }
        }

        public static async Task Logout()
        {
            try
            {
                await (Application.Current as App).SignOut();

                string token = (App.Current.Properties.ContainsKey("Token")) ? App.Current.Properties["Token"].ToString() : "";

                await Auth.Logout(token.Split('|')[0]);

                App.Current.Properties.Clear();

                await App.Current.SavePropertiesAsync();

            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                App.Current.MainPage = new NavigationPage(new LoginView());
            });
        }

        public static async void checkSession()
        {
            try
            {
                bool isAdmin = App.Current.Properties.ContainsKey("IsAdmin") ? (bool)(App.Current.Properties["IsAdmin"]) : false;

                var meResponse = await Auth.AdminMe();

                bool isActive = true;

                if (!meResponse.IsSuccess)
                    return;

                if (isAdmin)
                {
                    var Owner = (SegUser)meResponse.Result;
                    isActive = Owner?.active == "Y";
                }
                else
                {
                    var Owner = (Volunteer)meResponse.Result;
                    isActive = Owner?.id_estado_voluntario == 3;
                }

                if (!isActive)
                {
                    UserDialogs.Instance.Alert("Usuario desactivado");
                    await Utils.Logout();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }


        }

        public static async void ShowForm()
        {
            try
            {

                if (App.Current.Properties.ContainsKey("FormDate"))
                {
                    DateTime.TryParse(App.Current.Properties["FormDate"].ToString(), out DateTime FormDate);
                    //UserDialogs.Instance.Toast($"Verificando fecha {FormDate.ToShortDateString()}");
                    
                    if (FormDate < DateTime.Now)
                    {
                        var response = await UserDialogs.Instance.ConfirmAsync("¿Desea llenar la encuesta de satisfacción de la aplicación?");
                        if (response)
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                App.Current.MainPage = new NavigationPage(new FormPage());
                            });
                        else
                            App.Current.Properties["FormDate"] = DateTime.Now
                                .AddDays(DaysUntilForm)
                                .ToString("yyyy-MM-dd") + "T00:00:00+0:00";
                    }
                }
                else
                    App.Current.Properties["FormDate"] = DateTime.Now
                        .AddDays(DaysUntilForm)
                        .ToString("yyyy-MM-dd") + "T00:00:00+0:00";

                await App.Current.SavePropertiesAsync();
            }
            catch(Exception)
            {

            }
        }

        public static string Platform()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    return "iOS";
                case Device.Android:
                    return "Android";
                default:
                    return "Other";
            }
        }
    }
}
