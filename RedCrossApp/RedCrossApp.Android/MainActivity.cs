﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Acr.UserDialogs;
using Microsoft.Identity.Client;
using Android.Content;
using RedCrossApp.Services;
using Xamarin.Essentials;
using Android.Gms.Common;
using Newtonsoft.Json;

namespace RedCrossApp.Droid
{
    [Activity(Label = "ACTIVADOS", Icon = "@drawable/Icon", LaunchMode = LaunchMode.SingleTop, Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = ACTIVADOS.Droid.Resource.Layout.Tabbar;
            ToolbarResource = ACTIVADOS.Droid.Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            //Xamarin.FormsMaps.Init(this,savedInstanceState);
            bool googlePlayService = IsPlayServicesAvailable();
            UserDialogs.Init(this);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            if (Intent.Extras != null)
                NotificationClickedOn(Intent, true);
            else
                LoadApplication(new App());
 
            App.AuthUIParent = this;
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        // <OnActivityResultSnippet>
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationContinuationHelper
                .SetAuthenticationContinuationEventArgs(requestCode, resultCode, data);
        }
        // </OnActivityResultSnippet>

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            bool isGooglePlayServce = resultCode != ConnectionResult.Success;
            Preferences.Set("isGooglePlayServce", isGooglePlayServce);
            return isGooglePlayServce;
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            NotificationClickedOn(intent);
        }

        void NotificationClickedOn(Intent intent, bool LoadApp = false)
        {
            try
            {
                if (intent.Action == "Activity" && intent.HasExtra("Activity"))
                {

                    var Activity = JsonConvert.DeserializeObject<Models.Activity>(intent.GetStringExtra("Activity"));
                    if (LoadApp)
                        LoadApplication(new App(new Views.ActivityDetailPage(Activity)));
                    else
                        App.Current.MainPage = new MenuPage(new Views.ActivityDetailPage(Activity));

                }

                if (intent.Action == "News" && intent.HasExtra("News"))
                {

                    Preferences.Set("CurrentNews",intent.GetStringExtra("News"));
                    if (LoadApp)
                        LoadApplication(new App());
                    else
                        App.Current.MainPage = new MenuPage();

                }
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

    }
}