package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "ACTIVADOS.Droid.dll" comes first in this list. */
		"Xamarin.AndroidX.AppCompat.AppCompatResources.dll",
		"Xamarin.AndroidX.AppCompat.dll",
		"Xamarin.AndroidX.Browser.dll",
		"Xamarin.AndroidX.CardView.dll",
		"Xamarin.AndroidX.CoordinatorLayout.dll",
		"Xamarin.AndroidX.Core.dll",
		"Xamarin.AndroidX.CustomView.dll",
		"Xamarin.AndroidX.DrawerLayout.dll",
		"Xamarin.AndroidX.Fragment.dll",
		"Xamarin.AndroidX.Legacy.Support.Core.UI.dll",
		"Xamarin.AndroidX.Lifecycle.Common.dll",
		"Xamarin.AndroidX.Lifecycle.LiveData.Core.dll",
		"Xamarin.AndroidX.Lifecycle.ViewModel.dll",
		"Xamarin.AndroidX.Loader.dll",
		"Xamarin.AndroidX.RecyclerView.dll",
		"Xamarin.AndroidX.SwipeRefreshLayout.dll",
		"Xamarin.AndroidX.VersionedParcelable.dll",
		"Xamarin.AndroidX.ViewPager.dll",
		"Xamarin.Google.Android.Material.dll",
		"ACTIVADOS.Droid.dll",
		"Acr.UserDialogs.dll",
		"AndHUD.dll",
		"Azure.Core.dll",
		"CarouselView.FormsPlugin.Abstractions.dll",
		"CarouselView.FormsPlugin.Droid.dll",
		"Com.Android.DeskClock.dll",
		"Com.ViewPagerIndicator.dll",
		"FormsViewGroup.dll",
		"Microsoft.Bcl.AsyncInterfaces.dll",
		"Microsoft.Graph.Core.dll",
		"Microsoft.Graph.dll",
		"Microsoft.Identity.Client.dll",
		"Microsoft.IdentityModel.Clients.ActiveDirectory.dll",
		"Microsoft.IdentityModel.JsonWebTokens.dll",
		"Microsoft.IdentityModel.Logging.dll",
		"Microsoft.IdentityModel.Protocols.dll",
		"Microsoft.IdentityModel.Protocols.OpenIdConnect.dll",
		"Microsoft.IdentityModel.Tokens.dll",
		"Newtonsoft.Json.dll",
		"RedCrossApp.dll",
		"Syncfusion.Buttons.XForms.Android.dll",
		"Syncfusion.Buttons.XForms.dll",
		"Syncfusion.Cards.XForms.Android.dll",
		"Syncfusion.Cards.XForms.dll",
		"Syncfusion.Core.XForms.Android.dll",
		"Syncfusion.Core.XForms.dll",
		"Syncfusion.DataSource.Portable.dll",
		"Syncfusion.GridCommon.Portable.dll",
		"Syncfusion.Licensing.dll",
		"Syncfusion.SfAutoComplete.XForms.Android.dll",
		"Syncfusion.SfAutoComplete.XForms.dll",
		"Syncfusion.SfComboBox.XForms.Android.dll",
		"Syncfusion.SfComboBox.XForms.dll",
		"Syncfusion.SfListView.XForms.Android.dll",
		"Syncfusion.SfListView.XForms.dll",
		"Syncfusion.SfPicker.Android.dll",
		"Syncfusion.SfPicker.XForms.Android.dll",
		"Syncfusion.SfPicker.XForms.dll",
		"Syncfusion.SfRating.Android.dll",
		"Syncfusion.SfRating.XForms.Android.dll",
		"Syncfusion.SfRating.XForms.dll",
		"Syncfusion.SfRotator.Android.dll",
		"Syncfusion.SfRotator.XForms.Android.dll",
		"Syncfusion.SfRotator.XForms.dll",
		"Syncfusion.SfTabView.XForms.Android.dll",
		"Syncfusion.SfTabView.XForms.dll",
		"System.Diagnostics.DiagnosticSource.dll",
		"System.IdentityModel.Tokens.Jwt.dll",
		"System.Memory.Data.dll",
		"System.Text.Encodings.Web.dll",
		"System.Text.Json.dll",
		"Xamarin.Android.Volley.dll",
		"Xamarin.AndroidX.Activity.dll",
		"Xamarin.AndroidX.SavedState.dll",
		"Xamarin.Azure.NotificationHubs.Android.dll",
		"Xamarin.Essentials.dll",
		"Xamarin.Firebase.Messaging.dll",
		"Xamarin.Forms.Core.dll",
		"Xamarin.Forms.Maps.Android.dll",
		"Xamarin.Forms.Maps.dll",
		"Xamarin.Forms.Platform.Android.dll",
		"Xamarin.Forms.Platform.dll",
		"Xamarin.Forms.Xaml.dll",
		"Xamarin.Google.Android.DataTransport.TransportApi.dll",
		"Xamarin.Google.Android.DataTransport.TransportBackendCct.dll",
		"Xamarin.Google.Android.DataTransport.TransportRuntime.dll",
		"Xamarin.Google.Dagger.dll",
		"Xamarin.Google.Guava.ListenableFuture.dll",
		"Xamarin.GooglePlayServices.Base.dll",
		"Xamarin.GooglePlayServices.Basement.dll",
		"Xamarin.GooglePlayServices.Maps.dll",
		"Xamarin.GooglePlayServices.Tasks.dll",
		"Xamarin.JavaX.Inject.dll",
		"Xamarin.Plugin.Calendar.dll",
	};
	public static String[] Dependencies = new String[]{
	};
}
