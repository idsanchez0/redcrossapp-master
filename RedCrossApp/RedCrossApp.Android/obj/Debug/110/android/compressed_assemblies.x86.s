	.file	"compressed_assemblies.x86.x86.s"
	.include	"compressed_assemblies.x86-data.inc"

	.section	.data.compressed_assembly_descriptors,"aw",@progbits
	.type	.L.compressed_assembly_descriptors, @object
	.p2align	2
.L.compressed_assembly_descriptors:
	/* 0: ACTIVADOS.Droid.dll */
	/* uncompressed_file_size */
	.long	528896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_0

	/* 1: Acr.UserDialogs.dll */
	/* uncompressed_file_size */
	.long	200192
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_1

	/* 2: AndHUD.dll */
	/* uncompressed_file_size */
	.long	58368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_2

	/* 3: Azure.Core.dll */
	/* uncompressed_file_size */
	.long	205176
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_3

	/* 4: FormsViewGroup.dll */
	/* uncompressed_file_size */
	.long	27536
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_4

	/* 5: Java.Interop.dll */
	/* uncompressed_file_size */
	.long	208760
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_5

	/* 6: Microsoft.Bcl.AsyncInterfaces.dll */
	/* uncompressed_file_size */
	.long	14720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_6

	/* 7: Microsoft.CSharp.dll */
	/* uncompressed_file_size */
	.long	310648
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_7

	/* 8: Microsoft.Graph.Core.dll */
	/* uncompressed_file_size */
	.long	172408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_8

	/* 9: Microsoft.Graph.dll */
	/* uncompressed_file_size */
	.long	8640400
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_9

	/* 10: Microsoft.Identity.Client.dll */
	/* uncompressed_file_size */
	.long	1457080
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_10

	/* 11: Microsoft.IdentityModel.Clients.ActiveDirectory.dll */
	/* uncompressed_file_size */
	.long	1025024
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_11

	/* 12: Microsoft.IdentityModel.JsonWebTokens.dll */
	/* uncompressed_file_size */
	.long	62848
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_12

	/* 13: Microsoft.IdentityModel.Logging.dll */
	/* uncompressed_file_size */
	.long	29560
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_13

	/* 14: Microsoft.IdentityModel.Protocols.OpenIdConnect.dll */
	/* uncompressed_file_size */
	.long	109432
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_14

	/* 15: Microsoft.IdentityModel.Protocols.dll */
	/* uncompressed_file_size */
	.long	33144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_15

	/* 16: Microsoft.IdentityModel.Tokens.dll */
	/* uncompressed_file_size */
	.long	900992
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_16

	/* 17: Mono.Android.dll */
	/* uncompressed_file_size */
	.long	30011272
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_17

	/* 18: Mono.Security.dll */
	/* uncompressed_file_size */
	.long	250744
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_18

	/* 19: Newtonsoft.Json.dll */
	/* uncompressed_file_size */
	.long	695336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_19

	/* 20: RedCrossApp.dll */
	/* uncompressed_file_size */
	.long	1683456
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_20

	/* 21: Syncfusion.Buttons.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	104448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_21

	/* 22: Syncfusion.Buttons.XForms.dll */
	/* uncompressed_file_size */
	.long	160256
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_22

	/* 23: Syncfusion.Cards.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	14336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_23

	/* 24: Syncfusion.Cards.XForms.dll */
	/* uncompressed_file_size */
	.long	30720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_24

	/* 25: Syncfusion.Core.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	236032
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_25

	/* 26: Syncfusion.Core.XForms.dll */
	/* uncompressed_file_size */
	.long	409088
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_26

	/* 27: Syncfusion.DataSource.Portable.dll */
	/* uncompressed_file_size */
	.long	137728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_27

	/* 28: Syncfusion.GridCommon.Portable.dll */
	/* uncompressed_file_size */
	.long	130048
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_28

	/* 29: Syncfusion.Licensing.dll */
	/* uncompressed_file_size */
	.long	48128
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_29

	/* 30: Syncfusion.SfAutoComplete.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	303616
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_30

	/* 31: Syncfusion.SfAutoComplete.XForms.dll */
	/* uncompressed_file_size */
	.long	64000
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_31

	/* 32: Syncfusion.SfComboBox.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	318464
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_32

	/* 33: Syncfusion.SfComboBox.XForms.dll */
	/* uncompressed_file_size */
	.long	72192
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_33

	/* 34: Syncfusion.SfListView.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	30720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_34

	/* 35: Syncfusion.SfListView.XForms.dll */
	/* uncompressed_file_size */
	.long	149504
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_35

	/* 36: Syncfusion.SfPicker.Android.dll */
	/* uncompressed_file_size */
	.long	69632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_36

	/* 37: Syncfusion.SfPicker.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	31232
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_37

	/* 38: Syncfusion.SfPicker.XForms.dll */
	/* uncompressed_file_size */
	.long	68608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_38

	/* 39: Syncfusion.SfRating.Android.dll */
	/* uncompressed_file_size */
	.long	34304
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_39

	/* 40: Syncfusion.SfRating.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	16896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_40

	/* 41: Syncfusion.SfRating.XForms.dll */
	/* uncompressed_file_size */
	.long	20480
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_41

	/* 42: Syncfusion.SfRotator.Android.dll */
	/* uncompressed_file_size */
	.long	57856
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_42

	/* 43: Syncfusion.SfRotator.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	23552
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_43

	/* 44: Syncfusion.SfRotator.XForms.dll */
	/* uncompressed_file_size */
	.long	17408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_44

	/* 45: Syncfusion.SfTabView.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	106496
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_45

	/* 46: Syncfusion.SfTabView.XForms.dll */
	/* uncompressed_file_size */
	.long	34816
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_46

	/* 47: System.Buffers.dll */
	/* uncompressed_file_size */
	.long	13688
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_47

	/* 48: System.Collections.Concurrent.dll */
	/* uncompressed_file_size */
	.long	14200
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_48

	/* 49: System.Collections.dll */
	/* uncompressed_file_size */
	.long	14216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_49

	/* 50: System.ComponentModel.Composition.dll */
	/* uncompressed_file_size */
	.long	267640
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_50

	/* 51: System.ComponentModel.dll */
	/* uncompressed_file_size */
	.long	13704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_51

	/* 52: System.Core.dll */
	/* uncompressed_file_size */
	.long	1094008
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_52

	/* 53: System.Data.DataSetExtensions.dll */
	/* uncompressed_file_size */
	.long	38776
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_53

	/* 54: System.Data.dll */
	/* uncompressed_file_size */
	.long	1935224
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_54

	/* 55: System.Diagnostics.Debug.dll */
	/* uncompressed_file_size */
	.long	13704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_55

	/* 56: System.Diagnostics.DiagnosticSource.dll */
	/* uncompressed_file_size */
	.long	51584
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_56

	/* 57: System.Diagnostics.Tracing.dll */
	/* uncompressed_file_size */
	.long	14224
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_57

	/* 58: System.Drawing.Common.dll */
	/* uncompressed_file_size */
	.long	170352
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_58

	/* 59: System.Dynamic.Runtime.dll */
	/* uncompressed_file_size */
	.long	14720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_59

	/* 60: System.Globalization.dll */
	/* uncompressed_file_size */
	.long	14200
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_60

	/* 61: System.IO.Compression.FileSystem.dll */
	/* uncompressed_file_size */
	.long	27528
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_61

	/* 62: System.IO.Compression.dll */
	/* uncompressed_file_size */
	.long	124792
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_62

	/* 63: System.IdentityModel.Tokens.Jwt.dll */
	/* uncompressed_file_size */
	.long	81784
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_63

	/* 64: System.IdentityModel.dll */
	/* uncompressed_file_size */
	.long	23416
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_64

	/* 65: System.Linq.Expressions.dll */
	/* uncompressed_file_size */
	.long	15752
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_65

	/* 66: System.Linq.Queryable.dll */
	/* uncompressed_file_size */
	.long	13688
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_66

	/* 67: System.Linq.dll */
	/* uncompressed_file_size */
	.long	13176
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_67

	/* 68: System.Memory.Data.dll */
	/* uncompressed_file_size */
	.long	21368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_68

	/* 69: System.Memory.dll */
	/* uncompressed_file_size */
	.long	13688
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_69

	/* 70: System.Net.Http.dll */
	/* uncompressed_file_size */
	.long	291704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_70

	/* 71: System.Numerics.Vectors.dll */
	/* uncompressed_file_size */
	.long	21368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_71

	/* 72: System.Numerics.dll */
	/* uncompressed_file_size */
	.long	128888
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_72

	/* 73: System.ObjectModel.dll */
	/* uncompressed_file_size */
	.long	14216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_73

	/* 74: System.Reflection.Extensions.dll */
	/* uncompressed_file_size */
	.long	13704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_74

	/* 75: System.Reflection.dll */
	/* uncompressed_file_size */
	.long	14712
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_75

	/* 76: System.Resources.ResourceManager.dll */
	/* uncompressed_file_size */
	.long	13688
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_76

	/* 77: System.Runtime.CompilerServices.Unsafe.dll */
	/* uncompressed_file_size */
	.long	16776
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_77

	/* 78: System.Runtime.Extensions.dll */
	/* uncompressed_file_size */
	.long	14208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_78

	/* 79: System.Runtime.InteropServices.dll */
	/* uncompressed_file_size */
	.long	16776
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_79

	/* 80: System.Runtime.Serialization.dll */
	/* uncompressed_file_size */
	.long	875896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_80

	/* 81: System.Runtime.dll */
	/* uncompressed_file_size */
	.long	21384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_81

	/* 82: System.Security.Cryptography.Cng.dll */
	/* uncompressed_file_size */
	.long	14216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_82

	/* 83: System.ServiceModel.Internals.dll */
	/* uncompressed_file_size */
	.long	227704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_83

	/* 84: System.Text.Encoding.dll */
	/* uncompressed_file_size */
	.long	14208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_84

	/* 85: System.Text.Encodings.Web.dll */
	/* uncompressed_file_size */
	.long	66936
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_85

	/* 86: System.Text.Json.dll */
	/* uncompressed_file_size */
	.long	351632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_86

	/* 87: System.Text.RegularExpressions.dll */
	/* uncompressed_file_size */
	.long	14216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_87

	/* 88: System.Threading.Tasks.Extensions.dll */
	/* uncompressed_file_size */
	.long	14208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_88

	/* 89: System.Threading.dll */
	/* uncompressed_file_size */
	.long	14728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_89

	/* 90: System.Transactions.dll */
	/* uncompressed_file_size */
	.long	41336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_90

	/* 91: System.Web.Services.dll */
	/* uncompressed_file_size */
	.long	223608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_91

	/* 92: System.Xml.Linq.dll */
	/* uncompressed_file_size */
	.long	146808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_92

	/* 93: System.Xml.dll */
	/* uncompressed_file_size */
	.long	2451832
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_93

	/* 94: System.dll */
	/* uncompressed_file_size */
	.long	1969040
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_94

	/* 95: Xamarin.Android.Volley.dll */
	/* uncompressed_file_size */
	.long	278016
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_95

	/* 96: Xamarin.AndroidX.Activity.dll */
	/* uncompressed_file_size */
	.long	105360
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_96

	/* 97: Xamarin.AndroidX.Annotation.Experimental.dll */
	/* uncompressed_file_size */
	.long	25488
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_97

	/* 98: Xamarin.AndroidX.Annotation.dll */
	/* uncompressed_file_size */
	.long	158088
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_98

	/* 99: Xamarin.AndroidX.AppCompat.AppCompatResources.dll */
	/* uncompressed_file_size */
	.long	65936
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_99

	/* 100: Xamarin.AndroidX.AppCompat.dll */
	/* uncompressed_file_size */
	.long	1028608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_100

	/* 101: Xamarin.AndroidX.Arch.Core.Common.dll */
	/* uncompressed_file_size */
	.long	30608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_101

	/* 102: Xamarin.AndroidX.Arch.Core.Runtime.dll */
	/* uncompressed_file_size */
	.long	24448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_102

	/* 103: Xamarin.AndroidX.AsyncLayoutInflater.dll */
	/* uncompressed_file_size */
	.long	22416
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_103

	/* 104: Xamarin.AndroidX.Browser.dll */
	/* uncompressed_file_size */
	.long	284536
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_104

	/* 105: Xamarin.AndroidX.CardView.dll */
	/* uncompressed_file_size */
	.long	30088
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_105

	/* 106: Xamarin.AndroidX.Collection.dll */
	/* uncompressed_file_size */
	.long	74104
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_106

	/* 107: Xamarin.AndroidX.CoordinatorLayout.dll */
	/* uncompressed_file_size */
	.long	101776
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_107

	/* 108: Xamarin.AndroidX.Core.dll */
	/* uncompressed_file_size */
	.long	1404792
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_108

	/* 109: Xamarin.AndroidX.CursorAdapter.dll */
	/* uncompressed_file_size */
	.long	56720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_109

	/* 110: Xamarin.AndroidX.CustomView.dll */
	/* uncompressed_file_size */
	.long	63376
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_110

	/* 111: Xamarin.AndroidX.DocumentFile.dll */
	/* uncompressed_file_size */
	.long	28048
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_111

	/* 112: Xamarin.AndroidX.DrawerLayout.dll */
	/* uncompressed_file_size */
	.long	59280
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_112

	/* 113: Xamarin.AndroidX.Fragment.dll */
	/* uncompressed_file_size */
	.long	279440
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_113

	/* 114: Xamarin.AndroidX.Interpolator.dll */
	/* uncompressed_file_size */
	.long	21904
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_114

	/* 115: Xamarin.AndroidX.Legacy.Support.Core.UI.dll */
	/* uncompressed_file_size */
	.long	34192
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_115

	/* 116: Xamarin.AndroidX.Legacy.Support.Core.Utils.dll */
	/* uncompressed_file_size */
	.long	18832
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_116

	/* 117: Xamarin.AndroidX.Legacy.Support.V4.dll */
	/* uncompressed_file_size */
	.long	14200
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_117

	/* 118: Xamarin.AndroidX.Lifecycle.Common.dll */
	/* uncompressed_file_size */
	.long	42384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_118

	/* 119: Xamarin.AndroidX.Lifecycle.LiveData.Core.dll */
	/* uncompressed_file_size */
	.long	30096
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_119

	/* 120: Xamarin.AndroidX.Lifecycle.LiveData.dll */
	/* uncompressed_file_size */
	.long	24456
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_120

	/* 121: Xamarin.AndroidX.Lifecycle.Runtime.dll */
	/* uncompressed_file_size */
	.long	25488
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_121

	/* 122: Xamarin.AndroidX.Lifecycle.ViewModel.dll */
	/* uncompressed_file_size */
	.long	34192
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_122

	/* 123: Xamarin.AndroidX.Lifecycle.ViewModelSavedState.dll */
	/* uncompressed_file_size */
	.long	20880
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_123

	/* 124: Xamarin.AndroidX.Loader.dll */
	/* uncompressed_file_size */
	.long	61840
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_124

	/* 125: Xamarin.AndroidX.LocalBroadcastManager.dll */
	/* uncompressed_file_size */
	.long	18808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_125

	/* 126: Xamarin.AndroidX.Media.dll */
	/* uncompressed_file_size */
	.long	382328
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_126

	/* 127: Xamarin.AndroidX.MultiDex.dll */
	/* uncompressed_file_size */
	.long	14216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_127

	/* 128: Xamarin.AndroidX.Navigation.Common.dll */
	/* uncompressed_file_size */
	.long	89480
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_128

	/* 129: Xamarin.AndroidX.Navigation.Runtime.dll */
	/* uncompressed_file_size */
	.long	76680
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_129

	/* 130: Xamarin.AndroidX.Navigation.UI.dll */
	/* uncompressed_file_size */
	.long	35720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_130

	/* 131: Xamarin.AndroidX.Preference.dll */
	/* uncompressed_file_size */
	.long	263168
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_131

	/* 132: Xamarin.AndroidX.Print.dll */
	/* uncompressed_file_size */
	.long	22416
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_132

	/* 133: Xamarin.AndroidX.RecyclerView.dll */
	/* uncompressed_file_size */
	.long	563600
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_133

	/* 134: Xamarin.AndroidX.SavedState.dll */
	/* uncompressed_file_size */
	.long	29064
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_134

	/* 135: Xamarin.AndroidX.SlidingPaneLayout.dll */
	/* uncompressed_file_size */
	.long	43408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_135

	/* 136: Xamarin.AndroidX.SwipeRefreshLayout.dll */
	/* uncompressed_file_size */
	.long	66424
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_136

	/* 137: Xamarin.AndroidX.Transition.dll */
	/* uncompressed_file_size */
	.long	139776
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_137

	/* 138: Xamarin.AndroidX.VectorDrawable.Animated.dll */
	/* uncompressed_file_size */
	.long	44408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_138

	/* 139: Xamarin.AndroidX.VectorDrawable.dll */
	/* uncompressed_file_size */
	.long	31120
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_139

	/* 140: Xamarin.AndroidX.VersionedParcelable.dll */
	/* uncompressed_file_size */
	.long	106896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_140

	/* 141: Xamarin.AndroidX.ViewPager.dll */
	/* uncompressed_file_size */
	.long	83344
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_141

	/* 142: Xamarin.AndroidX.ViewPager2.dll */
	/* uncompressed_file_size */
	.long	53128
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_142

	/* 143: Xamarin.Azure.NotificationHubs.Android.dll */
	/* uncompressed_file_size */
	.long	291712
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_143

	/* 144: Xamarin.Essentials.dll */
	/* uncompressed_file_size */
	.long	210832
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_144

	/* 145: Xamarin.Firebase.Annotations.dll */
	/* uncompressed_file_size */
	.long	20872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_145

	/* 146: Xamarin.Firebase.Common.dll */
	/* uncompressed_file_size */
	.long	137080
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_146

	/* 147: Xamarin.Firebase.Components.dll */
	/* uncompressed_file_size */
	.long	107896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_147

	/* 148: Xamarin.Firebase.Datatransport.dll */
	/* uncompressed_file_size */
	.long	28024
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_148

	/* 149: Xamarin.Firebase.Iid.Interop.dll */
	/* uncompressed_file_size */
	.long	41352
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_149

	/* 150: Xamarin.Firebase.Installations.InterOp.dll */
	/* uncompressed_file_size */
	.long	45944
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_150

	/* 151: Xamarin.Firebase.Installations.dll */
	/* uncompressed_file_size */
	.long	142704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_151

	/* 152: Xamarin.Firebase.Measurement.Connector.dll */
	/* uncompressed_file_size */
	.long	60792
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_152

	/* 153: Xamarin.Firebase.Messaging.dll */
	/* uncompressed_file_size */
	.long	337272
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_153

	/* 154: Xamarin.Forms.Core.dll */
	/* uncompressed_file_size */
	.long	1225104
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_154

	/* 155: Xamarin.Forms.Platform.Android.dll */
	/* uncompressed_file_size */
	.long	869888
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_155

	/* 156: Xamarin.Forms.Platform.dll */
	/* uncompressed_file_size */
	.long	191368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_156

	/* 157: Xamarin.Forms.Xaml.dll */
	/* uncompressed_file_size */
	.long	113552
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_157

	/* 158: Xamarin.Google.Android.DataTransport.TransportApi.dll */
	/* uncompressed_file_size */
	.long	45968
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_158

	/* 159: Xamarin.Google.Android.DataTransport.TransportBackendCct.dll */
	/* uncompressed_file_size */
	.long	138120
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_159

	/* 160: Xamarin.Google.Android.DataTransport.TransportRuntime.dll */
	/* uncompressed_file_size */
	.long	363912
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_160

	/* 161: Xamarin.Google.Android.Material.dll */
	/* uncompressed_file_size */
	.long	1520640
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_161

	/* 162: Xamarin.Google.Dagger.dll */
	/* uncompressed_file_size */
	.long	138104
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_162

	/* 163: Xamarin.Google.Guava.ListenableFuture.dll */
	/* uncompressed_file_size */
	.long	18072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_163

	/* 164: Xamarin.GooglePlayServices.Base.dll */
	/* uncompressed_file_size */
	.long	1164176
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_164

	/* 165: Xamarin.GooglePlayServices.Basement.dll */
	/* uncompressed_file_size */
	.long	782736
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_165

	/* 166: Xamarin.GooglePlayServices.CloudMessaging.dll */
	/* uncompressed_file_size */
	.long	86408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_166

	/* 167: Xamarin.GooglePlayServices.Stats.dll */
	/* uncompressed_file_size */
	.long	52112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_167

	/* 168: Xamarin.GooglePlayServices.Tasks.dll */
	/* uncompressed_file_size */
	.long	121720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_168

	/* 169: Xamarin.JavaX.Inject.dll */
	/* uncompressed_file_size */
	.long	26752
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_169

	/* 170: Xamarin.Plugin.Calendar.dll */
	/* uncompressed_file_size */
	.long	229376
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_170

	/* 171: mscorlib.dll */
	/* uncompressed_file_size */
	.long	4513656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_171

	/* 172: netstandard.dll */
	/* uncompressed_file_size */
	.long	99720
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_172

	.size	.L.compressed_assembly_descriptors, 2076
	.section	.data.compressed_assemblies,"aw",@progbits
	.type	compressed_assemblies, @object
	.p2align	2
	.global	compressed_assemblies
compressed_assemblies:
	/* count */
	.long	173
	/* descriptors */
	.long	.L.compressed_assembly_descriptors
	.size	compressed_assemblies, 8
