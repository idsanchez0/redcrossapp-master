	.section	.rodata.map_aname.0,"aMS",%progbits,1
	.type	.L.map_aname.0, %object
.L.map_aname.0:
	.asciz	"Xamarin.JavaX.Inject"
	.size	.L.map_aname.0, 21

	.section	.rodata.map_aname.1,"aMS",%progbits,1
	.type	.L.map_aname.1, %object
.L.map_aname.1:
	.asciz	"Xamarin.Firebase.Messaging"
	.size	.L.map_aname.1, 27

	.section	.rodata.map_aname.2,"aMS",%progbits,1
	.type	.L.map_aname.2, %object
.L.map_aname.2:
	.asciz	"Xamarin.GooglePlayServices.Maps"
	.size	.L.map_aname.2, 32

	.section	.rodata.map_aname.3,"aMS",%progbits,1
	.type	.L.map_aname.3, %object
.L.map_aname.3:
	.asciz	"Mono.Android"
	.size	.L.map_aname.3, 13

	.section	.rodata.map_aname.4,"aMS",%progbits,1
	.type	.L.map_aname.4, %object
.L.map_aname.4:
	.asciz	"Syncfusion.SfRotator.XForms.Android"
	.size	.L.map_aname.4, 36

	.section	.rodata.map_aname.5,"aMS",%progbits,1
	.type	.L.map_aname.5, %object
.L.map_aname.5:
	.asciz	"Xamarin.AndroidX.Print"
	.size	.L.map_aname.5, 23

	.section	.rodata.map_aname.6,"aMS",%progbits,1
	.type	.L.map_aname.6, %object
.L.map_aname.6:
	.asciz	"Xamarin.Firebase.Measurement.Connector"
	.size	.L.map_aname.6, 39

	.section	.rodata.map_aname.7,"aMS",%progbits,1
	.type	.L.map_aname.7, %object
.L.map_aname.7:
	.asciz	"Xamarin.AndroidX.VectorDrawable.Animated"
	.size	.L.map_aname.7, 41

	.section	.rodata.map_aname.8,"aMS",%progbits,1
	.type	.L.map_aname.8, %object
.L.map_aname.8:
	.asciz	"Xamarin.AndroidX.Lifecycle.Common"
	.size	.L.map_aname.8, 34

	.section	.rodata.map_aname.9,"aMS",%progbits,1
	.type	.L.map_aname.9, %object
.L.map_aname.9:
	.asciz	"Xamarin.AndroidX.CoordinatorLayout"
	.size	.L.map_aname.9, 35

	.section	.rodata.map_aname.10,"aMS",%progbits,1
	.type	.L.map_aname.10, %object
.L.map_aname.10:
	.asciz	"Syncfusion.SfRating.XForms.Android"
	.size	.L.map_aname.10, 35

	.section	.rodata.map_aname.11,"aMS",%progbits,1
	.type	.L.map_aname.11, %object
.L.map_aname.11:
	.asciz	"Acr.UserDialogs"
	.size	.L.map_aname.11, 16

	.section	.rodata.map_aname.12,"aMS",%progbits,1
	.type	.L.map_aname.12, %object
.L.map_aname.12:
	.asciz	"CarouselView.FormsPlugin.Droid"
	.size	.L.map_aname.12, 31

	.section	.rodata.map_aname.13,"aMS",%progbits,1
	.type	.L.map_aname.13, %object
.L.map_aname.13:
	.asciz	"Xamarin.AndroidX.Preference"
	.size	.L.map_aname.13, 28

	.section	.rodata.map_aname.14,"aMS",%progbits,1
	.type	.L.map_aname.14, %object
.L.map_aname.14:
	.asciz	"Xamarin.Firebase.Installations.InterOp"
	.size	.L.map_aname.14, 39

	.section	.rodata.map_aname.15,"aMS",%progbits,1
	.type	.L.map_aname.15, %object
.L.map_aname.15:
	.asciz	"Xamarin.AndroidX.RecyclerView"
	.size	.L.map_aname.15, 30

	.section	.rodata.map_aname.16,"aMS",%progbits,1
	.type	.L.map_aname.16, %object
.L.map_aname.16:
	.asciz	"Xamarin.AndroidX.AsyncLayoutInflater"
	.size	.L.map_aname.16, 37

	.section	.rodata.map_aname.17,"aMS",%progbits,1
	.type	.L.map_aname.17, %object
.L.map_aname.17:
	.asciz	"Xamarin.AndroidX.AppCompat"
	.size	.L.map_aname.17, 27

	.section	.rodata.map_aname.18,"aMS",%progbits,1
	.type	.L.map_aname.18, %object
.L.map_aname.18:
	.asciz	"Xamarin.Google.Dagger"
	.size	.L.map_aname.18, 22

	.section	.rodata.map_aname.19,"aMS",%progbits,1
	.type	.L.map_aname.19, %object
.L.map_aname.19:
	.asciz	"Xamarin.AndroidX.Lifecycle.LiveData.Core"
	.size	.L.map_aname.19, 41

	.section	.rodata.map_aname.20,"aMS",%progbits,1
	.type	.L.map_aname.20, %object
.L.map_aname.20:
	.asciz	"Xamarin.AndroidX.DrawerLayout"
	.size	.L.map_aname.20, 30

	.section	.rodata.map_aname.21,"aMS",%progbits,1
	.type	.L.map_aname.21, %object
.L.map_aname.21:
	.asciz	"Xamarin.AndroidX.Loader"
	.size	.L.map_aname.21, 24

	.section	.rodata.map_aname.22,"aMS",%progbits,1
	.type	.L.map_aname.22, %object
.L.map_aname.22:
	.asciz	"Xamarin.AndroidX.Fragment"
	.size	.L.map_aname.22, 26

	.section	.rodata.map_aname.23,"aMS",%progbits,1
	.type	.L.map_aname.23, %object
.L.map_aname.23:
	.asciz	"Xamarin.Firebase.Components"
	.size	.L.map_aname.23, 28

	.section	.rodata.map_aname.24,"aMS",%progbits,1
	.type	.L.map_aname.24, %object
.L.map_aname.24:
	.asciz	"Xamarin.Plugin.Calendar"
	.size	.L.map_aname.24, 24

	.section	.rodata.map_aname.25,"aMS",%progbits,1
	.type	.L.map_aname.25, %object
.L.map_aname.25:
	.asciz	"Xamarin.Google.Android.DataTransport.TransportRuntime"
	.size	.L.map_aname.25, 54

	.section	.rodata.map_aname.26,"aMS",%progbits,1
	.type	.L.map_aname.26, %object
.L.map_aname.26:
	.asciz	"ACTIVADOS.Droid"
	.size	.L.map_aname.26, 16

	.section	.rodata.map_aname.27,"aMS",%progbits,1
	.type	.L.map_aname.27, %object
.L.map_aname.27:
	.asciz	"Syncfusion.SfRotator.Android"
	.size	.L.map_aname.27, 29

	.section	.rodata.map_aname.28,"aMS",%progbits,1
	.type	.L.map_aname.28, %object
.L.map_aname.28:
	.asciz	"Com.ViewPagerIndicator"
	.size	.L.map_aname.28, 23

	.section	.rodata.map_aname.29,"aMS",%progbits,1
	.type	.L.map_aname.29, %object
.L.map_aname.29:
	.asciz	"Xamarin.AndroidX.Interpolator"
	.size	.L.map_aname.29, 30

	.section	.rodata.map_aname.30,"aMS",%progbits,1
	.type	.L.map_aname.30, %object
.L.map_aname.30:
	.asciz	"Syncfusion.SfRating.Android"
	.size	.L.map_aname.30, 28

	.section	.rodata.map_aname.31,"aMS",%progbits,1
	.type	.L.map_aname.31, %object
.L.map_aname.31:
	.asciz	"Xamarin.AndroidX.Browser"
	.size	.L.map_aname.31, 25

	.section	.rodata.map_aname.32,"aMS",%progbits,1
	.type	.L.map_aname.32, %object
.L.map_aname.32:
	.asciz	"Xamarin.AndroidX.DocumentFile"
	.size	.L.map_aname.32, 30

	.section	.rodata.map_aname.33,"aMS",%progbits,1
	.type	.L.map_aname.33, %object
.L.map_aname.33:
	.asciz	"Xamarin.GooglePlayServices.Base"
	.size	.L.map_aname.33, 32

	.section	.rodata.map_aname.34,"aMS",%progbits,1
	.type	.L.map_aname.34, %object
.L.map_aname.34:
	.asciz	"Xamarin.AndroidX.Annotation.Experimental"
	.size	.L.map_aname.34, 41

	.section	.rodata.map_aname.35,"aMS",%progbits,1
	.type	.L.map_aname.35, %object
.L.map_aname.35:
	.asciz	"Xamarin.AndroidX.LocalBroadcastManager"
	.size	.L.map_aname.35, 39

	.section	.rodata.map_aname.36,"aMS",%progbits,1
	.type	.L.map_aname.36, %object
.L.map_aname.36:
	.asciz	"Microsoft.IdentityModel.Clients.ActiveDirectory"
	.size	.L.map_aname.36, 48

	.section	.rodata.map_aname.37,"aMS",%progbits,1
	.type	.L.map_aname.37, %object
.L.map_aname.37:
	.asciz	"Xamarin.Google.Android.DataTransport.TransportBackendCct"
	.size	.L.map_aname.37, 57

	.section	.rodata.map_aname.38,"aMS",%progbits,1
	.type	.L.map_aname.38, %object
.L.map_aname.38:
	.asciz	"Xamarin.AndroidX.Activity"
	.size	.L.map_aname.38, 26

	.section	.rodata.map_aname.39,"aMS",%progbits,1
	.type	.L.map_aname.39, %object
.L.map_aname.39:
	.asciz	"Xamarin.AndroidX.Arch.Core.Runtime"
	.size	.L.map_aname.39, 35

	.section	.rodata.map_aname.40,"aMS",%progbits,1
	.type	.L.map_aname.40, %object
.L.map_aname.40:
	.asciz	"Xamarin.AndroidX.Navigation.Common"
	.size	.L.map_aname.40, 35

	.section	.rodata.map_aname.41,"aMS",%progbits,1
	.type	.L.map_aname.41, %object
.L.map_aname.41:
	.asciz	"Syncfusion.SfListView.XForms.Android"
	.size	.L.map_aname.41, 37

	.section	.rodata.map_aname.42,"aMS",%progbits,1
	.type	.L.map_aname.42, %object
.L.map_aname.42:
	.asciz	"FormsViewGroup"
	.size	.L.map_aname.42, 15

	.section	.rodata.map_aname.43,"aMS",%progbits,1
	.type	.L.map_aname.43, %object
.L.map_aname.43:
	.asciz	"Xamarin.AndroidX.VectorDrawable"
	.size	.L.map_aname.43, 32

	.section	.rodata.map_aname.44,"aMS",%progbits,1
	.type	.L.map_aname.44, %object
.L.map_aname.44:
	.asciz	"Xamarin.AndroidX.SavedState"
	.size	.L.map_aname.44, 28

	.section	.rodata.map_aname.45,"aMS",%progbits,1
	.type	.L.map_aname.45, %object
.L.map_aname.45:
	.asciz	"Xamarin.AndroidX.Arch.Core.Common"
	.size	.L.map_aname.45, 34

	.section	.rodata.map_aname.46,"aMS",%progbits,1
	.type	.L.map_aname.46, %object
.L.map_aname.46:
	.asciz	"Xamarin.Forms.Maps.Android"
	.size	.L.map_aname.46, 27

	.section	.rodata.map_aname.47,"aMS",%progbits,1
	.type	.L.map_aname.47, %object
.L.map_aname.47:
	.asciz	"Xamarin.GooglePlayServices.CloudMessaging"
	.size	.L.map_aname.47, 42

	.section	.rodata.map_aname.48,"aMS",%progbits,1
	.type	.L.map_aname.48, %object
.L.map_aname.48:
	.asciz	"Xamarin.AndroidX.AppCompat.AppCompatResources"
	.size	.L.map_aname.48, 46

	.section	.rodata.map_aname.49,"aMS",%progbits,1
	.type	.L.map_aname.49, %object
.L.map_aname.49:
	.asciz	"Microsoft.Identity.Client"
	.size	.L.map_aname.49, 26

	.section	.rodata.map_aname.50,"aMS",%progbits,1
	.type	.L.map_aname.50, %object
.L.map_aname.50:
	.asciz	"Xamarin.AndroidX.Core"
	.size	.L.map_aname.50, 22

	.section	.rodata.map_aname.51,"aMS",%progbits,1
	.type	.L.map_aname.51, %object
.L.map_aname.51:
	.asciz	"Xamarin.Android.Volley"
	.size	.L.map_aname.51, 23

	.section	.rodata.map_aname.52,"aMS",%progbits,1
	.type	.L.map_aname.52, %object
.L.map_aname.52:
	.asciz	"Xamarin.AndroidX.Legacy.Support.Core.UI"
	.size	.L.map_aname.52, 40

	.section	.rodata.map_aname.53,"aMS",%progbits,1
	.type	.L.map_aname.53, %object
.L.map_aname.53:
	.asciz	"Xamarin.AndroidX.Transition"
	.size	.L.map_aname.53, 28

	.section	.rodata.map_aname.54,"aMS",%progbits,1
	.type	.L.map_aname.54, %object
.L.map_aname.54:
	.asciz	"Xamarin.Azure.NotificationHubs.Android"
	.size	.L.map_aname.54, 39

	.section	.rodata.map_aname.55,"aMS",%progbits,1
	.type	.L.map_aname.55, %object
.L.map_aname.55:
	.asciz	"Syncfusion.Cards.XForms.Android"
	.size	.L.map_aname.55, 32

	.section	.rodata.map_aname.56,"aMS",%progbits,1
	.type	.L.map_aname.56, %object
.L.map_aname.56:
	.asciz	"Xamarin.AndroidX.ViewPager2"
	.size	.L.map_aname.56, 28

	.section	.rodata.map_aname.57,"aMS",%progbits,1
	.type	.L.map_aname.57, %object
.L.map_aname.57:
	.asciz	"Syncfusion.SfAutoComplete.XForms.Android"
	.size	.L.map_aname.57, 41

	.section	.rodata.map_aname.58,"aMS",%progbits,1
	.type	.L.map_aname.58, %object
.L.map_aname.58:
	.asciz	"Xamarin.Google.Android.DataTransport.TransportApi"
	.size	.L.map_aname.58, 50

	.section	.rodata.map_aname.59,"aMS",%progbits,1
	.type	.L.map_aname.59, %object
.L.map_aname.59:
	.asciz	"Xamarin.AndroidX.Annotation"
	.size	.L.map_aname.59, 28

	.section	.rodata.map_aname.60,"aMS",%progbits,1
	.type	.L.map_aname.60, %object
.L.map_aname.60:
	.asciz	"Xamarin.AndroidX.CardView"
	.size	.L.map_aname.60, 26

	.section	.rodata.map_aname.61,"aMS",%progbits,1
	.type	.L.map_aname.61, %object
.L.map_aname.61:
	.asciz	"Xamarin.AndroidX.Collection"
	.size	.L.map_aname.61, 28

	.section	.rodata.map_aname.62,"aMS",%progbits,1
	.type	.L.map_aname.62, %object
.L.map_aname.62:
	.asciz	"Xamarin.AndroidX.Navigation.UI"
	.size	.L.map_aname.62, 31

	.section	.rodata.map_aname.63,"aMS",%progbits,1
	.type	.L.map_aname.63, %object
.L.map_aname.63:
	.asciz	"Xamarin.GooglePlayServices.Stats"
	.size	.L.map_aname.63, 33

	.section	.rodata.map_aname.64,"aMS",%progbits,1
	.type	.L.map_aname.64, %object
.L.map_aname.64:
	.asciz	"Syncfusion.SfTabView.XForms.Android"
	.size	.L.map_aname.64, 36

	.section	.rodata.map_aname.65,"aMS",%progbits,1
	.type	.L.map_aname.65, %object
.L.map_aname.65:
	.asciz	"Xamarin.AndroidX.Lifecycle.ViewModelSavedState"
	.size	.L.map_aname.65, 47

	.section	.rodata.map_aname.66,"aMS",%progbits,1
	.type	.L.map_aname.66, %object
.L.map_aname.66:
	.asciz	"Xamarin.Forms.Platform.Android"
	.size	.L.map_aname.66, 31

	.section	.rodata.map_aname.67,"aMS",%progbits,1
	.type	.L.map_aname.67, %object
.L.map_aname.67:
	.asciz	"Syncfusion.Buttons.XForms.Android"
	.size	.L.map_aname.67, 34

	.section	.rodata.map_aname.68,"aMS",%progbits,1
	.type	.L.map_aname.68, %object
.L.map_aname.68:
	.asciz	"AndHUD"
	.size	.L.map_aname.68, 7

	.section	.rodata.map_aname.69,"aMS",%progbits,1
	.type	.L.map_aname.69, %object
.L.map_aname.69:
	.asciz	"Syncfusion.Core.XForms.Android"
	.size	.L.map_aname.69, 31

	.section	.rodata.map_aname.70,"aMS",%progbits,1
	.type	.L.map_aname.70, %object
.L.map_aname.70:
	.asciz	"Xamarin.AndroidX.Lifecycle.ViewModel"
	.size	.L.map_aname.70, 37

	.section	.rodata.map_aname.71,"aMS",%progbits,1
	.type	.L.map_aname.71, %object
.L.map_aname.71:
	.asciz	"Xamarin.AndroidX.Navigation.Runtime"
	.size	.L.map_aname.71, 36

	.section	.rodata.map_aname.72,"aMS",%progbits,1
	.type	.L.map_aname.72, %object
.L.map_aname.72:
	.asciz	"Xamarin.Essentials"
	.size	.L.map_aname.72, 19

	.section	.rodata.map_aname.73,"aMS",%progbits,1
	.type	.L.map_aname.73, %object
.L.map_aname.73:
	.asciz	"Xamarin.Firebase.Installations"
	.size	.L.map_aname.73, 31

	.section	.rodata.map_aname.74,"aMS",%progbits,1
	.type	.L.map_aname.74, %object
.L.map_aname.74:
	.asciz	"Syncfusion.SfPicker.Android"
	.size	.L.map_aname.74, 28

	.section	.rodata.map_aname.75,"aMS",%progbits,1
	.type	.L.map_aname.75, %object
.L.map_aname.75:
	.asciz	"Com.Android.DeskClock"
	.size	.L.map_aname.75, 22

	.section	.rodata.map_aname.76,"aMS",%progbits,1
	.type	.L.map_aname.76, %object
.L.map_aname.76:
	.asciz	"Xamarin.AndroidX.CustomView"
	.size	.L.map_aname.76, 28

	.section	.rodata.map_aname.77,"aMS",%progbits,1
	.type	.L.map_aname.77, %object
.L.map_aname.77:
	.asciz	"Xamarin.AndroidX.Lifecycle.LiveData"
	.size	.L.map_aname.77, 36

	.section	.rodata.map_aname.78,"aMS",%progbits,1
	.type	.L.map_aname.78, %object
.L.map_aname.78:
	.asciz	"Xamarin.Google.Android.Material"
	.size	.L.map_aname.78, 32

	.section	.rodata.map_aname.79,"aMS",%progbits,1
	.type	.L.map_aname.79, %object
.L.map_aname.79:
	.asciz	"Syncfusion.SfPicker.XForms.Android"
	.size	.L.map_aname.79, 35

	.section	.rodata.map_aname.80,"aMS",%progbits,1
	.type	.L.map_aname.80, %object
.L.map_aname.80:
	.asciz	"Xamarin.GooglePlayServices.Tasks"
	.size	.L.map_aname.80, 33

	.section	.rodata.map_aname.81,"aMS",%progbits,1
	.type	.L.map_aname.81, %object
.L.map_aname.81:
	.asciz	"Xamarin.Google.Guava.ListenableFuture"
	.size	.L.map_aname.81, 38

	.section	.rodata.map_aname.82,"aMS",%progbits,1
	.type	.L.map_aname.82, %object
.L.map_aname.82:
	.asciz	"Xamarin.Firebase.Common"
	.size	.L.map_aname.82, 24

	.section	.rodata.map_aname.83,"aMS",%progbits,1
	.type	.L.map_aname.83, %object
.L.map_aname.83:
	.asciz	"Xamarin.AndroidX.Legacy.Support.Core.Utils"
	.size	.L.map_aname.83, 43

	.section	.rodata.map_aname.84,"aMS",%progbits,1
	.type	.L.map_aname.84, %object
.L.map_aname.84:
	.asciz	"Xamarin.AndroidX.SwipeRefreshLayout"
	.size	.L.map_aname.84, 36

	.section	.rodata.map_aname.85,"aMS",%progbits,1
	.type	.L.map_aname.85, %object
.L.map_aname.85:
	.asciz	"Syncfusion.SfComboBox.XForms.Android"
	.size	.L.map_aname.85, 37

	.section	.rodata.map_aname.86,"aMS",%progbits,1
	.type	.L.map_aname.86, %object
.L.map_aname.86:
	.asciz	"Xamarin.Firebase.Datatransport"
	.size	.L.map_aname.86, 31

	.section	.rodata.map_aname.87,"aMS",%progbits,1
	.type	.L.map_aname.87, %object
.L.map_aname.87:
	.asciz	"Xamarin.Firebase.Annotations"
	.size	.L.map_aname.87, 29

	.section	.rodata.map_aname.88,"aMS",%progbits,1
	.type	.L.map_aname.88, %object
.L.map_aname.88:
	.asciz	"Xamarin.AndroidX.Lifecycle.Runtime"
	.size	.L.map_aname.88, 35

	.section	.rodata.map_aname.89,"aMS",%progbits,1
	.type	.L.map_aname.89, %object
.L.map_aname.89:
	.asciz	"Xamarin.AndroidX.Media"
	.size	.L.map_aname.89, 23

	.section	.rodata.map_aname.90,"aMS",%progbits,1
	.type	.L.map_aname.90, %object
.L.map_aname.90:
	.asciz	"Xamarin.GooglePlayServices.Basement"
	.size	.L.map_aname.90, 36

	.section	.rodata.map_aname.91,"aMS",%progbits,1
	.type	.L.map_aname.91, %object
.L.map_aname.91:
	.asciz	"Xamarin.AndroidX.VersionedParcelable"
	.size	.L.map_aname.91, 37

	.section	.rodata.map_aname.92,"aMS",%progbits,1
	.type	.L.map_aname.92, %object
.L.map_aname.92:
	.asciz	"Xamarin.AndroidX.CursorAdapter"
	.size	.L.map_aname.92, 31

	.section	.rodata.map_aname.93,"aMS",%progbits,1
	.type	.L.map_aname.93, %object
.L.map_aname.93:
	.asciz	"Xamarin.Firebase.Iid.Interop"
	.size	.L.map_aname.93, 29

	.section	.rodata.map_aname.94,"aMS",%progbits,1
	.type	.L.map_aname.94, %object
.L.map_aname.94:
	.asciz	"Xamarin.AndroidX.ViewPager"
	.size	.L.map_aname.94, 27

	.section	.rodata.map_aname.95,"aMS",%progbits,1
	.type	.L.map_aname.95, %object
.L.map_aname.95:
	.asciz	"Xamarin.AndroidX.SlidingPaneLayout"
	.size	.L.map_aname.95, 35

