	.arch	armv7-a
	.syntax unified
	.eabi_attribute 67, "2.09"	@ Tag_conformance
	.eabi_attribute 6, 10	@ Tag_CPU_arch
	.eabi_attribute 7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute 8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute 9, 2	@ Tag_THUMB_ISA_use
	.fpu	vfpv3-d16
	.eabi_attribute 34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute 15, 1	@ Tag_ABI_PCS_RW_data
	.eabi_attribute 16, 1	@ Tag_ABI_PCS_RO_data
	.eabi_attribute 17, 2	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute 20, 2	@ Tag_ABI_FP_denormal
	.eabi_attribute 21, 0	@ Tag_ABI_FP_exceptions
	.eabi_attribute 23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute 24, 1	@ Tag_ABI_align_needed
	.eabi_attribute 25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute 38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute 18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute 26, 2	@ Tag_ABI_enum_size
	.eabi_attribute 14, 0	@ Tag_ABI_PCS_R9_use
	.file	"compressed_assemblies.armeabi-v7a.armeabi-v7a.s"
	.include	"compressed_assemblies.armeabi-v7a-data.inc"

	.section	.data.compressed_assembly_descriptors,"aw",%progbits
	.type	.L.compressed_assembly_descriptors, %object
	.p2align	2
.L.compressed_assembly_descriptors:
	/* 0: ACTIVADOS.Droid.dll */
	/* uncompressed_file_size */
	.long	635904
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_0

	/* 1: Acr.UserDialogs.dll */
	/* uncompressed_file_size */
	.long	199168
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_1

	/* 2: AndHUD.dll */
	/* uncompressed_file_size */
	.long	21504
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_2

	/* 3: Azure.Core.dll */
	/* uncompressed_file_size */
	.long	195072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_3

	/* 4: CarouselView.FormsPlugin.Abstractions.dll */
	/* uncompressed_file_size */
	.long	19456
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_4

	/* 5: CarouselView.FormsPlugin.Droid.dll */
	/* uncompressed_file_size */
	.long	156160
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_5

	/* 6: Com.Android.DeskClock.dll */
	/* uncompressed_file_size */
	.long	14336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_6

	/* 7: Com.ViewPagerIndicator.dll */
	/* uncompressed_file_size */
	.long	23040
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_7

	/* 8: FormsViewGroup.dll */
	/* uncompressed_file_size */
	.long	15872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_8

	/* 9: Java.Interop.dll */
	/* uncompressed_file_size */
	.long	164864
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_9

	/* 10: Microsoft.Bcl.AsyncInterfaces.dll */
	/* uncompressed_file_size */
	.long	5120
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_10

	/* 11: Microsoft.CSharp.dll */
	/* uncompressed_file_size */
	.long	300032
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_11

	/* 12: Microsoft.Graph.Core.dll */
	/* uncompressed_file_size */
	.long	172408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_12

	/* 13: Microsoft.Graph.dll */
	/* uncompressed_file_size */
	.long	8361472
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_13

	/* 14: Microsoft.Identity.Client.dll */
	/* uncompressed_file_size */
	.long	1441792
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_14

	/* 15: Microsoft.IdentityModel.Clients.ActiveDirectory.dll */
	/* uncompressed_file_size */
	.long	1025024
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_15

	/* 16: Microsoft.IdentityModel.JsonWebTokens.dll */
	/* uncompressed_file_size */
	.long	53760
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_16

	/* 17: Microsoft.IdentityModel.Logging.dll */
	/* uncompressed_file_size */
	.long	20480
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_17

	/* 18: Microsoft.IdentityModel.Protocols.OpenIdConnect.dll */
	/* uncompressed_file_size */
	.long	99840
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_18

	/* 19: Microsoft.IdentityModel.Protocols.dll */
	/* uncompressed_file_size */
	.long	24064
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_19

	/* 20: Microsoft.IdentityModel.Tokens.dll */
	/* uncompressed_file_size */
	.long	887808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_20

	/* 21: Mono.Android.dll */
	/* uncompressed_file_size */
	.long	2857472
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_21

	/* 22: Mono.Security.dll */
	/* uncompressed_file_size */
	.long	122880
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_22

	/* 23: Newtonsoft.Json.dll */
	/* uncompressed_file_size */
	.long	684544
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_23

	/* 24: RedCrossApp.dll */
	/* uncompressed_file_size */
	.long	1693696
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_24

	/* 25: Syncfusion.Buttons.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	73728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_25

	/* 26: Syncfusion.Buttons.XForms.dll */
	/* uncompressed_file_size */
	.long	159744
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_26

	/* 27: Syncfusion.Cards.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	13312
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_27

	/* 28: Syncfusion.Cards.XForms.dll */
	/* uncompressed_file_size */
	.long	30208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_28

	/* 29: Syncfusion.Core.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	70144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_29

	/* 30: Syncfusion.Core.XForms.dll */
	/* uncompressed_file_size */
	.long	408576
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_30

	/* 31: Syncfusion.DataSource.Portable.dll */
	/* uncompressed_file_size */
	.long	133632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_31

	/* 32: Syncfusion.GridCommon.Portable.dll */
	/* uncompressed_file_size */
	.long	125952
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_32

	/* 33: Syncfusion.Licensing.dll */
	/* uncompressed_file_size */
	.long	47616
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_33

	/* 34: Syncfusion.SfAutoComplete.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	299520
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_34

	/* 35: Syncfusion.SfAutoComplete.XForms.dll */
	/* uncompressed_file_size */
	.long	64000
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_35

	/* 36: Syncfusion.SfComboBox.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	315392
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_36

	/* 37: Syncfusion.SfComboBox.XForms.dll */
	/* uncompressed_file_size */
	.long	72704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_37

	/* 38: Syncfusion.SfListView.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	28672
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_38

	/* 39: Syncfusion.SfListView.XForms.dll */
	/* uncompressed_file_size */
	.long	149504
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_39

	/* 40: Syncfusion.SfPicker.Android.dll */
	/* uncompressed_file_size */
	.long	67584
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_40

	/* 41: Syncfusion.SfPicker.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	30208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_41

	/* 42: Syncfusion.SfPicker.XForms.dll */
	/* uncompressed_file_size */
	.long	68096
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_42

	/* 43: Syncfusion.SfRating.Android.dll */
	/* uncompressed_file_size */
	.long	32256
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_43

	/* 44: Syncfusion.SfRating.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	15872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_44

	/* 45: Syncfusion.SfRating.XForms.dll */
	/* uncompressed_file_size */
	.long	20480
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_45

	/* 46: Syncfusion.SfRotator.Android.dll */
	/* uncompressed_file_size */
	.long	55808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_46

	/* 47: Syncfusion.SfRotator.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	22528
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_47

	/* 48: Syncfusion.SfRotator.XForms.dll */
	/* uncompressed_file_size */
	.long	17408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_48

	/* 49: Syncfusion.SfTabView.XForms.Android.dll */
	/* uncompressed_file_size */
	.long	104448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_49

	/* 50: Syncfusion.SfTabView.XForms.dll */
	/* uncompressed_file_size */
	.long	34816
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_50

	/* 51: System.Buffers.dll */
	/* uncompressed_file_size */
	.long	13688
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_51

	/* 52: System.Core.dll */
	/* uncompressed_file_size */
	.long	1073664
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_52

	/* 53: System.Data.dll */
	/* uncompressed_file_size */
	.long	748032
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_53

	/* 54: System.Diagnostics.DiagnosticSource.dll */
	/* uncompressed_file_size */
	.long	41472
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_54

	/* 55: System.Drawing.Common.dll */
	/* uncompressed_file_size */
	.long	31744
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_55

	/* 56: System.IdentityModel.Tokens.Jwt.dll */
	/* uncompressed_file_size */
	.long	72192
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_56

	/* 57: System.Memory.Data.dll */
	/* uncompressed_file_size */
	.long	12288
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_57

	/* 58: System.Net.Http.dll */
	/* uncompressed_file_size */
	.long	232448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_58

	/* 59: System.Numerics.dll */
	/* uncompressed_file_size */
	.long	38912
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_59

	/* 60: System.Runtime.CompilerServices.Unsafe.dll */
	/* uncompressed_file_size */
	.long	7680
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_60

	/* 61: System.Runtime.Serialization.dll */
	/* uncompressed_file_size */
	.long	511488
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_61

	/* 62: System.ServiceModel.Internals.dll */
	/* uncompressed_file_size */
	.long	56320
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_62

	/* 63: System.Text.Encodings.Web.dll */
	/* uncompressed_file_size */
	.long	57344
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_63

	/* 64: System.Text.Json.dll */
	/* uncompressed_file_size */
	.long	340992
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_64

	/* 65: System.Threading.Tasks.Extensions.dll */
	/* uncompressed_file_size */
	.long	14208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_65

	/* 66: System.Xml.Linq.dll */
	/* uncompressed_file_size */
	.long	77312
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_66

	/* 67: System.Xml.dll */
	/* uncompressed_file_size */
	.long	1397760
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_67

	/* 68: System.dll */
	/* uncompressed_file_size */
	.long	908288
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_68

	/* 69: Xamarin.Android.Volley.dll */
	/* uncompressed_file_size */
	.long	177664
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_69

	/* 70: Xamarin.AndroidX.Activity.dll */
	/* uncompressed_file_size */
	.long	52224
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_70

	/* 71: Xamarin.AndroidX.AppCompat.AppCompatResources.dll */
	/* uncompressed_file_size */
	.long	16384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_71

	/* 72: Xamarin.AndroidX.AppCompat.dll */
	/* uncompressed_file_size */
	.long	460800
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_72

	/* 73: Xamarin.AndroidX.Browser.dll */
	/* uncompressed_file_size */
	.long	30208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_73

	/* 74: Xamarin.AndroidX.CardView.dll */
	/* uncompressed_file_size */
	.long	17408
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_74

	/* 75: Xamarin.AndroidX.CoordinatorLayout.dll */
	/* uncompressed_file_size */
	.long	78848
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_75

	/* 76: Xamarin.AndroidX.Core.dll */
	/* uncompressed_file_size */
	.long	643072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_76

	/* 77: Xamarin.AndroidX.CustomView.dll */
	/* uncompressed_file_size */
	.long	8704
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_77

	/* 78: Xamarin.AndroidX.DrawerLayout.dll */
	/* uncompressed_file_size */
	.long	43520
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_78

	/* 79: Xamarin.AndroidX.Fragment.dll */
	/* uncompressed_file_size */
	.long	183296
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_79

	/* 80: Xamarin.AndroidX.Legacy.Support.Core.UI.dll */
	/* uncompressed_file_size */
	.long	15872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_80

	/* 81: Xamarin.AndroidX.Lifecycle.Common.dll */
	/* uncompressed_file_size */
	.long	14848
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_81

	/* 82: Xamarin.AndroidX.Lifecycle.LiveData.Core.dll */
	/* uncompressed_file_size */
	.long	15872
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_82

	/* 83: Xamarin.AndroidX.Lifecycle.ViewModel.dll */
	/* uncompressed_file_size */
	.long	16896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_83

	/* 84: Xamarin.AndroidX.Loader.dll */
	/* uncompressed_file_size */
	.long	36352
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_84

	/* 85: Xamarin.AndroidX.RecyclerView.dll */
	/* uncompressed_file_size */
	.long	430080
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_85

	/* 86: Xamarin.AndroidX.SavedState.dll */
	/* uncompressed_file_size */
	.long	12800
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_86

	/* 87: Xamarin.AndroidX.SwipeRefreshLayout.dll */
	/* uncompressed_file_size */
	.long	39936
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_87

	/* 88: Xamarin.AndroidX.VersionedParcelable.dll */
	/* uncompressed_file_size */
	.long	9728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_88

	/* 89: Xamarin.AndroidX.ViewPager.dll */
	/* uncompressed_file_size */
	.long	57344
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_89

	/* 90: Xamarin.Azure.NotificationHubs.Android.dll */
	/* uncompressed_file_size */
	.long	127488
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_90

	/* 91: Xamarin.Essentials.dll */
	/* uncompressed_file_size */
	.long	39936
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_91

	/* 92: Xamarin.Firebase.Messaging.dll */
	/* uncompressed_file_size */
	.long	31232
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_92

	/* 93: Xamarin.Forms.Core.dll */
	/* uncompressed_file_size */
	.long	1207296
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_93

	/* 94: Xamarin.Forms.Maps.Android.dll */
	/* uncompressed_file_size */
	.long	153088
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_94

	/* 95: Xamarin.Forms.Maps.dll */
	/* uncompressed_file_size */
	.long	24064
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_95

	/* 96: Xamarin.Forms.Platform.Android.dll */
	/* uncompressed_file_size */
	.long	863232
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_96

	/* 97: Xamarin.Forms.Platform.dll */
	/* uncompressed_file_size */
	.long	191368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_97

	/* 98: Xamarin.Forms.Xaml.dll */
	/* uncompressed_file_size */
	.long	103424
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_98

	/* 99: Xamarin.Google.Android.DataTransport.TransportApi.dll */
	/* uncompressed_file_size */
	.long	29696
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_99

	/* 100: Xamarin.Google.Android.DataTransport.TransportBackendCct.dll */
	/* uncompressed_file_size */
	.long	91136
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_100

	/* 101: Xamarin.Google.Android.DataTransport.TransportRuntime.dll */
	/* uncompressed_file_size */
	.long	254976
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_101

	/* 102: Xamarin.Google.Android.Material.dll */
	/* uncompressed_file_size */
	.long	276480
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_102

	/* 103: Xamarin.Google.Dagger.dll */
	/* uncompressed_file_size */
	.long	92160
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_103

	/* 104: Xamarin.Google.Guava.ListenableFuture.dll */
	/* uncompressed_file_size */
	.long	18072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_104

	/* 105: Xamarin.GooglePlayServices.Base.dll */
	/* uncompressed_file_size */
	.long	185344
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_105

	/* 106: Xamarin.GooglePlayServices.Basement.dll */
	/* uncompressed_file_size */
	.long	75264
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_106

	/* 107: Xamarin.GooglePlayServices.Maps.dll */
	/* uncompressed_file_size */
	.long	235520
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_107

	/* 108: Xamarin.GooglePlayServices.Tasks.dll */
	/* uncompressed_file_size */
	.long	49664
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_108

	/* 109: Xamarin.JavaX.Inject.dll */
	/* uncompressed_file_size */
	.long	16896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_109

	/* 110: Xamarin.Plugin.Calendar.dll */
	/* uncompressed_file_size */
	.long	228864
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_110

	/* 111: mscorlib.dll */
	/* uncompressed_file_size */
	.long	2302976
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.long	compressed_assembly_data_111

	.size	.L.compressed_assembly_descriptors, 1344
	.section	.data.compressed_assemblies,"aw",%progbits
	.type	compressed_assemblies, %object
	.p2align	2
	.global	compressed_assemblies
compressed_assemblies:
	/* count */
	.long	112
	/* descriptors */
	.long	.L.compressed_assembly_descriptors
	.size	compressed_assemblies, 8
