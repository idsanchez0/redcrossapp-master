﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using AndroidApp = Android.App.Application;

namespace RedCrossApp.Droid
{
    public class NotificationHelper
    {
        const string channelId = "default";
        const string channelName = "Default";
        const string channelDescription = "The default channel for notifications.";
        const int pendingIntentId = 0;

        public const string TitleKey = "title";
        public const string MessageKey = "message";

        bool channelInitialized = false;
        int messageId = -1;
        NotificationManager manager;


        void CreateNotificationChannel()
        {
            manager = (NotificationManager)AndroidApp.Context.GetSystemService(AndroidApp.NotificationService);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                var channelNameJava = new Java.Lang.String(channelName);
                var channel = new NotificationChannel(channelId, channelNameJava, NotificationImportance.Default)
                {
                    Description = channelDescription
                };
                manager.CreateNotificationChannel(channel);
            }

            channelInitialized = true;
        }

        public int CrearNotificacionLocal(string pTitle, string pBody, string action = "", 
                            string extraField = "", string extraValue = "")
        {

            if (!channelInitialized)
            {
                CreateNotificationChannel();
            }

            messageId++;

            Intent intent = new Intent(AndroidApp.Context, typeof(MainActivity));
            intent.PutExtra(TitleKey, pTitle);
            intent.PutExtra(MessageKey, pBody);

            if(!string.IsNullOrEmpty(extraField) && !string.IsNullOrEmpty(extraValue))
                intent.PutExtra(extraField, extraValue);

            intent.SetAction(action);
            intent.AddFlags(ActivityFlags.ClearTop);

            PendingIntent pendingIntent = PendingIntent.GetActivity(AndroidApp.Context, pendingIntentId, intent, PendingIntentFlags.UpdateCurrent);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(AndroidApp.Context, channelId)
                .SetContentIntent(pendingIntent)
                .SetContentTitle(pTitle)
                .SetContentText(pBody)
                .SetAutoCancel(true)
                .SetSmallIcon(ACTIVADOS.Droid.Resource.Drawable.Icon)
                .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate);

            var notification = builder.Build();
            manager.Notify(messageId, notification);

            return messageId;
        }

    }
}