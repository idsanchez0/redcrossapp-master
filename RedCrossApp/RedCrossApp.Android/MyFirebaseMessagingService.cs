﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Firebase.Messaging;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;

namespace RedCrossApp.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        NotificationHelper androidNotification = new NotificationHelper();
        public override void OnMessageReceived(RemoteMessage message)
        {
            try
            {
                IDictionary<string, string> MensajeData = message.Data;

                string Titulo = MensajeData.ContainsKey("notiTitle") ? MensajeData["notiTitle"] : "";
                string SubTitulo = MensajeData.ContainsKey("notiBody") ? MensajeData["notiBody"] : "";
                string Action = MensajeData.ContainsKey("notiAction") ? MensajeData["notiAction"] : "";
                string Field = MensajeData.ContainsKey("key") ? MensajeData["key"] : "";
                string Value = MensajeData.ContainsKey("value") ? MensajeData["value"] : "";

                androidNotification.CrearNotificacionLocal(Titulo, SubTitulo, Action, Field, Value);
            }
            catch(Exception e)
            {
                androidNotification.CrearNotificacionLocal("Error", e.Message);
            }

        }

        public override void OnDeletedMessages()
        {
            base.OnDeletedMessages();
        }

        public override void OnNewToken(string token)
        {
            base.OnNewToken(token);

            Preferences.Set("TokenFirebase", token);
            sedRegisterToken(token);
        }
        public void sedRegisterToken(string token)
        {
            //Tu código para registrar el token a tu servidor y base de datos
        }
    }
}